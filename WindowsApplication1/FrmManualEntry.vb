﻿Imports QRCoder
Imports System.IO
Imports System.Drawing.Printing
Public Class FrmManualEntry
    Dim objcon As New Connections
    Public DSPkdid As New DataSet()
    Public DSpec As New DataSet()
    Public DsTruck As New DataSet()
    Public DsLeaftruck As New DataSet()
    Public SelectedShift As String
    Public SelectedDate As DateTime
    Public SelectedprodDate As DateTime
    Private Sub FrmManualEntry_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtItemCode.Text = GlobalVariables.SelectionTreadCode
        txtColourCode.Text = GlobalVariables.ItemCode

        If txtColourCode.Text.StartsWith("SW") Then
            GlobalVariables.Defect_Type = "SW"
            LocationLoad()
            CodeLoadSW()
        Else
            GlobalVariables.Defect_Type = "other"
            LocationLoad()
            CodeLoad()
        End If
    End Sub
    Public Sub CodeLoad()
        Dim dtab As New DataSet()
        dtab = objcon.GetCode()

        'cboCode.Items.Clear()
        cboCode.DisplayMember = "LeafTruck_No"
        cboCode.DataSource = dtab.Tables(0)

    End Sub
    Public Sub CodeLoadSW()
        Dim dtab As New DataSet()
        dtab = objcon.GetCodeSW()

        'cboCode.Items.Clear()
        cboCode.DisplayMember = "LeafTruck_No"
        cboCode.DataSource = dtab.Tables(0)
    End Sub
    Public Sub LocationLoad()
        Dim dtab1 As New DataSet()

        dtab1 = objcon.GetLocation1()

        If dtab1.Tables(0).Rows.Count <= 0 Then
            CboLocation.Items.Add("NL")
        End If

        'CboLocation.DisplayMember = "Location"
        'CboLocation.DataSource = dtab1.Tables(0)
        Dim dr As DataRow
        ' CboLocation.Items.Add("NL")
        For Each dr In dtab1.Tables(0).Rows
            CboLocation.Items.Add(dr("Location"))
        Next


    End Sub

    Private Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
        If Fdate.Text <> "" And Shift.Text <> "" And txtOperator.Text <> "" And hr.Text <> "" And min.Text <> "" And sec.Text <> "" And cboCode.Text <> "" And CboLocation.Text <> "" And txtQty.Text <> "" And txtNo.Text <> "" Then
            GlobalVariables.FromDate = Convert.ToDateTime(Fdate.Text).ToString("yyyy/MM/dd")
            GlobalVariables.Shift = Shift.Text
            GlobalVariables.OperatorNo = txtOperator.Text
            GlobalVariables.Code = cboCode.Text
            GlobalVariables.Location = CboLocation.Text
            GlobalVariables.Qty = txtQty.Text
            GlobalVariables.No = txtNo.Text
            GlobalVariables.time = hr.Text + ":" + min.Text + ":" + sec.Text + ".000"

            GlobalVariables.curdate = GlobalVariables.FromDate + " " + GlobalVariables.time
            GlobalVariables.DualDate = Convert.ToDateTime(GlobalVariables.curdate).ToString("yyyy/MM/dd HH:mm:ss")
            Dim pkextid As Single = objcon.get_DextPKID()
            GlobalVariables.DescId = pkextid
            If cboCode.Text <> "" And CboLocation.Text <> "" And txtQty.Text <> "" And txtNo.Text <> "" Then

                GlobalVariables.T1 = cboCode.Text
                GlobalVariables.T2 = CboLocation.Text
                GlobalVariables.T3 = txtQty.Text
                GlobalVariables.T4 = txtNo.Text

                Dim C1 As Single = objcon.Check_EntryCount()
                'If C1 <= 0 Then              //to by pass the truck no updated on 2021-04-01
                If C1 >= 0 Then

                    Dim Loc As Single = objcon.LocCount()
                    'If Loc <= 0 Then           //to bypass the leaf truck location  2021-04-01
                    If Loc > 0 Then
                        MessageBox.Show("Location not Available")
                        Me.Close()
                    Else
                        objcon.insertTreadEnteryManual()
                        objcon.FalseUpdateLocation()

                        If txtColourCode.Text.StartsWith("SW") Then
                            objcon.FalseUpdateLeaftruckSW()
                        Else
                            objcon.FalseUpdateLeaftruck()
                        End If



                        If txtColourCode.Text.StartsWith("SW") Then
                            Dim pd1 As New PrintDocument
                            AddHandler pd1.PrintPage, AddressOf Me.PrintDocument2_PrintPage
                            pd1.PrintController = New StandardPrintController()

                            pd1.DefaultPageSettings.Landscape = False
                            pd1.PrinterSettings.Copies = "1"

                            pd1.Print()
                        Else
                            Dim pd As New PrintDocument
                            AddHandler pd.PrintPage, AddressOf Me.PrintDocument1_PrintPage
                            pd.PrintController = New StandardPrintController()

                            pd.DefaultPageSettings.Landscape = False
                            pd.PrinterSettings.Copies = "1"

                            pd.Print()

                        End If


                        CboLocation.Text = ""
                        CboLocation.Items.Clear()
                        'cboCode.Items.Clear()
                        CodeLoad()
                        LocationLoad()
                        txtQty.Text = ""
                        txtNo.Text = ""
                        Me.Refresh()
                        Me.Close()
                    End If
                End If

            End If
        Else

            MessageBox.Show("Fill All Details")

        End If



        Clear()
        'MessageBox.Show("Truck No Not available")
        FrmManualSchedule.Show()
    End Sub
    Private Sub Clear()
        GlobalVariables.Location = ""
        GlobalVariables.Code = ""
    End Sub
    Private Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Me.Close()
        FrmManualSchedule.Show()
    End Sub

    Private Sub btnDel_Click(sender As Object, e As EventArgs) Handles btnDel.Click
        GlobalVariables.Size = txtItemCode.Text
        btnPrint.Refresh()
        Me.Refresh()
        DeleteDetails.Show()
    End Sub

    Private Sub PrintDocument1_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage


        Lblcomp.Text = GlobalVariables.Splicer1 + "/" + GlobalVariables.Splicer2 + "/" + GlobalVariables.Splicer3

        Dim printFont As New Font("Bell MT Bold", 12.0F, FontStyle.Bold)
        Dim printFontSmall As New Font("Bell MT Bold", 8.0F, FontStyle.Bold)
        Dim printFontHead As New Font(FontFamily.GenericMonospace, 18.0F, FontStyle.Bold)
        Dim printFontHeadHigh As New Font(FontFamily.GenericMonospace, 26.0F, FontStyle.Bold)
        Dim printFontHeadHighest As New Font("Bell MT Bold", 36.0F, FontStyle.Bold)

        e.Graphics.DrawString(GlobalVariables.DescId, printFontSmall, Brushes.Black, 200, 50)

        Dim qrGenerator As QRCodeGenerator = New QRCodeGenerator()
        Dim qrCode As QRCodeGenerator.QRCode = qrGenerator.CreateQrCode(GlobalVariables.DescId, QRCodeGenerator.ECCLevel.H)
        'Dim qrCode As QRCodeGenerator.QRCode = qrGenerator.CreateQrCode("1003-8336", QRCodeGenerator.ECCLevel.H)


        Dim bitmap As Bitmap = qrCode.GetGraphic(2.5)
        Dim ms As MemoryStream = New MemoryStream()
        bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Png)



        Dim blackPen As New Pen(Color.Black, 3)

        ' Create points that define line.
        Dim x As Integer = 15
        Dim y As Integer = 130
        Dim width As Integer = 280
        Dim height As Integer = 40

        ' Draw rectangle to screen.
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)




        e.Graphics.DrawString("FT-01-B5:02", printFontSmall, Brushes.Black, 20, 500)
        e.Graphics.DrawString("TREAD", printFontHeadHighest, Brushes.Black, 50, 70)
        ' e.Graphics.DrawString("FT-01-B5:03", printFontSmall, Brushes.Black, 10, 480)


        e.Graphics.DrawImage(bitmap, 200, 60)
        e.Graphics.DrawString("CODE", printFont, Brushes.Black, 20, 140)
        '   e.Graphics.DrawString("CODE", printFont, Brushes.Black, 10, 140)

        e.Graphics.DrawString(txtColourCode.Text, printFont, Brushes.Black, 130, 140)

        x = 15
        y = 170
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString("TREAD", printFont, Brushes.Black, 20, 180)
        e.Graphics.DrawString(txtItemCode.Text, printFont, Brushes.Black, 130, 180)
        x = 15
        y = 210
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)



        e.Graphics.DrawString("NOS", printFont, Brushes.Black, 20, 220)
        e.Graphics.DrawString(txtQty.Text, printFont, Brushes.Black, 130, 220)
        e.Graphics.DrawString(GlobalVariables.DualSelection, printFont, Brushes.Black, 210, 220)

        x = 15
        y = 250
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)




        e.Graphics.DrawString("OPERATOR", printFont, Brushes.Black, 20, 260)
        e.Graphics.DrawString(txtOperator.Text, printFont, Brushes.Black, 130, 260)

        x = 15
        y = 290
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString("DATE/SHIFT", printFont, Brushes.Black, 20, 300)
        e.Graphics.DrawString(GlobalVariables.FromDate & "--" & GlobalVariables.Shift, printFont, Brushes.Black, 130, 300)
        x = 15
        y = 330
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString("TIME", printFont, Brushes.Black, 20, 340)
        e.Graphics.DrawString(GlobalVariables.time, printFont, Brushes.Black, 130, 340)
        x = 15
        y = 370
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString("LOCATION", printFont, Brushes.Black, 20, 380)
        e.Graphics.DrawString(GlobalVariables.Location, printFont, Brushes.Black, 130, 380)

        x = 15
        y = 410
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)


        e.Graphics.DrawString("LT NO : ", printFont, Brushes.Black, 20, 420)
        e.Graphics.DrawString("TR NO : ", printFont, Brushes.Black, 140, 420)
        e.Graphics.DrawString(GlobalVariables.Code, printFont, Brushes.Black, 100, 420)
        e.Graphics.DrawString(txtNo.Text, printFont, Brushes.Black, 220, 420)

        x = 15
        y = 450
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString("COMP :", printFont, Brushes.Black, 20, 460)
        e.Graphics.DrawString(Lblcomp.Text, printFont, Brushes.Black, 100, 460)

        x = 15
        y = 450

        e.Graphics.DrawRectangle(blackPen, x, y, width, height)




        Dim Line1 As New Point(130, 130)
        Dim Line2 As New Point(130, 410)

        ' Draw line to screen.
        e.Graphics.DrawLine(blackPen, Line1, Line2)

        Dim Line3 As New Point(210, 210)
        Dim Line4 As New Point(210, 250)

        ' Draw line to screen.
        e.Graphics.DrawLine(blackPen, Line3, Line4)


    End Sub

    Private Sub PrintDocument2_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument2.PrintPage


        Dim printFont As New Font("Bell MT Bold", 12.0F, FontStyle.Bold)
        Dim printFontSmall As New Font("Bell MT Bold", 8.0F, FontStyle.Bold)
        Dim printFontHead As New Font(FontFamily.GenericMonospace, 18.0F, FontStyle.Bold)
        Dim printFontHeadHigh As New Font(FontFamily.GenericMonospace, 26.0F, FontStyle.Bold)
        Dim printFontHeadHighest As New Font("Bell MT Bold", 36.0F, FontStyle.Bold)

        e.Graphics.DrawString(GlobalVariables.DescId, printFontSmall, Brushes.Black, 200, 45)

        Dim qrGenerator As QRCodeGenerator = New QRCodeGenerator()
        Dim qrCode As QRCodeGenerator.QRCode = qrGenerator.CreateQrCode(GlobalVariables.DescId, QRCodeGenerator.ECCLevel.H)
        'Dim qrCode As QRCodeGenerator.QRCode = qrGenerator.CreateQrCode("1003-8336", QRCodeGenerator.ECCLevel.H)


        Dim bitmap As Bitmap = qrCode.GetGraphic(2.5)
        Dim ms As MemoryStream = New MemoryStream()
        bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Png)



        Dim blackPen As New Pen(Color.Black, 3)

        ' Create points that define line.
        Dim x As Integer = 15
        Dim y As Integer = 130
        Dim width As Integer = 290
        Dim height As Integer = 40

        ' Draw rectangle to screen.
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)




        e.Graphics.DrawString("FT-01-B5:03", printFontSmall, Brushes.Black, 20, 500)
        e.Graphics.DrawString("SIDE WALL", printFontHead, Brushes.Black, 10, 80)
        ' e.Graphics.DrawString("FT-01-B5:03", printFontSmall, Brushes.Black, 10, 480)


        e.Graphics.DrawImage(bitmap, 200, 60)
        e.Graphics.DrawString("CODE", printFont, Brushes.Black, 20, 140)
        '   e.Graphics.DrawString("CODE", printFont, Brushes.Black, 10, 140)

        e.Graphics.DrawString(txtColourCode.Text, printFont, Brushes.Black, 130, 140)

        x = 15
        y = 170
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString("SIDEWALL", printFont, Brushes.Black, 20, 180)
        e.Graphics.DrawString(txtItemCode.Text, printFont, Brushes.Black, 130, 180)
        x = 15
        y = 210
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)


        e.Graphics.DrawString("NOS", printFont, Brushes.Black, 20, 220)
        e.Graphics.DrawString(txtQty.Text, printFont, Brushes.Black, 130, 220)
        e.Graphics.DrawString(GlobalVariables.DualSelection, printFont, Brushes.Black, 210, 220)

        x = 15
        y = 250
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString("OPERATOR", printFont, Brushes.Black, 20, 260)
        e.Graphics.DrawString(txtOperator.Text, printFont, Brushes.Black, 130, 260)

        x = 15
        y = 290
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString("DATE/SHIFT", printFont, Brushes.Black, 20, 300)
        e.Graphics.DrawString(GlobalVariables.FromDate & "--" & GlobalVariables.Shift, printFont, Brushes.Black, 130, 300)
        x = 15
        y = 330
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString("TIME", printFont, Brushes.Black, 20, 340)
        e.Graphics.DrawString(GlobalVariables.time, printFont, Brushes.Black, 130, 340)
        x = 15
        y = 370
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString("LOCATION", printFont, Brushes.Black, 20, 380)
        e.Graphics.DrawString(CboLocation.Text, printFont, Brushes.Black, 130, 380)
        x = 15
        y = 410
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString("M/C NO", printFont, Brushes.Black, 20, 420)
        e.Graphics.DrawString(GlobalVariables.DualSelection, printFont, Brushes.Black, 130, 420)
        x = 15
        y = 450
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString("SWB NO : ", printFont, Brushes.Black, 20, 460)
        e.Graphics.DrawString("SL NO : ", printFont, Brushes.Black, 140, 460)
        e.Graphics.DrawString(GlobalVariables.Code, printFont, Brushes.Black, 100, 460)
        e.Graphics.DrawString(txtNo.Text, printFont, Brushes.Black, 220, 460)

        Dim Line1 As New Point(130, 130)
        Dim Line2 As New Point(130, 450)

        '' Draw line to screen.
        e.Graphics.DrawLine(blackPen, Line1, Line2)


    End Sub

    Private Sub CboLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CboLocation.SelectedIndexChanged

    End Sub

    Private Sub lblCode_Click(sender As Object, e As EventArgs) Handles lblCode.Click

    End Sub
End Class