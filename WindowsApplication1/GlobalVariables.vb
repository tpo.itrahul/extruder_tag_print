﻿Imports System.Data
Public Class GlobalVariables
    Public Shared LoginName As String
    Public Shared Bias As String
    Public Shared CurrentDate As String
    Public Shared Shift As String
    Public Shared SerialNo As String
    Public Shared MouldNo As String
    Public Shared Tyre As String
    Public Shared Size As String
    Public Shared Press As String
    Public Shared Position As String

    Public Shared Defect_Type As String
    Public Shared DefectID As Integer

    Public Shared Password As String
    Public Shared Plycode As String

    Public Shared DTPlyCode As New DataTable
    Public Shared DTOperator As New DataTable
    Public Shared DTSplicer As New DataTable
    Public Shared DTSelection As New DataTable
    Public Shared DTSpecMaster As New DataTable


    Public Shared OperatorNo As String
    Public Shared CalPRNo As String
    Public Shared CalPRShift As String
    Public Shared CalPRDate As String
    Public Shared Splicer1 As String
    Public Shared Splicer2 As String
    Public Shared Splicer3 As String
    Public Shared Splicer4 As String
    Public Shared PartRoleNo As String
    Public Shared DualSelection As String

    Public Shared DualShift As String
    Public Shared DualDate As Date
    Public Shared DualExactShift As String
    Public Shared DualExactDate As String
    Public Shared SelectionTreadCode As String

    Public Shared ItemCode As String
    Public Shared TreadCode As String

    Public Shared Code As String
    Public Shared Location As String
    Public Shared pkdextid As String
    Public Shared id As Single
    Public Shared DescId As Single


    Public Shared extdate As String
    Public Shared exttime As String
    Public Shared extshift As String
    Public Shared Qty As String
    Public Shared No As String
    Public Shared colourcode As String
    Public Shared description As String
    Public Shared truckno As String
    Public Shared txtSelectDate As String
    Public Shared treads As String
    Public Shared proddate As String
    Public Shared prodshift As String
    Public Shared prodtime As String
    Public Shared ddlShift As String
    Public Shared shiftTime As String
    Public Shared txtDate As String
    Public Shared curdate As String

    Public Shared gsTruckNo As String

    Public Shared Username As String
    Friend Shared spectablename As String


    Public Shared T1 As String
    Public Shared T2 As String
    Public Shared T3 As String
    Public Shared T4 As String
    Public Shared oldLocation As String
    Public Shared oldLeaf As String
    Public Shared ccode As String
    Public Shared time As String
    Public Shared timevaluehr As String
    Public Shared selectTread As String
    Public Shared agevalue As String
    Public Shared idvalue As String
    Public Shared timevalue As String
    Public Shared dualext As String
    Public Shared colorcode As String

    
    Public Shared clrcode As String

    Public Shared usertype As String
    Public Shared Name As String
    Public Shared Id1 As String

    Public Shared HDate As String
    Public Shared HShift As String
    Public Shared HEmpNo As String
    Public Shared HEmpName As String
    Public Shared HBy As String
    Public Shared HReason As String
    Public Shared HId As String

    Public Shared FromDate As String
    Public Shared ToDate As String
    Public Shared MobNo As String
    Public Shared BookerNo As String
    Public Shared UWB_Tag_Id As String

End Class
