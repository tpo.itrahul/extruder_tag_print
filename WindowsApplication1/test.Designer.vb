﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class test
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.DGSchSelection = New System.Windows.Forms.DataGridView()
        Me.SLNO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Code = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Description = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Priority = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColorCode = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Schedule = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Compound = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.comp2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.comp3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProductionEntry = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.Column1 = New System.Windows.Forms.DataGridViewComboBoxColumn()
        CType(Me.DGSchSelection, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DGSchSelection
        '
        Me.DGSchSelection.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Peru
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGSchSelection.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DGSchSelection.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGSchSelection.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.SLNO, Me.Code, Me.Description, Me.Priority, Me.ColorCode, Me.Schedule, Me.Compound, Me.comp2, Me.comp3, Me.ProductionEntry, Me.Column1})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DGSchSelection.DefaultCellStyle = DataGridViewCellStyle2
        Me.DGSchSelection.Location = New System.Drawing.Point(-275, 27)
        Me.DGSchSelection.Name = "DGSchSelection"
        Me.DGSchSelection.RowTemplate.Height = 30
        Me.DGSchSelection.Size = New System.Drawing.Size(1196, 492)
        Me.DGSchSelection.TabIndex = 1
        '
        'SLNO
        '
        Me.SLNO.HeaderText = "SL No"
        Me.SLNO.Name = "SLNO"
        Me.SLNO.Visible = False
        '
        'Code
        '
        Me.Code.HeaderText = "Code"
        Me.Code.Name = "Code"
        Me.Code.Width = 200
        '
        'Description
        '
        Me.Description.HeaderText = "Description"
        Me.Description.Name = "Description"
        Me.Description.Width = 350
        '
        'Priority
        '
        Me.Priority.HeaderText = "Priority"
        Me.Priority.Name = "Priority"
        '
        'ColorCode
        '
        Me.ColorCode.HeaderText = "Color Code"
        Me.ColorCode.Name = "ColorCode"
        Me.ColorCode.Width = 150
        '
        'Schedule
        '
        Me.Schedule.HeaderText = "Schedule"
        Me.Schedule.Name = "Schedule"
        Me.Schedule.Width = 120
        '
        'Compound
        '
        Me.Compound.HeaderText = "Compound"
        Me.Compound.Name = "Compound"
        Me.Compound.Visible = False
        '
        'comp2
        '
        Me.comp2.HeaderText = "comp2"
        Me.comp2.Name = "comp2"
        Me.comp2.Visible = False
        '
        'comp3
        '
        Me.comp3.HeaderText = "comp3"
        Me.comp3.Name = "comp3"
        Me.comp3.Visible = False
        '
        'ProductionEntry
        '
        Me.ProductionEntry.HeaderText = "Production Entry"
        Me.ProductionEntry.Name = "ProductionEntry"
        Me.ProductionEntry.Text = "SELECT"
        Me.ProductionEntry.UseColumnTextForButtonValue = True
        '
        'Column1
        '
        Me.Column1.HeaderText = "Column1"
        Me.Column1.Name = "Column1"
        '
        'test
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1004, 510)
        Me.Controls.Add(Me.DGSchSelection)
        Me.Name = "test"
        Me.Text = "test"
        CType(Me.DGSchSelection, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DGSchSelection As System.Windows.Forms.DataGridView
    Friend WithEvents SLNO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Code As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Description As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Priority As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColorCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Schedule As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Compound As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents comp2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents comp3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ProductionEntry As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewComboBoxColumn
End Class
