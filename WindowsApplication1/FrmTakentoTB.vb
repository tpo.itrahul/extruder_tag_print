﻿Public Class FrmTakentoTB
    Public DsTreads As New DataSet()
    Dim objcon As New Connections
    Public dt As New DataTable()
    Dim rwcnt As Integer = 0
    Public ds As New DataSet()
    Dim ff As Double
    Private Sub FrmTakentoTB_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        dt.Rows.Clear()
        dt.Columns.Clear()
        rwcnt = 0
        DataGridView1.Visible = True


        dt.Columns.Add("id", GetType(String))
        dt.Columns.Add("ext_date", GetType(String))
        dt.Columns.Add("ext_time", GetType(String))
        dt.Columns.Add("ext_shift", GetType(String))
        dt.Columns.Add("treadcode", GetType(String))
        dt.Columns.Add("colorcode", GetType(String))
        dt.Columns.Add("description", GetType(String))
        dt.Columns.Add("truckno", GetType(String))
        dt.Columns.Add("location", GetType(String))
        dt.Columns.Add("treads", GetType(String))
        dt.Columns.Add("prod_date", GetType(String))
        dt.Columns.Add("prod_shift", GetType(String))
        dt.Columns.Add("pkdextid", GetType(String))
        dt.Columns.Add("Dual_Ext", GetType(String))
        dt.Columns.Add("Operator", GetType(String))
        dt.Columns.Add("Truck_No", GetType(String))

        ds = objcon.GETFinalData()

        If ds.Tables(0).Rows.Count > 0 Then

            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1

                Dim dr As DataRow = dt.NewRow()

                dt.Rows.Add(dr)
                dt.Rows(rwcnt)(0) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(0))
                dt.Rows(rwcnt)(1) = Convert.ToDateTime(ds.Tables(0).Rows(i).ItemArray(1)).ToString("dd/MM/yyyy")
                dt.Rows(rwcnt)(2) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(2))
                dt.Rows(rwcnt)(3) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(3))
                dt.Rows(rwcnt)(4) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(4))
                dt.Rows(rwcnt)(5) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(5))
                dt.Rows(rwcnt)(6) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(6))
                dt.Rows(rwcnt)(7) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(7))
                dt.Rows(rwcnt)(8) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(8))
                dt.Rows(rwcnt)(9) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(9))
                dt.Rows(rwcnt)(10) = Convert.ToDateTime(ds.Tables(0).Rows(i).ItemArray(10)).ToString("dd/MM/yyyy")
                dt.Rows(rwcnt)(11) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(11))
                dt.Rows(rwcnt)(12) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(13))
                dt.Rows(rwcnt)(13) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(14))
                dt.Rows(rwcnt)(14) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(15))
                dt.Rows(rwcnt)(14) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(16))

                '' rwcnt += 1
            Next

            DataGridView1.DataSource = dt


        Else
            ' MessageBox.Show("No data Found !!!")
        End If
    End Sub
    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        If txtSearch.Text <> "" Then
            Dim str As String = txtSearch.Text
            str = str.Replace(" ", "")
            GlobalVariables.ccode = str
            DsTreads = objcon.SpecificTreadRetrieval()
            If DsTreads.Tables(0).Rows.Count > 0 Then
                GlobalVariables.time = Convert.ToString(DsTreads.Tables(0).Rows(0).ItemArray(9))
                Dim timespan As TimeSpan
                timespan = DateTime.Now - Convert.ToDateTime(GlobalVariables.time)
                Dim intMinutes As Double = timespan.TotalMinutes
                'Dim days As Integer = timespan.Days
                'Dim span As TimeSpan = timespan.FromMinutes(intMinutes)

                'If days = 0 Then
                '    Dim answer As String = String.Format("{0:D2}H:{1:D2}M:{2:D2}S", span.Hours, span.Minutes, span.Seconds)
                '    GlobalVariables.timevaluehr = answer
                'Else
                '    Dim answer As String = String.Format("{0:D2}D:{1:D2}H:{2:D2}M:{3:D2}S", span.Days, span.Hours, span.Minutes, span.Seconds)
                '    GlobalVariables.timevaluehr = answer
                'End If

                'old GlobalVariables.selectTread = GlobalVariables.ccode
                GlobalVariables.selectTread = Convert.ToString(DsTreads.Tables(0).Rows(0).ItemArray(4))
                Dim count As Single = objcon.Check_Tread_Value()
                If count > 0 Then
                    If intMinutes < 181 Then

                        GlobalVariables.agevalue = "underaged"
                        GlobalVariables.idvalue = Convert.ToString(DsTreads.Tables(0).Rows(0).ItemArray(0))
                        ff = 180 - intMinutes
                        Dim days As Integer = timespan.Days
                        Dim span As TimeSpan = timespan.FromMinutes(ff)

                        If days = 0 Then
                            Dim answer As String = String.Format("{0:D2}H:{1:D2}M:{2:D2}S", span.Hours, span.Minutes, span.Seconds)
                            GlobalVariables.timevaluehr = answer
                        Else
                            Dim answer As String = String.Format("{0:D2}D:{1:D2}H:{2:D2}M:{3:D2}S", span.Days, span.Hours, span.Minutes, span.Seconds)
                            GlobalVariables.timevaluehr = answer
                        End If

                    ElseIf intMinutes > 180 AndAlso intMinutes < 8641 Then

                        GlobalVariables.agevalue = "aged"
                        GlobalVariables.idvalue = Convert.ToString(DsTreads.Tables(0).Rows(0).ItemArray(0))
                        ff = intMinutes - 180
                        Dim days As Integer = timespan.Days
                        Dim span As TimeSpan = timespan.FromMinutes(ff)

                        If days = 0 Then
                            Dim answer As String = String.Format("{0:D2}H:{1:D2}M:{2:D2}S", span.Hours, span.Minutes, span.Seconds)
                            GlobalVariables.timevaluehr = answer
                        Else
                            Dim answer As String = String.Format("{0:D2}D:{1:D2}H:{2:D2}M:{3:D2}S", span.Days, span.Hours, span.Minutes, span.Seconds)
                            GlobalVariables.timevaluehr = answer
                        End If

                    ElseIf intMinutes > 8640 AndAlso intMinutes < 10081 Then

                        GlobalVariables.agevalue = "usetoday"
                        GlobalVariables.idvalue = Convert.ToString(DsTreads.Tables(0).Rows(0).ItemArray(0))

                        Dim days As Integer = timespan.Days
                        Dim span As TimeSpan = timespan.FromMinutes(intMinutes)

                        If days = 0 Then
                            Dim answer As String = String.Format("{0:D2}H:{1:D2}M:{2:D2}S", span.Hours, span.Minutes, span.Seconds)
                            GlobalVariables.timevaluehr = answer
                        Else
                            Dim answer As String = String.Format("{0:D2}D:{1:D2}H:{2:D2}M:{3:D2}S", span.Days, span.Hours, span.Minutes, span.Seconds)
                            GlobalVariables.timevaluehr = answer
                        End If
                    Else

                        GlobalVariables.agevalue = "overaged"
                        GlobalVariables.idvalue = Convert.ToString(DsTreads.Tables(0).Rows(0).ItemArray(0))

                    End If

                    GlobalVariables.timevalue = intMinutes

                    Me.Close()
                    FrmDisplay.Show()

                Else
                    ' ageing 4 hour
                    If intMinutes < 241 Then
                        GlobalVariables.agevalue = "underaged"
                        GlobalVariables.idvalue = Convert.ToString(DsTreads.Tables(0).Rows(0).ItemArray(0))                       
                        ff = 240 - intMinutes
                        Dim days As Integer = timespan.Days
                        Dim span As TimeSpan = timespan.FromMinutes(ff)

                        If days = 0 Then
                            Dim answer As String = String.Format("{0:D2}H:{1:D2}M:{2:D2}S", span.Hours, span.Minutes, span.Seconds)
                            GlobalVariables.timevaluehr = answer
                        Else
                            Dim answer As String = String.Format("{0:D2}D:{1:D2}H:{2:D2}M:{3:D2}S", span.Days, span.Hours, span.Minutes, span.Seconds)
                            GlobalVariables.timevaluehr = answer
                        End If

                    ElseIf intMinutes > 240 AndAlso intMinutes < 8641 Then
                        GlobalVariables.agevalue = "aged"
                        GlobalVariables.idvalue = Convert.ToString(DsTreads.Tables(0).Rows(0).ItemArray(0))
                        ff = intMinutes - 240
                        Dim days As Integer = timespan.Days
                        Dim span As TimeSpan = timespan.FromMinutes(ff)

                        If days = 0 Then
                            Dim answer As String = String.Format("{0:D2}H:{1:D2}M:{2:D2}S", span.Hours, span.Minutes, span.Seconds)
                            GlobalVariables.timevaluehr = answer
                        Else
                            Dim answer As String = String.Format("{0:D2}D:{1:D2}H:{2:D2}M:{3:D2}S", span.Days, span.Hours, span.Minutes, span.Seconds)
                            GlobalVariables.timevaluehr = answer
                        End If

                    ElseIf intMinutes > 8640 AndAlso intMinutes < 10081 Then
                        GlobalVariables.agevalue = "usetoday"
                        GlobalVariables.idvalue = Convert.ToString(DsTreads.Tables(0).Rows(0).ItemArray(0))
                        Dim days As Integer = timespan.Days
                        Dim span As TimeSpan = timespan.FromMinutes(intMinutes)

                        If days = 0 Then
                            Dim answer As String = String.Format("{0:D2}H:{1:D2}M:{2:D2}S", span.Hours, span.Minutes, span.Seconds)
                            GlobalVariables.timevaluehr = answer
                        Else
                            Dim answer As String = String.Format("{0:D2}D:{1:D2}H:{2:D2}M:{3:D2}S", span.Days, span.Hours, span.Minutes, span.Seconds)
                            GlobalVariables.timevaluehr = answer
                        End If
                    Else
                        GlobalVariables.agevalue = "overaged"
                        GlobalVariables.idvalue = Convert.ToString(DsTreads.Tables(0).Rows(0).ItemArray(0))
                    End If
                    GlobalVariables.timevalue = intMinutes

                    Me.Close()
                    FrmDisplay.Show()

                End If
            Else
                MessageBox.Show("Not available....!!!!")
            End If
        Else
            MessageBox.Show("Please enter a colour code....!!!!")
        End If

        txtSearch.Text = ""
    End Sub

    Private Sub lblCode_Click(sender As Object, e As EventArgs) Handles lblCode.Click

    End Sub

    Private Sub txtSearch_TextChanged(sender As Object, e As EventArgs) Handles txtSearch.TextChanged

    End Sub
End Class