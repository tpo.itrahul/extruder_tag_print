﻿Imports QRCoder
Imports System.IO
Imports System.Drawing.Printing

Public Class lblTime
    Dim objcon As New Connections
    Public DSPkdid As New DataSet()
    Public DSpec As New DataSet()
    Public DsTruck As New DataSet()
    Public DsLeaftruck As New DataSet()
    Public dt As New DataTable()
    Public SelectedShift As String
    Public SelectedDate As DateTime
    Public SelectedprodDate As DateTime

    Private Property IsPostback As Boolean

    Public Sub DualTagPrint_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ' MessageBox.Show(GlobalVariables.SelectionTreadCode),
        Dim Dual = GlobalVariables.DualSelection
        lblDate123.Text = GlobalVariables.DualExactDate
        lblShift.Text = GlobalVariables.DualExactShift
        txtItemCode.Text = GlobalVariables.SelectionTreadCode
        txtColourCode.Text = GlobalVariables.ItemCode
        txtOperator.Text = GlobalVariables.Username
        LblDesc.Text = GlobalVariables.description


        frmScheduleSelection.Hide()
        'load truckno
        TruckNoLoad()

        lblDate0.Text = DateTime.Now.ToString("dd/MM/yyyy")
        Me.Text = DateTime.Now.ToString("hh:mm:ss")
        lblTime0.Text = DateTime.Now.ToString("HH:mm:ss tt")
        lblTime24.Text = DateTime.Now.ToString("HH:mm:ss")
        lblfinal.Text = DateTime.Now.ToString("hh:mm tt")



        ''   objprop.dualext = Session("dual").ToString()
        SelectedDate = Convert.ToDateTime(lblDate123.Text)
        GlobalVariables.txtSelectDate = Convert.ToDateTime(SelectedDate).ToString("yyyy/MM/dd")


        If txtColourCode.Text.StartsWith("SW") Then
            GlobalVariables.Defect_Type = "SW"
            LocationLoad()
            CodeLoadSW()
            lblCode.Text = "SW BOOK NO"
            Lblno.Text = "SW NO"
        Else
            GlobalVariables.Defect_Type = "other"
            LocationLoad()
            CodeLoad()
        End If

        CboReason.Items.Add("Air Under Cusion")
        CboReason.Items.Add("Defective Cushion")
        CboReason.Items.Add("Defective Skiving")
        CboReason.Items.Add("Depreciation")
        CboReason.Items.Add("Excess Width")
        CboReason.Items.Add("FM Condamination")
        CboReason.Items.Add("Length Variation")
        CboReason.Items.Add("Lumpy (Cap/Base/Cusion)")
        CboReason.Items.Add("Less Overall Width")
        CboReason.Items.Add("Mid Cap Offcenter")
        CboReason.Items.Add("No Central Line")
        CboReason.Items.Add("Online Scrap")
        CboReason.Items.Add("Over Weight")
        CboReason.Items.Add("Profile Variation")
        CboReason.Items.Add("TB Rejection")
        CboReason.Items.Add("Tech Trial")
        CboReason.Items.Add("Torn Edge")
        CboReason.Items.Add("Under Weight")
        CboReason.Items.Add("Wrong Identification")
        'CboReason.Items.Clear()
        CboReason.SelectedIndex = 0




    End Sub

    Public Sub CodeLoad()
        Dim dtab As New DataSet()
        Dim Dual = GlobalVariables.DualSelection
        dtab = objcon.GetCode()
        cboCode.DisplayMember = "LeafTruck_No"
        cboCode.DataSource = dtab.Tables(0)

    End Sub

    Public Sub CodeLoadSW()
        Dim dtab As New DataSet()
        dtab = objcon.GetCodeSW()
        cboCode.DisplayMember = "LeafTruck_No"
        cboCode.DataSource = dtab.Tables(0)
    End Sub
    Public Sub LocationLoad()
        Dim dtab1 As New DataSet()

        dtab1 = objcon.GetLocation()

        If dtab1.Tables(0).Rows.Count <= 0 Then
            CboLocation.Items.Add("NL")
        End If
        Dim dr As DataRow
        ' CboLocation.Items.Add("NL")
        For Each dr In dtab1.Tables(0).Rows
            CboLocation.Items.Add(dr("Location"))
        Next
    End Sub

    Public Sub TruckNoLoad()
        Dim maxtruck As Single
        Dim Dual = GlobalVariables.DualSelection

        maxtruck = objcon.Fetch_Recent_TruckNo(lblShift.Text, lblDate123.Text)
        'cbTruckNo.Items.Add(maxtruck)
        For i = maxtruck To 14
            cbTruckNo.Items.Add(i + 1)
        Next
    End Sub


    Private Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
        Dim maxtruck As String
        Dim Dual = GlobalVariables.DualSelection
        maxtruck = objcon.GetCodeuwb(Dual)
        Dim UWB_Tag_Id = maxtruck
        Dim leafTruck = objcon.GetCodeuwbTag(UWB_Tag_Id)
        Dim sList = leafTruck.ToString().TrimEnd()

        If cboCode.Text <> sList Then
            Dim result As DialogResult = MessageBox.Show("Wrong LeafTruck at Booking Area Please Check, Click YES to Continue NO to Cancel", "Warning", MessageBoxButtons.YesNo)
            If result = DialogResult.Yes Then
                If cboCode.Text.Trim() <> "" And CboLocation.Text.Trim() <> "" And txtQty.Text.Trim() <> "" And txtBooker.Text <> "" And cbTruckNo.Text <> "" Then

                    GlobalVariables.Qty = txtQty.Text
                    GlobalVariables.No = cbTruckNo.Text
                    Dim pkextid As Single = objcon.get_DextPKID()
                    GlobalVariables.DescId = pkextid
                    SelectedDate = Convert.ToDateTime(lblDate123.Text)
                    GlobalVariables.extdate = Convert.ToDateTime(SelectedDate).ToString("yyyy/MM/dd")
                    GlobalVariables.exttime = lblTime0.Text
                    lblhr.Text = Convert.ToDateTime(lblTime0.Text).ToString("hh")
                    GlobalVariables.Bias = lblhr.Text
                    GlobalVariables.extshift = lblShift.Text
                    GlobalVariables.curdate = Convert.ToDateTime(DateTime.Now).ToString("yyyy/MM/dd HH:mm:ss ")
                    GlobalVariables.Code = cboCode.Text
                    GlobalVariables.Location = CboLocation.Text
                    GlobalVariables.BookerNo = txtBooker.Text
                    GlobalVariables.T1 = cboCode.Text
                    GlobalVariables.T2 = CboLocation.Text
                    GlobalVariables.T3 = txtQty.Text
                    GlobalVariables.T4 = cbTruckNo.Text

                    Dim C1 As Single = objcon.Check_EntryCount()
                    ' If C1 <= 0 Then

                    'Dim Loc As Single = objcon.LocCount()
                    'If Loc <= 0 Then
                    '    MessageBox.Show("Location not Available")
                    '    Me.Close()
                    'Else
                    objcon.insertTreadEntery()
                        objcon.FalseUpdateLocation()

                        If txtColourCode.Text.StartsWith("SW") Then
                            objcon.FalseUpdateLeaftruckSW()
                        Else
                            objcon.FalseUpdateLeaftruck()
                        End If


                        If txtColourCode.Text.StartsWith("SW") Then
                            Dim pd1 As New PrintDocument
                            AddHandler pd1.PrintPage, AddressOf Me.PrintDocument2_PrintPage
                            pd1.PrintController = New StandardPrintController()

                            pd1.DefaultPageSettings.Landscape = False
                            pd1.PrinterSettings.Copies = "1"

                            pd1.Print()
                        Else
                            Dim pd As New PrintDocument
                            AddHandler pd.PrintPage, AddressOf Me.PrintDocument1_PrintPage
                            pd.PrintController = New StandardPrintController()

                            pd.DefaultPageSettings.Landscape = False
                            pd.PrinterSettings.Copies = "1"

                            pd.Print()

                        End If

                        'End If
                        'End If

                    Else
                    MessageBox.Show("Enter All Details")

                End If
            ElseIf result = DialogResult.No Then
                Clear()
                Me.Close()
                frmScheduleSelection.Show()
            End If
        ElseIf cboCode.Text.Trim() <> "" And CboLocation.Text.Trim() <> "" And txtQty.Text.Trim() <> "" And txtBooker.Text <> "" And cbTruckNo.Text <> "" Then


            GlobalVariables.Qty = txtQty.Text
                'GlobalVariables.No = txtNo.Text

                'TruckNo
                GlobalVariables.No = cbTruckNo.Text




                Dim pkextid As Single = objcon.get_DextPKID()
                GlobalVariables.DescId = pkextid


                SelectedDate = Convert.ToDateTime(lblDate123.Text)
                GlobalVariables.extdate = Convert.ToDateTime(SelectedDate).ToString("yyyy/MM/dd")

                GlobalVariables.exttime = lblTime0.Text
                lblhr.Text = Convert.ToDateTime(lblTime0.Text).ToString("hh")
                GlobalVariables.Bias = lblhr.Text
                GlobalVariables.extshift = lblShift.Text


                GlobalVariables.curdate = Convert.ToDateTime(DateTime.Now).ToString("yyyy/MM/dd HH:mm:ss ")

                GlobalVariables.Code = cboCode.Text
                GlobalVariables.Location = CboLocation.Text
                GlobalVariables.BookerNo = txtBooker.Text

                GlobalVariables.T1 = cboCode.Text
                GlobalVariables.T2 = CboLocation.Text
                GlobalVariables.T3 = txtQty.Text
                GlobalVariables.T4 = cbTruckNo.Text

                Dim C1 As Single = objcon.Check_EntryCount()
                If C1 <= 0 Then

                    'Dim Loc As Single = objcon.LocCount()
                    'If Loc <= 0 Then
                    '    MessageBox.Show("Location not Available")
                    '    Me.Close()
                    'Else
                    objcon.insertTreadEntery()
                    objcon.FalseUpdateLocation()

                    If txtColourCode.Text.StartsWith("SW") Then
                        objcon.FalseUpdateLeaftruckSW()
                    Else
                        objcon.FalseUpdateLeaftruck()
                    End If


                    If txtColourCode.Text.StartsWith("SW") Then
                        Dim pd1 As New PrintDocument
                        AddHandler pd1.PrintPage, AddressOf Me.PrintDocument2_PrintPage
                        pd1.PrintController = New StandardPrintController()

                        pd1.DefaultPageSettings.Landscape = False
                        pd1.PrinterSettings.Copies = "1"

                        pd1.Print()
                    Else
                        Dim pd As New PrintDocument
                        AddHandler pd.PrintPage, AddressOf Me.PrintDocument1_PrintPage
                        pd.PrintController = New StandardPrintController()

                        pd.DefaultPageSettings.Landscape = False
                        pd.PrinterSettings.Copies = "1"

                        pd.Print()

                    End If

                    'End If
                End If

            Else
                MessageBox.Show("Enter All Details")

            End If

        Clear()
        Me.Close()
        frmScheduleSelection.Show()

    End Sub
    Public Sub funtn()

    End Sub

    Private Sub Clear()
        GlobalVariables.Location = ""
        GlobalVariables.Code = ""
    End Sub
    Private Sub PrintDocument1_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage


        Lblcomp.Text = GlobalVariables.Splicer1 + "/" + GlobalVariables.Splicer2 + "/" + GlobalVariables.Splicer3





        Dim printFont As New Font("Bell MT Bold", 12.0F, FontStyle.Bold)
        Dim printFontSmall As New Font("Bell MT Bold", 8.0F, FontStyle.Bold)
        Dim printFontHead As New Font(FontFamily.GenericMonospace, 18.0F, FontStyle.Bold)
        Dim printFontHeadHigh As New Font(FontFamily.GenericMonospace, 26.0F, FontStyle.Bold)
        Dim printFontHeadHighest As New Font("Bell MT Bold", 36.0F, FontStyle.Bold)

        e.Graphics.DrawString(GlobalVariables.DescId, printFontSmall, Brushes.Black, 200, 50)

        Dim qrGenerator As QRCodeGenerator = New QRCodeGenerator()
        Dim qrCode As QRCodeGenerator.QRCode = qrGenerator.CreateQrCode(GlobalVariables.DescId, QRCodeGenerator.ECCLevel.H)
        'Dim qrCode As QRCodeGenerator.QRCode = qrGenerator.CreateQrCode("1003-8336", QRCodeGenerator.ECCLevel.H)


        Dim bitmap As Bitmap = qrCode.GetGraphic(2.5)
        Dim ms As MemoryStream = New MemoryStream()
        bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Png)



        Dim blackPen As New Pen(Color.Black, 3)

        ' Create points that define line.
        Dim x As Integer = 15
        Dim y As Integer = 130
        Dim width As Integer = 280
        Dim height As Integer = 40

        ' Draw rectangle to screen.
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)




        e.Graphics.DrawString("FT-01-B5:02", printFontSmall, Brushes.Black, 20, 500)
        e.Graphics.DrawString("TREAD", printFontHeadHighest, Brushes.Black, 20, 70)
        ' e.Graphics.DrawString("FT-01-B5:03", printFontSmall, Brushes.Black, 10, 480)


        e.Graphics.DrawImage(bitmap, 200, 60)
        e.Graphics.DrawString("CODE", printFont, Brushes.Black, 20, 140)
        '   e.Graphics.DrawString("CODE", printFont, Brushes.Black, 10, 140)

        e.Graphics.DrawString(txtColourCode.Text, printFont, Brushes.Black, 130, 140)

        x = 15
        y = 170
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString("TREAD", printFont, Brushes.Black, 20, 180)
        e.Graphics.DrawString(txtItemCode.Text, printFont, Brushes.Black, 130, 180)
        x = 15
        y = 210
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)



        e.Graphics.DrawString("NOS", printFont, Brushes.Black, 20, 220)
        e.Graphics.DrawString(txtQty.Text, printFont, Brushes.Black, 130, 220)
        e.Graphics.DrawString(GlobalVariables.DualSelection, printFont, Brushes.Black, 210, 220)

        x = 15
        y = 250
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)




        e.Graphics.DrawString("OPERATOR", printFont, Brushes.Black, 20, 260)
        e.Graphics.DrawString(txtOperator.Text & " / " & txtBooker.Text, printFont, Brushes.Black, 130, 260)

        x = 15
        y = 290
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString("DATE/SHIFT", printFont, Brushes.Black, 20, 300)
        e.Graphics.DrawString(GlobalVariables.DualExactDate & "--" & GlobalVariables.DualExactShift, printFont, Brushes.Black, 130, 300)
        x = 15
        y = 330
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString("TIME", printFont, Brushes.Black, 20, 340)
        e.Graphics.DrawString(lblfinal.Text, printFont, Brushes.Black, 130, 340)
        x = 15
        y = 370
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString("LOCATION", printFont, Brushes.Black, 20, 380)
        e.Graphics.DrawString(GlobalVariables.Location, printFont, Brushes.Black, 130, 380)

        x = 15
        y = 410
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)


        e.Graphics.DrawString("LT NO : ", printFont, Brushes.Black, 20, 420)
        e.Graphics.DrawString("TR NO : ", printFont, Brushes.Black, 140, 420)
        e.Graphics.DrawString(GlobalVariables.Code, printFont, Brushes.Black, 100, 420)
        e.Graphics.DrawString(cbTruckNo.Text, printFont, Brushes.Black, 220, 420)

        x = 15
        y = 450
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString("COMP :", printFont, Brushes.Black, 20, 460)
        e.Graphics.DrawString(Lblcomp.Text, printFont, Brushes.Black, 100, 460)

        x = 15
        y = 450

        e.Graphics.DrawRectangle(blackPen, x, y, width, height)




        Dim Line1 As New Point(130, 130)
        Dim Line2 As New Point(130, 410)

        ' Draw line to screen.
        e.Graphics.DrawLine(blackPen, Line1, Line2)

        Dim Line3 As New Point(210, 210)
        Dim Line4 As New Point(210, 250)

        ' Draw line to screen.
        e.Graphics.DrawLine(blackPen, Line3, Line4)


    End Sub

    Private Sub PrintDocument2_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument2.PrintPage


        Dim printFont As New Font("Bell MT Bold", 12.0F, FontStyle.Bold)
        Dim printFontSmall As New Font("Bell MT Bold", 8.0F, FontStyle.Bold)
        Dim printFontHead As New Font(FontFamily.GenericMonospace, 18.0F, FontStyle.Bold)
        Dim printFontHeadHigh As New Font(FontFamily.GenericMonospace, 26.0F, FontStyle.Bold)
        Dim printFontHeadHighest As New Font("Bell MT Bold", 36.0F, FontStyle.Bold)

        e.Graphics.DrawString(GlobalVariables.DescId, printFontSmall, Brushes.Black, 200, 45)

        Dim qrGenerator As QRCodeGenerator = New QRCodeGenerator()
        Dim qrCode As QRCodeGenerator.QRCode = qrGenerator.CreateQrCode(GlobalVariables.DescId, QRCodeGenerator.ECCLevel.H)
        'Dim qrCode As QRCodeGenerator.QRCode = qrGenerator.CreateQrCode("1003-8336", QRCodeGenerator.ECCLevel.H)


        Dim bitmap As Bitmap = qrCode.GetGraphic(2.5)
        Dim ms As MemoryStream = New MemoryStream()
        bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Png)



        Dim blackPen As New Pen(Color.Black, 3)

        ' Create points that define line.
        Dim x As Integer = 15
        Dim y As Integer = 130
        Dim width As Integer = 290
        Dim height As Integer = 40

        ' Draw rectangle to screen.
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)




        e.Graphics.DrawString("FT-01-B5:03", printFontSmall, Brushes.Black, 20, 500)
        e.Graphics.DrawString("SIDE WALL", printFontHead, Brushes.Black, 10, 80)
        ' e.Graphics.DrawString("FT-01-B5:03", printFontSmall, Brushes.Black, 10, 480)


        e.Graphics.DrawImage(bitmap, 200, 60)
        e.Graphics.DrawString("CODE", printFont, Brushes.Black, 20, 140)
        '   e.Graphics.DrawString("CODE", printFont, Brushes.Black, 10, 140)

        e.Graphics.DrawString(txtColourCode.Text, printFont, Brushes.Black, 130, 140)

        x = 15
        y = 170
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString("SIDEWALL", printFont, Brushes.Black, 20, 180)
        e.Graphics.DrawString(txtItemCode.Text, printFont, Brushes.Black, 130, 180)
        x = 15
        y = 210
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)


        e.Graphics.DrawString("NOS", printFont, Brushes.Black, 20, 220)
        e.Graphics.DrawString(txtQty.Text, printFont, Brushes.Black, 130, 220)
        e.Graphics.DrawString(GlobalVariables.DualSelection, printFont, Brushes.Black, 210, 220)

        x = 15
        y = 250
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString("OPERATOR", printFont, Brushes.Black, 20, 260)
        e.Graphics.DrawString(txtOperator.Text & " / " & txtBooker.Text, printFont, Brushes.Black, 130, 260)

        x = 15
        y = 290
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString("DATE/SHIFT", printFont, Brushes.Black, 20, 300)
        e.Graphics.DrawString(GlobalVariables.DualExactDate & "--" & GlobalVariables.DualExactShift, printFont, Brushes.Black, 130, 300)
        x = 15
        y = 330
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString("TIME", printFont, Brushes.Black, 20, 340)
        e.Graphics.DrawString(lblfinal.Text, printFont, Brushes.Black, 130, 340)
        x = 15
        y = 370
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString("LOCATION", printFont, Brushes.Black, 20, 380)
        e.Graphics.DrawString(CboLocation.Text, printFont, Brushes.Black, 130, 380)
        x = 15
        y = 410
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString("M/C NO", printFont, Brushes.Black, 20, 420)
        e.Graphics.DrawString(GlobalVariables.DualSelection, printFont, Brushes.Black, 130, 420)
        x = 15
        y = 450
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString("SWB NO : ", printFont, Brushes.Black, 20, 460)
        e.Graphics.DrawString("SL NO : ", printFont, Brushes.Black, 140, 460)
        e.Graphics.DrawString(GlobalVariables.Code, printFont, Brushes.Black, 100, 460)
        e.Graphics.DrawString(cbTruckNo.Text, printFont, Brushes.Black, 220, 460)

        Dim Line1 As New Point(130, 130)
        Dim Line2 As New Point(130, 450)

        '' Draw line to screen.
        e.Graphics.DrawLine(blackPen, Line1, Line2)











    End Sub

    Private Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Me.Close()
        frmScheduleSelection.Show()
    End Sub

    Private Sub btnDel_Click(sender As Object, e As EventArgs) Handles btnDel.Click
        GlobalVariables.Size = txtItemCode.Text
        btnPrint.Refresh()
        Me.Refresh()
        DeleteDetails.Show()
    End Sub

    Private Sub BtnHold_Click(sender As Object, e As EventArgs) Handles BtnHold.Click
        Dim maxtruck As String
        Dim Dual = GlobalVariables.DualSelection

        maxtruck = objcon.GetCodeuwbhold(Dual)
        Dim UWB_Tag_Id = maxtruck
        Dim leafTruck = objcon.GetCodeuwbTag(UWB_Tag_Id)
        Dim sList = leafTruck.ToString().TrimEnd()
        If cboCode.Text <> sList Then
            Dim result As DialogResult = MessageBox.Show("Wrong LeafTruck at Scrap Area Please Check, Click YES to Continue NO to Cancel", "Warning", MessageBoxButtons.YesNo)
            If result = DialogResult.Yes Then
                If cboCode.Text.Trim() <> "" And CboLocation.Text.Trim() <> "" And txtQty.Text.Trim() <> "" And cbTruckNo.Text <> "" Then


                    GlobalVariables.Qty = txtQty.Text
                    GlobalVariables.No = cbTruckNo.Text

                    Dim pkextid As Single = objcon.get_DextPKID()
                    GlobalVariables.DescId = pkextid
                    GlobalVariables.id = pkextid

                    Dim hid As Single = objcon.get_DextholdID()
                    GlobalVariables.HId = hid

                    SelectedDate = Convert.ToDateTime(lblDate123.Text)
                    GlobalVariables.extdate = Convert.ToDateTime(SelectedDate).ToString("yyyy/MM/dd")

                    GlobalVariables.exttime = lblTime0.Text
                    lblhr.Text = Convert.ToDateTime(lblTime0.Text).ToString("hh")
                    GlobalVariables.Bias = lblhr.Text
                    GlobalVariables.extshift = lblShift.Text


                    GlobalVariables.curdate = Convert.ToDateTime(DateTime.Now).ToString("yyyy/MM/dd HH:mm:ss ")

                    GlobalVariables.Code = cboCode.Text
                    GlobalVariables.Location = CboLocation.Text
                    GlobalVariables.BookerNo = txtBooker.Text

                    GlobalVariables.T1 = cboCode.Text
                    GlobalVariables.T2 = CboLocation.Text
                    GlobalVariables.T3 = txtQty.Text
                    GlobalVariables.T4 = cbTruckNo.Text



                    Dim C1 As Single = objcon.Check_EntryCount()
                    'If C1 <= 0 Then

                    objcon.insertTreadEntery()
                        objcon.FalseUpdateLocation()
                        objcon.FalseUpdateLeaftruck()


                        dt = objcon.GetDetails().Tables(0)
                        If dt.Rows.Count > 0 Then
                            GlobalVariables.extdate = Convert.ToDateTime(dt.Rows(0).ItemArray(1)).ToString("yyyy/MM/dd")
                            GlobalVariables.exttime = Convert.ToDateTime(dt.Rows(0).ItemArray(2)).ToString("HH:mm:ss tt")
                            GlobalVariables.Press = Convert.ToDateTime(dt.Rows(0).ItemArray(2)).ToString("hh:mm tt")
                            GlobalVariables.extshift = Convert.ToString(dt.Rows(0).ItemArray(3))
                            GlobalVariables.SelectionTreadCode = Convert.ToString(dt.Rows(0).ItemArray(4))
                            GlobalVariables.description = Convert.ToString(dt.Rows(0).ItemArray(5))
                            GlobalVariables.Code = Convert.ToString(dt.Rows(0).ItemArray(6))
                            GlobalVariables.Qty = Convert.ToString(dt.Rows(0).ItemArray(7))
                            GlobalVariables.Location = Convert.ToString(dt.Rows(0).ItemArray(8))
                            GlobalVariables.curdate = Convert.ToDateTime(dt.Rows(0).ItemArray(9)).ToString("yyyy/MM/dd HH:mm:ss")
                            GlobalVariables.No = Convert.ToString(dt.Rows(0).ItemArray(22))
                            GlobalVariables.ItemCode = Convert.ToString(dt.Rows(0).ItemArray(19))
                            GlobalVariables.DualSelection = Convert.ToString(dt.Rows(0).ItemArray(20))
                            GlobalVariables.Username = Convert.ToString(dt.Rows(0).ItemArray(21))
                        End If

                        GlobalVariables.HEmpNo = txtOperator.Text
                        GlobalVariables.HEmpName = txtOperator.Text
                        GlobalVariables.HBy = txtOperator.Text
                        GlobalVariables.HReason = CboReason.Text
                        GlobalVariables.HDate = Convert.ToDateTime(DateTime.Now).ToString("yyyy/MM/dd")
                        GlobalVariables.HShift = lblShift.Text

                        objcon.insertTreadHoldEntery()
                        objcon.insertTreadHoldStatus()

                        If txtColourCode.Text.StartsWith("SW") Then
                            'Dim pd1 As New PrintDocument
                            'AddHandler pd1.PrintPage, AddressOf Me.PrintDocument2_PrintPage
                            'pd1.PrintController = New StandardPrintController()

                            'pd1.DefaultPageSettings.Landscape = False
                            'pd1.PrinterSettings.Copies = "1"

                            'pd1.Print()
                        Else
                            Dim pd As New PrintDocument
                            AddHandler pd.PrintPage, AddressOf Me.PrintDocument3_PrintPage
                            pd.PrintController = New StandardPrintController()

                            pd.DefaultPageSettings.Landscape = False
                            pd.PrinterSettings.Copies = "1"

                            pd.Print()

                        End If


                        'btnPrint.Enabled = True
                        'btnPrint.BackColor = Color.Yellow


                        CboLocation.Text = ""
                        CboLocation.Items.Clear()
                        'cboCode.Items.Clear()
                        CodeLoad()
                        LocationLoad()
                        txtQty.Text = ""
                        txtNo.Text = ""
                        Me.Refresh()

                        ' End If
                        'End If

                    Else
                    MessageBox.Show("Enter All Details")

                End If
            ElseIf result = DialogResult.No Then
                Clear()
                Me.Close()
                frmScheduleSelection.Show()
            End If
        ElseIf cboCode.Text.Trim() <> "" And CboLocation.Text.Trim() <> "" And txtQty.Text.Trim() <> "" And cbTruckNo.Text <> "" Then


            GlobalVariables.Qty = txtQty.Text
            GlobalVariables.No = cbTruckNo.Text

            Dim pkextid As Single = objcon.get_DextPKID()
            GlobalVariables.DescId = pkextid
            GlobalVariables.id = pkextid

            Dim hid As Single = objcon.get_DextholdID()
            GlobalVariables.HId = hid

            SelectedDate = Convert.ToDateTime(lblDate123.Text)
            GlobalVariables.extdate = Convert.ToDateTime(SelectedDate).ToString("yyyy/MM/dd")

            GlobalVariables.exttime = lblTime0.Text
            lblhr.Text = Convert.ToDateTime(lblTime0.Text).ToString("hh")
            GlobalVariables.Bias = lblhr.Text
            GlobalVariables.extshift = lblShift.Text


            GlobalVariables.curdate = Convert.ToDateTime(DateTime.Now).ToString("yyyy/MM/dd HH:mm:ss ")

            GlobalVariables.Code = cboCode.Text
            GlobalVariables.Location = CboLocation.Text
            GlobalVariables.BookerNo = txtBooker.Text

            GlobalVariables.T1 = cboCode.Text
            GlobalVariables.T2 = CboLocation.Text
            GlobalVariables.T3 = txtQty.Text
            GlobalVariables.T4 = cbTruckNo.Text



            Dim C1 As Single = objcon.Check_EntryCount()
            If C1 <= 0 Then

                objcon.insertTreadEntery()
                objcon.FalseUpdateLocation()
                objcon.FalseUpdateLeaftruck()


                dt = objcon.GetDetails().Tables(0)
                If dt.Rows.Count > 0 Then
                    GlobalVariables.extdate = Convert.ToDateTime(dt.Rows(0).ItemArray(1)).ToString("yyyy/MM/dd")
                    GlobalVariables.exttime = Convert.ToDateTime(dt.Rows(0).ItemArray(2)).ToString("HH:mm:ss tt")
                    GlobalVariables.Press = Convert.ToDateTime(dt.Rows(0).ItemArray(2)).ToString("hh:mm tt")
                    GlobalVariables.extshift = Convert.ToString(dt.Rows(0).ItemArray(3))
                    GlobalVariables.SelectionTreadCode = Convert.ToString(dt.Rows(0).ItemArray(4))
                    GlobalVariables.description = Convert.ToString(dt.Rows(0).ItemArray(5))
                    GlobalVariables.Code = Convert.ToString(dt.Rows(0).ItemArray(6))
                    GlobalVariables.Qty = Convert.ToString(dt.Rows(0).ItemArray(7))
                    GlobalVariables.Location = Convert.ToString(dt.Rows(0).ItemArray(8))
                    GlobalVariables.curdate = Convert.ToDateTime(dt.Rows(0).ItemArray(9)).ToString("yyyy/MM/dd HH:mm:ss")
                    GlobalVariables.No = Convert.ToString(dt.Rows(0).ItemArray(22))
                    GlobalVariables.ItemCode = Convert.ToString(dt.Rows(0).ItemArray(19))
                    GlobalVariables.DualSelection = Convert.ToString(dt.Rows(0).ItemArray(20))
                    GlobalVariables.Username = Convert.ToString(dt.Rows(0).ItemArray(21))
                End If

                GlobalVariables.HEmpNo = txtOperator.Text
                GlobalVariables.HEmpName = txtOperator.Text
                GlobalVariables.HBy = txtOperator.Text
                GlobalVariables.HReason = CboReason.Text
                GlobalVariables.HDate = Convert.ToDateTime(DateTime.Now).ToString("yyyy/MM/dd")
                GlobalVariables.HShift = lblShift.Text

                objcon.insertTreadHoldEntery()
                objcon.insertTreadHoldStatus()

                If txtColourCode.Text.StartsWith("SW") Then
                    'Dim pd1 As New PrintDocument
                    'AddHandler pd1.PrintPage, AddressOf Me.PrintDocument2_PrintPage
                    'pd1.PrintController = New StandardPrintController()

                    'pd1.DefaultPageSettings.Landscape = False
                    'pd1.PrinterSettings.Copies = "1"

                    'pd1.Print()
                Else
                    Dim pd As New PrintDocument
                    AddHandler pd.PrintPage, AddressOf Me.PrintDocument3_PrintPage
                    pd.PrintController = New StandardPrintController()

                    pd.DefaultPageSettings.Landscape = False
                    pd.PrinterSettings.Copies = "1"

                    pd.Print()

                End If


                'btnPrint.Enabled = True
                'btnPrint.BackColor = Color.Yellow


                CboLocation.Text = ""
                CboLocation.Items.Clear()
                'cboCode.Items.Clear()
                CodeLoad()
                LocationLoad()
                txtQty.Text = ""
                txtNo.Text = ""
                Me.Refresh()

            End If
            'End If

        Else
            MessageBox.Show("Enter All Details")

        End If
        Clear()
        Me.Close()
        frmScheduleSelection.Show()


    End Sub

    Private Sub PrintDocument3_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument3.PrintPage



        ''   Lblcomp.Text = Convert.ToString(ddt.Rows(0).ItemArray(8)) + "/" + Convert.ToString(ddt.Rows(0).ItemArray(9)) + "/" + Convert.ToString(ddt.Rows(0).ItemArray(10))



        Dim printFont As New Font("Bell MT Bold", 12.0F, FontStyle.Bold)
        Dim printFontSmall As New Font("Bell MT Bold", 8.0F, FontStyle.Bold)
        Dim printFontHead As New Font(FontFamily.GenericMonospace, 18.0F, FontStyle.Bold)
        Dim printFontHeadHigh As New Font(FontFamily.GenericMonospace, 26.0F, FontStyle.Bold)
        Dim printFontHeadHighest As New Font("Bell MT Bold", 36.0F, FontStyle.Bold)

        e.Graphics.DrawString(GlobalVariables.HId, printFontSmall, Brushes.Black, 240, 30)


        ''  e.Graphics.TranslateTransform(0, 200)
        ''  e.Graphics.RotateTransform(If(e.PageSettings.Landscape, 30, 60))

        e.Graphics.RotateTransform(50) 'rotate 45° clockwise
        Dim myfont As New Font("Arial", 80)
        Dim myBrush As New SolidBrush(Color.FromArgb(30, 0, 0, 0))
        e.Graphics.DrawString("HOLD", myfont, myBrush, 110, 0) 'this location is relative to the point of rotation
        e.Graphics.ResetTransform()

        'e.Graphics.RotateTransform(0) 'rotate 45° clockwise
        'Dim myfont1 As New Font("Arial", 150)
        'Dim myBrush1 As New SolidBrush(Color.FromArgb(100, 0, 0, 255))
        'e.Graphics.DrawString("X", myfont, myBrush, 120, 120) 'this location is relative to the point of rotation
        'e.Graphics.ResetTransform()



        Dim qrGenerator As QRCodeGenerator = New QRCodeGenerator()
        Dim qrCode As QRCodeGenerator.QRCode = qrGenerator.CreateQrCode(GlobalVariables.HId, QRCodeGenerator.ECCLevel.H)


        Dim bitmap As Bitmap = qrCode.GetGraphic(2.5)
        Dim ms As MemoryStream = New MemoryStream()
        bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Png)



        Dim blackPen As New Pen(Color.Black, 3)

        ' Create points that define line.
        Dim x As Integer = 15
        Dim y As Integer = 130
        Dim width As Integer = 280
        Dim height As Integer = 40

        ' Draw rectangle to screen.
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)



        e.Graphics.DrawString("FT-01-B5:02", printFontSmall, Brushes.Black, 20, 500)
        e.Graphics.DrawString("HOLD TREAD", printFontHeadHigh, Brushes.Black, 10, 80)
        ' e.Graphics.DrawString("FT-01-B5:03", printFontSmall, Brushes.Black, 10, 480)


        e.Graphics.DrawImage(bitmap, 180, 20)
        e.Graphics.DrawString("CODE", printFont, Brushes.Black, 20, 140)
        '   e.Graphics.DrawString("CODE", printFont, Brushes.Black, 10, 140)

        e.Graphics.DrawString(GlobalVariables.ItemCode, printFont, Brushes.Black, 130, 140)

        x = 15
        y = 170
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString("TREAD", printFont, Brushes.Black, 20, 180)
        e.Graphics.DrawString(GlobalVariables.SelectionTreadCode, printFont, Brushes.Black, 130, 180)
        x = 15
        y = 210
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)


        e.Graphics.DrawString("NOS", printFont, Brushes.Black, 20, 220)
        e.Graphics.DrawString(GlobalVariables.Qty, printFont, Brushes.Black, 130, 220)
        e.Graphics.DrawString(GlobalVariables.DualSelection, printFont, Brushes.Black, 210, 220)

        x = 15
        y = 250
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString("E DATE/SHIFT", printFont, Brushes.Black, 20, 260)
        e.Graphics.DrawString(GlobalVariables.extdate & "--" & GlobalVariables.extshift, printFont, Brushes.Black, 130, 260)

        x = 15
        y = 290
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString("LOCATION", printFont, Brushes.Black, 20, 300)
        e.Graphics.DrawString(GlobalVariables.Location, printFont, Brushes.Black, 130, 300)
        x = 15
        y = 330
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString("H DATE/SHIFT", printFont, Brushes.Black, 20, 340)
        e.Graphics.DrawString(GlobalVariables.HDate & "--" & GlobalVariables.HShift, printFont, Brushes.Black, 130, 340)
        x = 15
        y = 370
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString(GlobalVariables.HEmpNo, printFont, Brushes.Black, 20, 380)
        e.Graphics.DrawString(GlobalVariables.HEmpName & "/" & GlobalVariables.HBy, printFont, Brushes.Black, 130, 380)

        x = 15
        y = 410
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)


        e.Graphics.DrawString("LT NO : ", printFont, Brushes.Black, 20, 420)
        e.Graphics.DrawString("TR NO : ", printFont, Brushes.Black, 140, 420)
        e.Graphics.DrawString(GlobalVariables.Code, printFont, Brushes.Black, 100, 420)
        e.Graphics.DrawString(GlobalVariables.No, printFont, Brushes.Black, 220, 420)

        x = 15
        y = 450
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString("Rsn:", printFont, Brushes.Black, 20, 460)
        e.Graphics.DrawString(GlobalVariables.HReason, printFont, Brushes.Black, 70, 460)

        x = 15
        y = 450

        e.Graphics.DrawRectangle(blackPen, x, y, width, height)




        Dim Line1 As New Point(130, 130)
        Dim Line2 As New Point(130, 410)

        ' Draw line to screen.
        e.Graphics.DrawLine(blackPen, Line1, Line2)

        Dim Line3 As New Point(210, 210)
        Dim Line4 As New Point(210, 250)

        ' Draw line to screen.
        e.Graphics.DrawLine(blackPen, Line3, Line4)


        Dim Line5 As New Point(0, 0)
        Dim Line6 As New Point(300, 500)

        ' Draw line to screen.
        e.Graphics.DrawLine(blackPen, Line5, Line6)

        Dim Line7 As New Point(0, 500)
        Dim Line8 As New Point(300, 0)

        ' Draw line to screen.
        e.Graphics.DrawLine(blackPen, Line7, Line8)

    End Sub

    Private Sub cbTruckNo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbTruckNo.SelectedIndexChanged

    End Sub

    Private Sub Panel3_Paint(sender As Object, e As PaintEventArgs) Handles Panel3.Paint

    End Sub



    'If (CboReason.Items.Count > 0) Then
    'Dim result As DialogResult = MessageBox.Show("Are you sure you want add material to Hold", "Hold", MessageBoxButtons.YesNoCancel)
    'If result = DialogResult.Cancel Then

    'ElseIf result = DialogResult.No Then
    '            MessageBox.Show("No pressed")
    '        ElseIf result = DialogResult.Yes Then
    '            MessageBox.Show("Yes pressed")
    '        End If
    'ElseIf (CboReason.Items.Count > 0) Then

    'End If
End Class