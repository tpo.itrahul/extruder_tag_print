﻿Public Class Login
    Dim objcon As New Connections
    Public ds As New DataSet()

    Private Sub btnLogin_Click(sender As Object, e As EventArgs) Handles btnLogin.Click
        If txtName.Text = "" Then
            MessageBox.Show("Username Missing")
        End If
        If txtPassword.Text = "" Then
            MessageBox.Show("Password Missing")
        End If
        If txtName.Text <> "" And txtPassword.Text <> "" Then
            GlobalVariables.Username = txtName.Text
            GlobalVariables.Password = txtPassword.Text



            Dim count As Single = objcon.Check_Login()
            If count > 0 Then
                ds = objcon.userData()
                GlobalVariables.usertype = Convert.ToString(ds.Tables(0).Rows(0).ItemArray(3))
                GlobalVariables.Name = Convert.ToString(ds.Tables(0).Rows(0).ItemArray(2))
                '   If GlobalVariables.Username = "admin" And GlobalVariables.Password = "admin" And GlobalVariables.usertype = "admin" Then

                If GlobalVariables.usertype = "DivA" Then
                    Me.Hide()
                    FrmSelection.Show()
                ElseIf GlobalVariables.Username = "user" And GlobalVariables.Password = "user" And GlobalVariables.usertype = "user" Then
                    Me.Hide()
                    FrmTakentoTB.Show()
                ElseIf GlobalVariables.usertype = "Technical" Or GlobalVariables.usertype = "Quality" Then
                    Me.Hide()
                    FrmTech_Qa.Show()
                ElseIf GlobalVariables.usertype = "TB" Then
                    Me.Hide()
                    TBSelection.Show()
                Else
                    frmDualSelecion.Show()
                    Me.Hide()
                End If
            Else
                MessageBox.Show("Invalid User")
                txtName.Text = ""
                txtPassword.Text = ""
            End If

        End If


    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        txtName.Text = ""
        txtPassword.Text = ""
    End Sub
End Class