﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class TBSelection
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.BtnDisplay = New System.Windows.Forms.Button()
        Me.btnCurrent = New System.Windows.Forms.Button()
        Me.btntbReport = New System.Windows.Forms.Button()
        Me.btnDelHist = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'BtnDisplay
        '
        Me.BtnDisplay.BackColor = System.Drawing.Color.LightSeaGreen
        Me.BtnDisplay.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnDisplay.Location = New System.Drawing.Point(336, 217)
        Me.BtnDisplay.Name = "BtnDisplay"
        Me.BtnDisplay.Size = New System.Drawing.Size(260, 65)
        Me.BtnDisplay.TabIndex = 92
        Me.BtnDisplay.Text = "Display"
        Me.BtnDisplay.UseVisualStyleBackColor = False
        '
        'btnCurrent
        '
        Me.btnCurrent.BackColor = System.Drawing.Color.LightSeaGreen
        Me.btnCurrent.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCurrent.Location = New System.Drawing.Point(27, 106)
        Me.btnCurrent.Name = "btnCurrent"
        Me.btnCurrent.Size = New System.Drawing.Size(260, 69)
        Me.btnCurrent.TabIndex = 91
        Me.btnCurrent.Text = "Current Stock"
        Me.btnCurrent.UseVisualStyleBackColor = False
        '
        'btntbReport
        '
        Me.btntbReport.BackColor = System.Drawing.Color.LightSeaGreen
        Me.btntbReport.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btntbReport.Location = New System.Drawing.Point(327, 106)
        Me.btntbReport.Name = "btntbReport"
        Me.btntbReport.Size = New System.Drawing.Size(269, 69)
        Me.btntbReport.TabIndex = 90
        Me.btntbReport.Text = "Taken To TB Report"
        Me.btntbReport.UseVisualStyleBackColor = False
        '
        'btnDelHist
        '
        Me.btnDelHist.BackColor = System.Drawing.Color.LightSeaGreen
        Me.btnDelHist.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelHist.Location = New System.Drawing.Point(27, 213)
        Me.btnDelHist.Name = "btnDelHist"
        Me.btnDelHist.Size = New System.Drawing.Size(260, 69)
        Me.btnDelHist.TabIndex = 93
        Me.btnDelHist.Text = "Delete History"
        Me.btnDelHist.UseVisualStyleBackColor = False
        '
        'TBSelection
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightCoral
        Me.ClientSize = New System.Drawing.Size(622, 388)
        Me.Controls.Add(Me.btnDelHist)
        Me.Controls.Add(Me.BtnDisplay)
        Me.Controls.Add(Me.btnCurrent)
        Me.Controls.Add(Me.btntbReport)
        Me.Name = "TBSelection"
        Me.Text = "TBSelection"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents BtnDisplay As System.Windows.Forms.Button
    Friend WithEvents btnCurrent As System.Windows.Forms.Button
    Friend WithEvents btntbReport As System.Windows.Forms.Button
    Friend WithEvents btnDelHist As System.Windows.Forms.Button
End Class
