﻿Imports Excel = Microsoft.Office.Interop.Excel

Imports Microsoft.Office
Imports System.Text
Imports System.IO
Imports System.Windows.Forms

Imports System.Data.SqlClient
Imports System.Data
Imports System.Drawing
Public Class FrmProd_Report
    Dim DTSpec As DataTable
    Dim objcon As New Connections
    Dim DualExactDate As Date
    Dim DualExactShift As String
    Public ds As New DataSet()
    Dim DtStock As DataTable
    Dim rwcnt As Integer = 0
    Public dt As New DataTable()

    Dim excelLocation As String
    Dim APP As New Excel.Application
    Dim worksheet As Excel.Worksheet
    Dim workbook As Excel.Workbook

    Private Sub BtnSubmit_Click(sender As Object, e As EventArgs) Handles BtnSubmit.Click
        If TDate.Text <> "" And Fdate.Text <> "" Then



            GlobalVariables.FromDate = Fdate.Text
            GlobalVariables.ToDate = TDate.Text

            dt.Rows.Clear()
            dt.Columns.Clear()
            rwcnt = 0
            DataGridView1.Visible = True

            dt.Columns.Add("PKDEXTID", GetType(String))
            dt.Columns.Add("EXT_DATE", GetType(String))
            dt.Columns.Add("EXT_DATETIME", GetType(String))
            dt.Columns.Add("SHIFT", GetType(String))
            dt.Columns.Add("TREADCODE", GetType(String))
            dt.Columns.Add("Tread_Code", GetType(String))
            dt.Columns.Add("TREAD_DESCRIPTION", GetType(String))
            dt.Columns.Add("LEAF_TRUCKNO", GetType(String))
            dt.Columns.Add("TREADS", GetType(String))
            dt.Columns.Add("LOCATION", GetType(String))
            dt.Columns.Add("Dual_Ext", GetType(String))
            dt.Columns.Add("Age", GetType(String))

            ds = objcon.GETProductionData()

            If ds.Tables(0).Rows.Count > 0 Then

                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1

                    Dim dr As DataRow = dt.NewRow()

                    dt.Rows.Add(dr)
                    dt.Rows(rwcnt)(0) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(0))
                    dt.Rows(rwcnt)(1) = Convert.ToDateTime(ds.Tables(0).Rows(i).ItemArray(1)).ToString("dd/MM/yyyy")
                    dt.Rows(rwcnt)(2) = Convert.ToDateTime(ds.Tables(0).Rows(i).ItemArray(9)).ToString("dd/MM/yyyy HH:mm:ss tt")
                    dt.Rows(rwcnt)(3) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(3))
                    dt.Rows(rwcnt)(4) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(4))
                    dt.Rows(rwcnt)(5) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(19))
                    dt.Rows(rwcnt)(6) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(5))
                    dt.Rows(rwcnt)(7) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(6))
                    dt.Rows(rwcnt)(8) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(7))
                    dt.Rows(rwcnt)(9) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(8))
                    dt.Rows(rwcnt)(10) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(20))


                    GlobalVariables.time = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(9))
                    Dim timespan As TimeSpan
                    timespan = DateTime.Now - Convert.ToDateTime(GlobalVariables.time)
                    Dim intMinutes As Double = timespan.TotalMinutes

                    GlobalVariables.selectTread = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(19))

                    Dim count As Single = objcon.Check_Tread_Value()

                    If count > 0 Then
                        ' 3 hour in list
                        If intMinutes < 181 Then

                            GlobalVariables.agevalue = "Underaged"


                        ElseIf intMinutes > 180 AndAlso intMinutes < 8641 Then

                            GlobalVariables.agevalue = "Aged"

                        ElseIf intMinutes > 8640 AndAlso intMinutes < 10081 Then

                            GlobalVariables.agevalue = "Usetoday"
                        Else

                            GlobalVariables.agevalue = "Overaged"

                        End If
                    Else
                        ' 4 hour
                        If intMinutes < 241 Then

                            GlobalVariables.agevalue = "Underaged"

                        ElseIf intMinutes > 240 AndAlso intMinutes < 8641 Then

                            GlobalVariables.agevalue = "Aged"

                        ElseIf intMinutes > 8640 AndAlso intMinutes < 10081 Then

                            GlobalVariables.agevalue = "Usetoday"
                        Else

                            GlobalVariables.agevalue = "Overaged"


                        End If

                    End If

                    dt.Rows(rwcnt)(11) = Convert.ToString(GlobalVariables.agevalue)
                    rwcnt += 1


                Next

                DataGridView1.DataSource = dt
                Dim cc As String
                For k As Integer = 0 To DataGridView1.Rows.Count - 2
                    cc = DataGridView1.Rows(k).Cells(11).Value.ToString
                    If cc = "Underaged" Then
                        DataGridView1.Rows(k).Cells(11).Style.BackColor = Color.Red
                        DataGridView1.Rows(k).Cells(11).Style.ForeColor = Color.White
                    ElseIf cc = "Aged" Then
                        DataGridView1.Rows(k).Cells(11).Style.BackColor = Color.Green
                        DataGridView1.Rows(k).Cells(11).Style.ForeColor = Color.White
                    ElseIf cc = "Usetoday" Then
                        DataGridView1.Rows(k).Cells(11).Style.BackColor = Color.Yellow
                        DataGridView1.Rows(k).Cells(11).Style.ForeColor = Color.Black
                    ElseIf cc = "Overaged" Then
                        DataGridView1.Rows(k).Cells(11).Style.BackColor = Color.Blue
                        DataGridView1.Rows(k).Cells(11).Style.ForeColor = Color.White
                    Else

                    End If
                Next
                Me.DataGridView1.DefaultCellStyle.Font = New Font("Times New Roman", 14)

                Me.DataGridView1.ColumnHeadersDefaultCellStyle.Font = New Font("Times New Roman", 17)


                For i = 0 To DataGridView1.Rows.Count - 1
                    Dim r As DataGridViewRow = DataGridView1.Rows(i)
                    r.Height = 40
                Next
            Else
                MessageBox.Show("No data Found !!!")
            End If

        Else
            MessageBox.Show("Enter All Details !!!")
        End If

    End Sub

    Private Sub btnExport_Click(sender As Object, e As EventArgs) Handles btnExport.Click
        If DataGridView1.Rows.Count <= 2 Then
            MsgBox("Nothing to Export")
            Exit Sub

        End If
        Dim saveDlg As New System.Windows.Forms.SaveFileDialog()

        saveDlg.InitialDirectory = "C:\"
        saveDlg.Filter = "Excel files (*.xlsx)|*.xlsx"
        saveDlg.FilterIndex = 0
        saveDlg.RestoreDirectory = True
        saveDlg.Title = "Export Excel File To"

        If saveDlg.ShowDialog() = System.Windows.Forms.DialogResult.OK Then

            workbook = APP.Workbooks.Open("c:\Temp\File.xlsx")
            worksheet = workbook.Worksheets("Sheet1")

            worksheet.Range(worksheet.Cells(1, 1), worksheet.Cells(1, 10)).Merge()
            worksheet.Range(worksheet.Cells(1, 1), worksheet.Cells(1, 10)).VerticalAlignment = Excel.Constants.xlCenter

            worksheet.Cells(1, 1).Value = "Production Report from " & Fdate.Text & "  to  " & TDate.Text
            Dim columnsCount As Integer = DataGridView1.Columns.Count
            For Each column In DataGridView1.Columns
                worksheet.Cells(2, column.Index + 1).Value = column.Name
                worksheet.Columns.AutoFit()
            Next
            'Export Header Name End


            'Export Each Row Start
            For i As Integer = 0 To DataGridView1.Rows.Count - 2
                Dim columnIndex As Integer = 0
                Do Until columnIndex = columnsCount
                    worksheet.Cells(i + 3, columnIndex + 1).Value = DataGridView1.Item(columnIndex, i).Value.ToString
                    columnIndex += 1
                Loop
            Next
            'Export Each Row End




            excelLocation = saveDlg.FileName
            workbook.SaveCopyAs(excelLocation)
            workbook.Saved = True
            workbook.Close(True, 0, 0)
            APP.Quit()

            MessageBox.Show("Exporting is completed..")
        End If

    End Sub

    Private Sub BtnCancel_Click(sender As Object, e As EventArgs) Handles BtnCancel.Click

    End Sub
End Class