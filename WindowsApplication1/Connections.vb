﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.OleDb
Public Class Connections
    Dim Dsp As New DataSet
    Dim CONN As New SqlConnection()
    Dim CONN1 As New SqlConnection()
    Dim Ds As New DataSet
    Dim StrSql As String
    Dim ADP As New SqlDataAdapter()
    Dim cmd As New SqlCommand

    Public Function OpenConnectionDataserver() As SqlConnection
lab:
        CONN.Close()
        On Error GoTo lab
        If CONN.State = ConnectionState.Open Then CONN.Close()
        CONN.ConnectionString = "Data Source=PRMBSQLS01\DATASERVER;Initial Catalog=Apollo_Cochin;User ID=sa;Password=luttaapi;"
        CONN.Open()
        OpenConnectionDataserver = CONN
    End Function
    Public Function OpenConnectionScadaServer() As SqlConnection
lab:
        CONN.Close()
        On Error GoTo lab
        If CONN.State = ConnectionState.Open Then CONN.Close()
        CONN.ConnectionString = "Data Source=1001ITSP43\DATAEXPRESS;Initial Catalog=Scada;User ID=sa;Password=luttaapi;"
        CONN.Open()
        OpenConnectionScadaServer = CONN
    End Function
    Public Function OpenConnectionFIFO() As SqlConnection
lab:
        CONN.Close()
        On Error GoTo lab
        If CONN.State = ConnectionState.Open Then CONN.Close()
        CONN.ConnectionString = "Data Source=PRMBSQLS01\DATASERVER;Initial Catalog=FIFO;User ID=sa;Password=luttaapi;"
        CONN.Open()
        OpenConnectionFIFO = CONN
    End Function
    Public Function OpenConnectionTraceMeDB() As SqlConnection
lab:
        CONN.Close()
        On Error GoTo lab
        If CONN.State = ConnectionState.Open Then CONN.Close()
        CONN.ConnectionString = "Data Source=PRMBPLCS01\DATASERVER_PRODN;Initial Catalog=TraceMeDB;User ID=sa;Password=Pass@2020#;"
        CONN.Open()
        OpenConnectionTraceMeDB = CONN
    End Function


    'test////////////////////
    '    Public Function OpenConnectionTraceMeDB() As SqlConnection
    'lab:
    '        CONN.Close()
    '        On Error GoTo lab
    '        If CONN.State = ConnectionState.Open Then CONN.Close()
    '        CONN.ConnectionString = "Data Source=PRMBPLCS01\DATASERVER_PRODN;Initial Catalog=TraceMeDB;User ID=sa;Password=Pass@2020#;"
    '        CONN.Open()
    '        OpenConnectionTraceMeDB = CONN
    '    End Function

    '    Public Function OpenConnectionTraceMeDB1() As SqlConnection
    'lab:
    '        CONN.Close()
    '        On Error GoTo lab
    '        If CONN.State = ConnectionState.Open Then CONN.Close()
    '        CONN.ConnectionString = "Data Source=1004ITSP97\SQLEXPRESS;Initial Catalog=TraceMeDB;User ID=sa;Password=netfinity;"
    '        CONN.Open()
    '        OpenConnectionTraceMeDB1 = CONN
    '    End Function
    '    Public Function OpenConnectionFIFO() As SqlConnection
    'lab:
    '        CONN.Close()
    '        On Error GoTo lab
    '        If CONN.State = ConnectionState.Open Then CONN.Close()
    '        CONN.ConnectionString = "Data Source=1004ITSP97\SQLEXPRESS;Initial Catalog=FIFO;User ID=sa;Password=netfinity;"
    '        CONN.Open()
    '        OpenConnectionFIFO = CONN
    '    End Function

    '    Public Function OpenConnectionDataserver() As SqlConnection
    'lab:
    '        CONN.Close()
    '        On Error GoTo lab
    '        If CONN.State = ConnectionState.Open Then CONN.Close()
    '        CONN.ConnectionString = "Data Source=1004ITSP97\SQLEXPRESS;Initial Catalog=Apollo_Cochin;User ID=sa;Password=netfinity;"
    '        CONN.Open()
    '        OpenConnectionDataserver = CONN
    '    End Function
    '///////////////////////

    Public Function GetDataDataServer(strsql As String) As DataSet
        Ds.Clear()
        cmd.Connection = OpenConnectionDataserver()


        cmd.CommandText = strsql
        ADP.SelectCommand = cmd
        ADP.Fill(Ds)
        GetDataDataServer = Ds



    End Function
    Public Function insertTreadEntery2()

        cmd.Connection = OpenConnectionFIFO()
        StrSql = "insert into DEXT_TREADENTRY(PKDEXTID,EXT_DATE,EXT_TIME,SHIFT,TREADCODE,TREAD_DESCRIPTION,LEAF_TRUCKNO,TREADS,LOCATION,EXT_DATETIME,STATUS,HOLD,SCRAP,CHANGE,MACHINE_ID,PLANT,Tread_Code,Dual_Ext,Emp_Name,TRUCK_NO,Hold_id) values('" & GlobalVariables.DescId & "','" & GlobalVariables.extdate & "','" & GlobalVariables.exttime & "','" & GlobalVariables.extshift & "','" & GlobalVariables.SelectionTreadCode & "','" & GlobalVariables.description & "','" & GlobalVariables.Code & "','" & GlobalVariables.Qty & "','" & GlobalVariables.Location & "','" & GlobalVariables.curdate & "','FALSE','FALSE','FALSE','FALSE','1003','1001','" & GlobalVariables.ItemCode & "','" & GlobalVariables.DualSelection & "','" & GlobalVariables.Username & "','" & GlobalVariables.No & "','" & GlobalVariables.HId & "')"

        cmd.CommandText = StrSql
        cmd.ExecuteNonQuery()
    End Function

    Public Function ExecuteCommand(strsql As String) As Integer


        cmd.Connection = OpenConnectionDataserver()


        cmd.CommandText = strsql
        cmd.ExecuteNonQuery()

        ExecuteCommand = 0

    End Function

    Public Function ExecuteCommandScadaServer(strsql As String) As Integer
        cmd.Connection = OpenConnectionScadaServer()
        cmd.CommandText = strsql
        cmd.ExecuteNonQuery()

        ExecuteCommandScadaServer = 0

    End Function

    Public Function GetMouldData(strsql As String) As DataTable

        cmd.Connection = OpenConnectionDataserver()


        cmd.CommandText = strsql
        ADP.SelectCommand = cmd
        ADP.Fill(Ds, "Mould")
        GetMouldData = Ds.Tables("Mould")

    End Function
    Public Function GetDefectData(strsql As String) As DataTable

        cmd.Connection = OpenConnectionDataserver()


        cmd.CommandText = strsql
        ADP.SelectCommand = cmd
        ADP.Fill(Ds, "Defect")
        GetDefectData = Ds.Tables("Defect")
        '  Ds.Clear()
    End Function

    Public Function GetTyreData(strsql As String) As DataTable

        cmd.Connection = OpenConnectionDataserver()


        cmd.CommandText = strsql
        ADP.SelectCommand = cmd
        ADP.Fill(Ds, "Tyre")
        GetTyreData = Ds.Tables("Tyre")

    End Function
    Public Function GetPlyCode(strsql As String) As DataTable

        cmd.Connection = OpenConnectionDataserver()
        cmd.CommandText = strsql
        ADP.SelectCommand = cmd
        ADP.Fill(Ds, "Plycode")
        GetPlyCode = Ds.Tables("Plycode")

    End Function

    Public Function GetOperator(strsql As String) As DataTable

        cmd.Connection = OpenConnectionDataserver()
        cmd.CommandText = strsql
        ADP.SelectCommand = cmd
        ADP.Fill(Ds, "Operator")
        GetOperator = Ds.Tables("Operator")

    End Function
    Public Function GetSplicer(strsql As String) As DataTable

        cmd.Connection = OpenConnectionDataserver()
        cmd.CommandText = strsql
        ADP.SelectCommand = cmd
        ADP.Fill(Ds, "Splicer")
        GetSplicer = Ds.Tables("Splicer")

    End Function
    Public Function GetSelection(strsql As String) As DataTable

        cmd.Connection = OpenConnectionFIFO()
        cmd.CommandText = strsql
        ADP.SelectCommand = cmd
        ADP.Fill(Ds, "Selection")
        GetSelection = Ds.Tables("Selection")

    End Function
    Public Function GetSelectionFIFO(strsql As String) As DataTable

        cmd.Connection = OpenConnectionFIFO()
        cmd.CommandText = strsql
        ADP.SelectCommand = cmd
        ADP.Fill(Ds, "Selection")
        GetSelectionFIFO = Ds.Tables("Selection")

    End Function

    Public Function GetWidth(strsql As String) As DataTable

        cmd.Connection = OpenConnectionDataserver()
        cmd.CommandText = strsql
        ADP.SelectCommand = cmd
        ADP.Fill(Ds, "Width")
        GetWidth = Ds.Tables("Width")

    End Function
    Public Function GetAngle(strsql As String) As DataTable

        cmd.Connection = OpenConnectionDataserver()
        cmd.CommandText = strsql
        ADP.SelectCommand = cmd
        ADP.Fill(Ds, "Angle")
        GetAngle = Ds.Tables("Angle")

    End Function
    Public Function GetFabCode(strsql As String) As DataTable

        cmd.Connection = OpenConnectionDataserver()
        cmd.CommandText = strsql
        ADP.SelectCommand = cmd
        ADP.Fill(Ds, "FabCode")
        GetFabCode = Ds.Tables("FabCode")

    End Function

    Public Function GetDualAllSchedule() As DataSet
        Ds.Clear()
        cmd.Connection = OpenConnectionDataserver()

        If GlobalVariables.DualSelection = "dual1" Then
            StrSql = "select DE.Ext_Code,DE.Priority_No,DE.Sch_value,DEP.Tread_Code,DEP.Tread_Name,DEP.Comp_Cap,DEP.Comp_Base,DEP.Comp_Cushion from DE_DailyProduction DE left outer join DE_SpecMaster DEP on DE.Ext_Code=DEP.Ext_Code  where Prod_Date between '" & GlobalVariables.FromDate & "' and '" & GlobalVariables.ToDate & "' order by Priority_No"
        Else
            StrSql = "select DE.Ext_Code,DE.Priority_No,DE.Sch_value,DEP.Tread_Code,DEP.Tread_Name,DEP.Comp_Cap,DEP.Comp_Base,DEP.Comp_Cushion from DE_DailyProduction_M DE left outer join DE_SpecMaster_M DEP on DE.Ext_Code=DEP.Ext_Code  where Prod_Date between '" & GlobalVariables.FromDate & "' and '" & GlobalVariables.ToDate & "' order by Priority_No"

        End If


        cmd.CommandText = StrSql
        ADP.SelectCommand = cmd
        ADP.Fill(Ds)
        GetDualAllSchedule = Ds

    End Function
    Public Function GetMobileno() As DataSet
        Ds.Clear()
        cmd.Connection = OpenConnectionFIFO()
        StrSql = "Select Mobile_No from FIFO_MobileNo "
        cmd.CommandText = StrSql
        ADP.SelectCommand = cmd
        ADP.Fill(Ds)
        GetMobileno = Ds
    End Function
    Public Function GetDualAllScheduleSpec() As DataSet
        Ds.Clear()
        cmd.Connection = OpenConnectionDataserver()

        If GlobalVariables.DualSelection = "DUAL1" Then
            StrSql = "Select * from DE_SpecMaster where Ext_Code='" & GlobalVariables.ItemCode & "'"
        Else
            StrSql = "Select * from DE_SpecMaster_M where Ext_Code='" & GlobalVariables.ItemCode & "'"

        End If

        cmd.CommandText = StrSql
        ADP.SelectCommand = cmd
        ADP.Fill(Ds)
        GetDualAllScheduleSpec = Ds



    End Function
    Public Function GetDualScheduleData() As DataSet
        Ds.Clear()
        cmd.Connection = OpenConnectionDataserver()

        If GlobalVariables.DualSelection = "DUAL1" Then
            ' StrSql = "select DE.Ext_Code,DE.Priority_No,DE.Sch_value,DEP.Tread_Code,DEP.Tread_Name,DEP.Comp_Cap,DEP.Comp_Base,DEP.Comp_Cushion from DE_DailyProduction DE left outer join DE_SpecMaster DEP on DE.Ext_Code=DEP.Ext_Code  where Prod_Date='2021-08-10' and Shift='" & GlobalVariables.DualExactShift & "' order by Priority_No"
            'use below code for live
            StrSql = "select DE.Ext_Code,DE.Priority_No,DE.Sch_value,DEP.Tread_Code,DEP.Tread_Name,DEP.Comp_Cap,DEP.Comp_Base,DEP.Comp_Cushion from DE_DailyProduction DE left outer join DE_SpecMaster DEP on DE.Ext_Code=DEP.Ext_Code  where Prod_Date='" & GlobalVariables.DualExactDate & "' and Shift='" & GlobalVariables.DualExactShift & "' order by Priority_No"
        Else
            ' StrSql = "select DE.Ext_Code,DE.Priority_No,DE.Sch_value,DEP.Tread_Code,DEP.Tread_Name,DEP.Comp_Cap,DEP.Comp_Base,DEP.Comp_Cushion from DE_DailyProduction_M DE left outer join DE_SpecMaster_M DEP on DE.Ext_Code=DEP.Ext_Code  where Prod_Date='2021-08-10' and Shift='" & GlobalVariables.DualExactShift & "' order by Priority_No"

            'use below code for live
            StrSql = "select DE.Ext_Code,DE.Priority_No,DE.Sch_value,DEP.Tread_Code,DEP.Tread_Name,DEP.Comp_Cap,DEP.Comp_Base,DEP.Comp_Cushion from DE_DailyProduction_M DE left outer join DE_SpecMaster_M DEP on DE.Ext_Code=DEP.Ext_Code  where Prod_Date='" & GlobalVariables.DualExactDate & "' and Shift='" & GlobalVariables.DualExactShift & "' order by Priority_No"
        End If

        cmd.CommandText = StrSql
        ADP.SelectCommand = cmd
        ADP.Fill(Ds)
        GetDualScheduleData = Ds



    End Function
    Public Function GetDeleteData() As DataSet
        Ds.Clear()
        cmd.Connection = OpenConnectionFIFO()
        StrSql = "select * FROM DEXT_TREADENTRY where EXT_DATE='" & GlobalVariables.CalPRDate & "' and SHIFT='" & GlobalVariables.CalPRShift & "' and Dual_Ext='" & GlobalVariables.DualSelection & "' and TREADCODE='" & GlobalVariables.Size & "' order by TREADCODE "
        cmd.CommandText = StrSql
        ADP.SelectCommand = cmd
        ADP.Fill(Ds)
        GetDeleteData = Ds

    End Function
    Public Function userData() As DataSet
        Ds.Clear()
        cmd.Connection = OpenConnectionFIFO()
        StrSql = " select * FROM DEXT_OPRLOGIN where Login_Name='" & GlobalVariables.Username & "' and Password='" & GlobalVariables.Password & "'"
        cmd.CommandText = StrSql
        ADP.SelectCommand = cmd
        ADP.Fill(Ds)
        userData = Ds

    End Function
    Public Function SpecificTreadRetrieval() As DataSet
        Ds.Clear()
        cmd.Connection = OpenConnectionFIFO()
        StrSql = "Select top 1 * from DEXT_TREADENTRY where STATUS='FALSE' and HOLD<>'TRUE' and SCRAP='FALSE' and Tread_Code='" + GlobalVariables.ccode + "' and (DATEDIFF(mi, EXT_DATETIME, GETDATE()) < 10080) order by EXT_DATETIME asc"
        cmd.CommandText = StrSql
        ADP.SelectCommand = cmd
        ADP.Fill(Ds)
        SpecificTreadRetrieval = Ds
    End Function
    Public Function SpecificTreadRetrieval1() As DataSet
        Ds.Clear()
        cmd.Connection = OpenConnectionFIFO()
        StrSql = "Select top 1 * from DEXT_TREADENTRY where STATUS='FALSE' and HOLD<>'TRUE' and SCRAP='FALSE' and Tread_Code='" + GlobalVariables.ccode + "'  order by EXT_DATETIME asc"
        cmd.CommandText = StrSql
        ADP.SelectCommand = cmd
        ADP.Fill(Ds)
        SpecificTreadRetrieval1 = Ds
    End Function

    Public Function GetDetails() As DataSet
        Dim Dsnew As New DataSet
        cmd.Connection = OpenConnectionFIFO()
        StrSql = "select * FROM DEXT_TREADENTRY where PKDEXTID='" & GlobalVariables.id & "' "
        cmd.CommandText = StrSql
        ADP.SelectCommand = cmd
        ADP.Fill(Dsnew)
        GetDetails = Dsnew

    End Function
    Public Function HoldDetRetrieval() As DataSet
        Dim Dsnew As New DataSet
        cmd.Connection = OpenConnectionFIFO()

        StrSql = "select * FROM DEXT_HOLDENTRY where Hold_id='" & GlobalVariables.id & "' and H_Status='Hold' "

        cmd.CommandText = StrSql
        ADP.SelectCommand = cmd
        ADP.Fill(Dsnew)
        HoldDetRetrieval = Dsnew

    End Function
    Public Function LEAFTRUCKNORetrieval() As DataSet
        Dim Dsnew As New DataSet
        cmd.Connection = OpenConnectionFIFO()

        StrSql = "select * FROM DEXT_TREADENTRY WHERE LEAF_TRUCKNO='" & GlobalVariables.oldLeaf & "' AND STATUS='FALSE'"

        cmd.CommandText = StrSql
        ADP.SelectCommand = cmd
        ADP.Fill(Dsnew)
        LEAFTRUCKNORetrieval = Dsnew

    End Function
    Public Function GetHoldDetails() As DataSet
        Dim Dsnew As New DataSet
        cmd.Connection = OpenConnectionFIFO()

        StrSql = "select * FROM DEXT_TREADENTRY where PKDEXTID='" & GlobalVariables.id & "' and HOLD='FALSE'  "

        cmd.CommandText = StrSql
        ADP.SelectCommand = cmd
        ADP.Fill(Dsnew)
        GetHoldDetails = Dsnew

    End Function
    Public Function GetCompound() As DataSet
        Dim Dsnew As New DataSet
        cmd.Connection = OpenConnectionDataserver()

        If GlobalVariables.DualSelection = "dual1" Or GlobalVariables.DualSelection = "DUAL1" Then
            StrSql = "select * FROM DE_SpecMaster where Ext_Code='" & GlobalVariables.SelectionTreadCode & "' "
        ElseIf GlobalVariables.DualSelection = "DUAL2" Or GlobalVariables.DualSelection = "dual2" Then
            StrSql = "select * FROM DE_SpecMaster_M where Ext_Code='" & GlobalVariables.SelectionTreadCode & "' "
        End If


        cmd.CommandText = StrSql
        ADP.SelectCommand = cmd
        ADP.Fill(Dsnew)
        GetCompound = Dsnew

    End Function
    Public Function GetCode() As DataSet
        Dim Dsnew As New DataSet
        cmd.Connection = OpenConnectionFIFO()

        StrSql = "select LeafTruck_No from Leaf_Truck_Master where Type='" & GlobalVariables.Defect_Type & "' and LeafTruck_Status='T' order by LeafTruck_ID "

        cmd.CommandText = StrSql
        ADP.SelectCommand = cmd
        ADP.Fill(Dsnew)
        GetCode = Dsnew
    End Function

    Public Function GetCodeuwbTag(UWB_Tag_Id As String) As String
        Dim strsql As String = "select MHE_Name from UWB_Tag_Mapping where UWB_Tag_Id ='" & UWB_Tag_Id & "'  and Status= 1"
        cmd.Connection = OpenConnectionTraceMeDB()
        Dim maxcount As New SqlCommand(strsql, CONN)

        Dim count As String = maxcount.ExecuteScalar()
        If String.IsNullOrEmpty(count) Then
            Return count = 0
        Else

            Return count
        End If

    End Function


    Public Function GetTagNameForMHEName(MHE_Name As String) As String
        Dim strsql As String = "select UWB_Tag_Id from UWB_Tag_Mapping where MHE_Name ='" & MHE_Name & "'  and Status= 1"
        cmd.Connection = OpenConnectionTraceMeDB()
        Dim maxcount As New SqlCommand(strsql, CONN)

        Dim count As String = maxcount.ExecuteScalar()
        If String.IsNullOrEmpty(count) Then
            Return count = 0
        Else

            Return count
        End If
    End Function

    Public Function insertTBRequest()
        cmd.Connection = OpenConnectionFIFO()
        StrSql = "insert into DEXT_TBREQUEST(id,req_time,ext_shift,treadcode,colorcode,description,truckno,location,treads,prod_date,prod_shift,prod_time,pkdextid,Operator,Truck_No,Dual_Ext,RequestStatus) values('" & GlobalVariables.id & "','" & DateTime.Now() & "','" & GlobalVariables.extshift & "','" & GlobalVariables.TreadCode & "','" & GlobalVariables.colorcode & "','" & GlobalVariables.description & "','" & GlobalVariables.truckno & "','" & GlobalVariables.Location & "','" & GlobalVariables.treads & "','" & GlobalVariables.proddate & "','" & GlobalVariables.prodshift & "','" & GlobalVariables.prodtime & "','" & GlobalVariables.pkdextid & "','" & GlobalVariables.Qty & "','" & GlobalVariables.No & "','" & GlobalVariables.dualext & "',0)"
        cmd.CommandText = StrSql
        cmd.ExecuteNonQuery()
    End Function

    Public Function GetMHELocation(TagID As String) As String 'Fetching leaftruck at Booking Area
        Dim cmd As New SqlCommand("GetLocation_MHETest", CONN)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Connection = OpenConnectionTraceMeDB()
        cmd.Parameters.Add("@TagID", SqlDbType.NVarChar).Value = TagID
        'Dim returnParameter As SqlParameter = cmd.Parameters.Add("Tag_Name", SqlDbType.NVarChar)
        'returnParameter.Direction = ParameterDirection.ReturnValue
        ''cmd.ExecuteNonQuery()\
        Dim value = cmd.ExecuteScalar()
        If value Is Nothing Or IsDBNull(value) Then
            Dim Val = "Outside Zone"
            Return Val

        End If
        ' Dim countVal = String(cmd.ExecuteScalar())
        ' Dim countVal = cmd.ExecuteScalar()
        Return value

    End Function
    Public Function GetCodeuwb(Dual As String) As String 'Fetching leaftruck at Booking Area
        Dim cmd As New SqlCommand("GetMHEForDualLocation", CONN)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Connection = OpenConnectionTraceMeDB()
        cmd.Parameters.Add("@DualId", SqlDbType.NVarChar).Value = Dual
        'Dim returnParameter As SqlParameter = cmd.Parameters.Add("Tag_Name", SqlDbType.NVarChar)
        'returnParameter.Direction = ParameterDirection.ReturnValue
        ''cmd.ExecuteNonQuery()\
        Dim value = cmd.ExecuteScalar()
        If value Is Nothing Or IsDBNull(value) Then
            Dim Val = 0
            Return Val

        End If
        ' Dim countVal = String(cmd.ExecuteScalar())
        ' Dim countVal = cmd.ExecuteScalar()
        Return value

    End Function
    Public Function GetCodeuwbhold(Dual As String) As String 'Fetching leaftrucks at Scrap Area
        Dim cmd As New SqlCommand("GetMHEForDualScrapLocation", CONN)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Connection = OpenConnectionTraceMeDB()
        cmd.Parameters.Add("@DualId", SqlDbType.NVarChar).Value = Dual
        'Dim returnParameter As SqlParameter = cmd.Parameters.Add("Tag_Name", SqlDbType.NVarChar)
        'returnParameter.Direction = ParameterDirection.ReturnValue
        ''cmd.ExecuteNonQuery()\
        Dim value = cmd.ExecuteScalar()
        If value Is Nothing Or IsDBNull(value) Then
            Dim Val = 0
            Return Val

        End If
        ' Dim countVal = String(cmd.ExecuteScalar())
        ' Dim countVal = cmd.ExecuteScalar()
        Return value

    End Function

    Public Function GetCodeSW() As DataSet
        Dim Dsnew As New DataSet
        cmd.Connection = OpenConnectionFIFO()

        StrSql = "select LeafTruck_No from Leaf_Truck_Master_SW where Type='" & GlobalVariables.Defect_Type & "' and LeafTruck_Status='T' order by LeafTruck_ID "

        cmd.CommandText = StrSql
        ADP.SelectCommand = cmd
        ADP.Fill(Dsnew)
        GetCodeSW = Dsnew

    End Function

    Public Function GetLocation() As DataSet
        Dim Dsnew1 As New DataSet
        cmd.Connection = OpenConnectionFIFO()

        If GlobalVariables.Defect_Type = "SW" Then
            StrSql = "  SELECT Location FROM Location_Master where  Location_Status='T' and  Type='SW 'order by Location_ID "
        Else

            If GlobalVariables.DualSelection = "dual1" Or GlobalVariables.DualSelection = "DUAL1" Then
                StrSql = "  SELECT Location FROM Location_Master where  Location_Status='T' and (location like 'A%' or location like 'B%' or location like 'C%'  ) order by Location_ID "
            ElseIf GlobalVariables.DualSelection = "DUAL2" Or GlobalVariables.DualSelection = "dual2" Then
                StrSql = "  SELECT Location FROM Location_Master where  Location_Status='T' and (location like 'D%' or location like 'E%' or location like 'F%'  ) order by Location_ID "
            End If
        End If


        cmd.CommandText = StrSql
        ADP.SelectCommand = cmd
        ADP.Fill(Dsnew1)
        GetLocation = Dsnew1

    End Function
    Public Function GetLocation1() As DataSet
        Dim Dsnew1 As New DataSet
        cmd.Connection = OpenConnectionFIFO()

        If GlobalVariables.Defect_Type = "SW" Then
            StrSql = "  SELECT Location FROM Location_Master where  Location_Status='T' and Type='SW' order by Location_ID "
        Else
            StrSql = "  SELECT Location FROM Location_Master where  Location_Status='T' and Type='other' order by Location_ID "
        End If

        cmd.CommandText = StrSql
        ADP.SelectCommand = cmd
        ADP.Fill(Dsnew1)
        GetLocation1 = Dsnew1

    End Function
    Public Function Check_leafTruckCount() As Single
        Dim strsql As String = "select count(*) from DEXT_TREADENTRY where STATUS='FALSE' and SCRAP='FALSE' and  LEAF_TRUCKNO='" & GlobalVariables.Code & "'"

        cmd.Connection = OpenConnectionFIFO()
        Dim maxcount As New SqlCommand(strsql, CONN)

        Dim count As Single = Convert.ToInt64(maxcount.ExecuteScalar())
        Return count
    End Function
    Public Function Check_Tread() As Single
        Dim strsql As String = "select count(*) from Tbl_Treads where Tread_Code='" + GlobalVariables.selectTread + "' "
        cmd.Connection = OpenConnectionFIFO()
        Dim maxcount As New SqlCommand(strsql, CONN)

        Dim count As Single = Convert.ToInt64(maxcount.ExecuteScalar())
        Return count
    End Function

    Public Function Check_EntryCount() As Single
        Dim strsql As String = "select count(*) from DEXT_TREADENTRY where LEAF_TRUCKNO='" & GlobalVariables.T1 & "' and LOCATION='" & GlobalVariables.T2 & "' and  TREADS='" & GlobalVariables.T3 & "' and TRUCK_NO='" & GlobalVariables.T4 & "' and STATUS='FALSE'"

        cmd.Connection = OpenConnectionFIFO()
        Dim maxcount As New SqlCommand(strsql, CONN)

        Dim count As Single = Convert.ToInt64(maxcount.ExecuteScalar())
        ' Dim count As Single = 0
        Return count
    End Function
    Public Function LocCount() As Single
        Dim strsql As String = "select count(*) from Location_Master where Location_Status='T' and LOCATION='" & GlobalVariables.Location & "'"

        cmd.Connection = OpenConnectionFIFO()
        Dim maxcount As New SqlCommand(strsql, CONN)

        Dim count As Single = Convert.ToInt64(maxcount.ExecuteScalar())
        Return count
    End Function
    Public Function Check_Locationcount() As Single
        Dim strsql As String = "select count(*) from DEXT_TREADENTRY where STATUS='FALSE' and SCRAP='FALSE' and  LOCATION='" & GlobalVariables.Location & "'"

        cmd.Connection = OpenConnectionFIFO()
        Dim maxcount As New SqlCommand(strsql, CONN)

        Dim count As Single = Convert.ToInt64(maxcount.ExecuteScalar())
        Return count
    End Function
    Public Function PKDidRetrieval() As DataSet
        Ds.Clear()
        cmd.Connection = OpenConnectionFIFO()
        StrSql = "Select top 1 * from DEXT_TREADENTRY where STATUS='FALSE' and SCRAP='FALSE' and  LEAF_TRUCKNO='" & GlobalVariables.Code & "'  order by EXT_DATETIME asc"
        cmd.CommandText = StrSql
        ADP.SelectCommand = cmd
        ADP.Fill(Ds)
        PKDidRetrieval = Ds
    End Function
    Public Function PKDidLocationRetrieval() As DataSet
        Ds.Clear()
        cmd.Connection = OpenConnectionFIFO()
        StrSql = "Select top 1 * from DEXT_TREADENTRY where STATUS='FALSE' and SCRAP='FALSE' and  LOCATION='" & GlobalVariables.Location & "'  order by EXT_DATETIME asc"
        cmd.CommandText = StrSql
        ADP.SelectCommand = cmd
        ADP.Fill(Ds)
        PKDidLocationRetrieval = Ds
    End Function
    Public Function TreadDetailsRetrieval() As DataSet
        Ds.Clear()
        cmd.Connection = OpenConnectionFIFO()
        StrSql = "Select * from DEXT_TREADENTRY where PKDEXTID='" & GlobalVariables.pkdextid & "'"
        cmd.CommandText = StrSql
        ADP.SelectCommand = cmd
        ADP.Fill(Ds)
        TreadDetailsRetrieval = Ds
    End Function
    Public Function get_DextOKID() As Single
        Dim strsql As String = "Select isnull(max(id),0)+1 from DEXT_OKENTRY"

        cmd.Connection = OpenConnectionFIFO()
        Dim maxcount As New SqlCommand(strsql, CONN)

        Dim count As Single = Convert.ToInt64(maxcount.ExecuteScalar())
        Return count
    End Function

    Public Function insertHOLD()

        cmd.Connection = OpenConnectionFIFO()
        StrSql = "update DEXT_TREADENTRY set HOLD='TRUE',EXT_REASON='" + GlobalVariables.Position + "',Emp_Name='" + GlobalVariables.LoginName + "' where PKDEXTID='" + GlobalVariables.pkdextid + "'"
        cmd.CommandText = StrSql
        cmd.ExecuteNonQuery()
    End Function
    Public Function insertOKEntery()

        cmd.Connection = OpenConnectionFIFO()
        StrSql = "insert into DEXT_OKENTRY(id,ext_date,ext_time,ext_shift,treadcode,colorcode,description,truckno,location,treads,prod_date,prod_shift,prod_time,pkdextid,Operator,Truck_No,Dual_Ext) values('" & GlobalVariables.id & "','" & GlobalVariables.extdate & "','" & GlobalVariables.exttime & "','" & GlobalVariables.extshift & "','" & GlobalVariables.TreadCode & "','" & GlobalVariables.colorcode & "','" & GlobalVariables.description & "','" & GlobalVariables.truckno & "','" & GlobalVariables.Location & "','" & GlobalVariables.treads & "','" & GlobalVariables.proddate & "','" & GlobalVariables.prodshift & "','" & GlobalVariables.prodtime & "','" & GlobalVariables.pkdextid & "','" & GlobalVariables.Qty & "','" & GlobalVariables.No & "','" & GlobalVariables.dualext & "')"
        cmd.CommandText = StrSql
        cmd.ExecuteNonQuery()
    End Function
    Public Function insertOKEntery1()

        cmd.Connection = OpenConnectionFIFO()
        StrSql = "insert into DEXT_OKENTRY(id,ext_date,ext_time,ext_shift,treadcode,colorcode,description,truckno,location,treads,prod_date,prod_shift,prod_time,pkdextid,Operator,Truck_No,Dual_Ext) values('" & GlobalVariables.DescId & "','" & GlobalVariables.CurrentDate & "','" & GlobalVariables.time & "','" & GlobalVariables.Shift & "','" & GlobalVariables.SelectionTreadCode & "','" & GlobalVariables.ItemCode & "','" & GlobalVariables.description & "','" & GlobalVariables.oldLeaf & "','" & GlobalVariables.Location & "','" & GlobalVariables.Qty & "','" & GlobalVariables.extdate & "','" & GlobalVariables.exttime & "','" & GlobalVariables.extshift & "','" & GlobalVariables.pkdextid & "','" & GlobalVariables.Username & "','" & GlobalVariables.No & "','" & GlobalVariables.DualSelection & "')"
        cmd.CommandText = StrSql
        cmd.ExecuteNonQuery()
    End Function

    Public Function insertDeleteEntry()

        cmd.Connection = OpenConnectionFIFO()
        StrSql = "insert into DEXT_DELETEENTRY(pkdextid,prod_date,prod_time,prod_shift,treadcode,colorcode,description,truckno,location,treads,del_date,del_time,del_shift,Dual_Ext,Deleted_By) values('" & GlobalVariables.pkdextid & "','" & GlobalVariables.proddate & "','" & GlobalVariables.prodtime & "','" & GlobalVariables.prodshift & "','" & GlobalVariables.TreadCode & "','" & GlobalVariables.colourcode & "','" & GlobalVariables.description & "','" & GlobalVariables.truckno & "','" & GlobalVariables.oldLocation & "','" & GlobalVariables.treads & "','" & GlobalVariables.CurrentDate & "','" & GlobalVariables.timevalue & "','" & GlobalVariables.shiftTime & "','" & GlobalVariables.T1 & "','" & GlobalVariables.Bias & "')"
        cmd.CommandText = StrSql
        cmd.ExecuteNonQuery()
    End Function
    Public Function DeleteTread()

        cmd.Connection = OpenConnectionFIFO()
        StrSql = " delete FROM DEXT_TREADENTRY  where PKDEXTID ='" & GlobalVariables.id & "' "
        cmd.CommandText = StrSql
        cmd.ExecuteNonQuery()
    End Function
    Public Function DeleteTreadH()

        cmd.Connection = OpenConnectionFIFO()
        StrSql = " delete FROM DEXT_TREADENTRY  where PKDEXTID ='" & GlobalVariables.Id1 & "' "
        cmd.CommandText = StrSql
        cmd.ExecuteNonQuery()
    End Function

    Public Function TrueUpdateLocation()
        cmd.Connection = OpenConnectionFIFO()
        StrSql = "update Location_Master set Location_Status='T' where Location='" & GlobalVariables.Location & "' "
        cmd.CommandText = StrSql
        cmd.ExecuteNonQuery()
    End Function
    Public Function TrueUpdateOldLocation()

        cmd.Connection = OpenConnectionFIFO()
        StrSql = "update Location_Master set Location_Status='T' where Location='" & GlobalVariables.oldLocation & "' "
        cmd.CommandText = StrSql
        cmd.ExecuteNonQuery()

    End Function
    Public Function UpdateNewID()

        cmd.Connection = OpenConnectionFIFO()
        '' StrSql = "update Location_Master set Location_Status='T' where Location='" & GlobalVariables.oldLocation & "' and Type='" & GlobalVariables.Defect_Type & "'"
        StrSql = "update DEXT_HOLDENTRY set New_PKDEXTID='T' where Hold_id='" & GlobalVariables.Id1 & "' "
        cmd.CommandText = StrSql
        cmd.ExecuteNonQuery()

    End Function

    Public Function TrueUpdateLeaftruck()

        cmd.Connection = OpenConnectionFIFO()
        StrSql = "update Leaf_Truck_Master set LeafTruck_Status='T' where LeafTruck_No='" & GlobalVariables.SerialNo & "'"
        cmd.CommandText = StrSql
        cmd.ExecuteNonQuery()
    End Function

    Public Function TrueUpdateLeaftruckSW()

        cmd.Connection = OpenConnectionFIFO()
        StrSql = "update Leaf_Truck_Master_SW set LeafTruck_Status='T' where LeafTruck_No='" & GlobalVariables.SerialNo & "'"
        cmd.CommandText = StrSql
        cmd.ExecuteNonQuery()
    End Function
    Public Function TrueUpdateoldLeaftruck()

        cmd.Connection = OpenConnectionFIFO()
        StrSql = "update Leaf_Truck_Master set LeafTruck_Status='T' where LeafTruck_No='" & GlobalVariables.oldLeaf & "' "
        cmd.CommandText = StrSql
        cmd.ExecuteNonQuery()
    End Function

    Public Function TrueUpdateoldLeaftruckSW()

        cmd.Connection = OpenConnectionFIFO()
        StrSql = "update Leaf_Truck_Master_SW set LeafTruck_Status='T' where LeafTruck_No='" & GlobalVariables.oldLeaf & "' "
        cmd.CommandText = StrSql
        cmd.ExecuteNonQuery()
    End Function

    Public Function insertOK()

        cmd.Connection = OpenConnectionFIFO()
        StrSql = "update DEXT_TREADENTRY set STATUS='TRUE' where PKDEXTID='" & GlobalVariables.pkdextid & "'"
        cmd.CommandText = StrSql
        cmd.ExecuteNonQuery()
    End Function
    Public Function LeafTruckRetrieval() As DataSet
        Ds.Clear()
        cmd.Connection = OpenConnectionFIFO()
        StrSql = "SELECT * FROM Leaf_Truck_Master WHERE LeafTruck_No LIKE '" & GlobalVariables.Code & "' and LeafTruck_Status='T' and Type='" & GlobalVariables.Defect_Type & "' "
        cmd.CommandText = StrSql
        ADP.SelectCommand = cmd
        ADP.Fill(Ds)
        LeafTruckRetrieval = Ds
    End Function
    Public Function get_DextPKID() As Single
        Dim strsql As String = "Select isnull(max(PKDEXTID),0)+1 from DEXT_TREADENTRY"

        cmd.Connection = OpenConnectionFIFO()
        Dim maxcount As New SqlCommand(strsql, CONN)

        Dim count As Single = Convert.ToInt64(maxcount.ExecuteScalar())
        Return count
    End Function
    Public Function get_DextScrapID() As Single
        Dim strsql As String = "Select isnull(max(id),0)+1 from DEXT_SCRAPENTRY"

        cmd.Connection = OpenConnectionFIFO()
        Dim maxcount As New SqlCommand(strsql, CONN)

        Dim count As Single = Convert.ToInt64(maxcount.ExecuteScalar())
        Return count
    End Function
    Public Function get_DextholdID() As Single
        Dim strsql As String = "Select isnull(max(Hold_id),0)+1 from DEXT_HOLDENTRY"

        cmd.Connection = OpenConnectionFIFO()
        Dim maxcount As New SqlCommand(strsql, CONN)

        Dim count As Single = Convert.ToInt64(maxcount.ExecuteScalar())
        Return count
    End Function
    Public Function insertTreadEntery()

        cmd.Connection = OpenConnectionFIFO()
        StrSql = "insert into DEXT_TREADENTRY(PKDEXTID,EXT_DATE,EXT_TIME,SHIFT,TREADCODE,TREAD_DESCRIPTION,LEAF_TRUCKNO,TREADS,LOCATION,EXT_DATETIME,STATUS,HOLD,SCRAP,CHANGE,MACHINE_ID,PLANT,Tread_Code,Dual_Ext,Emp_Name,TRUCK_NO,BOOKER_NO) values('" & GlobalVariables.DescId & "','" & GlobalVariables.extdate & "','" & GlobalVariables.exttime & "','" & GlobalVariables.extshift & "','" & GlobalVariables.SelectionTreadCode & "','" & GlobalVariables.description & "','" & GlobalVariables.Code & "','" & GlobalVariables.Qty & "','" & GlobalVariables.Location & "','" & GlobalVariables.curdate & "','FALSE','FALSE','FALSE','FALSE','1003','1001','" & GlobalVariables.ItemCode & "','" & GlobalVariables.DualSelection & "','" & GlobalVariables.Username & "','" & GlobalVariables.No & "','" & GlobalVariables.BookerNo & "')"

        cmd.CommandText = StrSql
        cmd.ExecuteNonQuery()
    End Function
    Public Function insertTreadEnteryManual()

        cmd.Connection = OpenConnectionFIFO()
        StrSql = "insert into DEXT_TREADENTRY(PKDEXTID,EXT_DATE,EXT_TIME,SHIFT,TREADCODE,TREAD_DESCRIPTION,LEAF_TRUCKNO,TREADS,LOCATION,EXT_DATETIME,STATUS,HOLD,SCRAP,CHANGE,MACHINE_ID,PLANT,Tread_Code,Dual_Ext,Emp_Name,TRUCK_NO) values('" & GlobalVariables.DescId & "','" & GlobalVariables.FromDate & "','" & GlobalVariables.time & "','" & GlobalVariables.Shift & "','" & GlobalVariables.SelectionTreadCode & "','" & GlobalVariables.description & "','" & GlobalVariables.Code & "','" & GlobalVariables.Qty & "','" & GlobalVariables.Location & "','" & GlobalVariables.curdate & "','FALSE','FALSE','FALSE','FALSE','1003','1001','" & GlobalVariables.ItemCode & "','" & GlobalVariables.DualSelection & "','" & GlobalVariables.OperatorNo & "','" & GlobalVariables.No & "')"

        cmd.CommandText = StrSql
        cmd.ExecuteNonQuery()
    End Function
    Public Function insertTreadHoldEntery()

        cmd.Connection = OpenConnectionFIFO()
        StrSql = "insert into DEXT_HOLDENTRY(PKDEXTID,EXT_DATE,EXT_TIME,SHIFT,TREADCODE,TREAD_DESCRIPTION,LEAF_TRUCKNO,TREADS,LOCATION,EXT_DATETIME,STATUS,HOLD,SCRAP,CHANGE,MACHINE_ID,PLANT,Tread_Code,Dual_Ext,Emp_Name,TRUCK_NO,Hold_id,Hold_Date,Hold_Shift,Hold_Reason,Hold_EmpNo,Hold_EmpName,Hold_By,H_Status) values('" & GlobalVariables.id & "','" & GlobalVariables.extdate & "','" & GlobalVariables.exttime & "','" & GlobalVariables.extshift & "','" & GlobalVariables.SelectionTreadCode & "','" & GlobalVariables.description & "','" & GlobalVariables.Code & "','" & GlobalVariables.Qty & "','" & GlobalVariables.Location & "','" & GlobalVariables.curdate & "','FALSE','TRUE','FALSE','FALSE','1003','1001','" & GlobalVariables.ItemCode & "','" & GlobalVariables.DualSelection & "','" & GlobalVariables.Username & "','" & GlobalVariables.No & "','" & GlobalVariables.HId & "','" & GlobalVariables.HDate & "','" & GlobalVariables.HShift & "','" & GlobalVariables.HReason & "','" & GlobalVariables.HEmpNo & "','" & GlobalVariables.HEmpName & "','" & GlobalVariables.HBy & "','Hold')"
        cmd.CommandText = StrSql
        cmd.ExecuteNonQuery()
    End Function
    Public Function insertTreadScrapEntery()

        cmd.Connection = OpenConnectionFIFO()
        StrSql = "insert into DEXT_SCRAPENTRY(id,scrp_date,scrp_time,scrp_shift,treadcode,colorcode,description,truckno,location,treads,prod_date,prod_time,prod_shift,pkdextid,Dual_Ext,Holdid,Scrap_By) values('" & GlobalVariables.DescId & "','" & GlobalVariables.CurrentDate & "','" & GlobalVariables.time & "','" & GlobalVariables.Shift & "','" & GlobalVariables.SelectionTreadCode & "','" & GlobalVariables.ItemCode & "','" & GlobalVariables.description & "','" & GlobalVariables.oldLeaf & "','" & GlobalVariables.Location & "','" & GlobalVariables.Qty & "','" & GlobalVariables.extdate & "','" & GlobalVariables.exttime & "','" & GlobalVariables.extshift & "','" & GlobalVariables.Id1 & "','" & GlobalVariables.DualSelection & "','" & GlobalVariables.HId & "','" & GlobalVariables.OperatorNo & "')"
        cmd.CommandText = StrSql
        cmd.ExecuteNonQuery()
    End Function
    Public Function UpdateHoldStatus()

        cmd.Connection = OpenConnectionFIFO()
        StrSql = "update DEXT_HOLDENTRY set H_Status='Released' where Hold_id='" & GlobalVariables.id & "' "

        cmd.CommandText = StrSql
        cmd.ExecuteNonQuery()
    End Function
    Public Function FalseUpdateLeaftruckSW()
        cmd.Connection = OpenConnectionFIFO()
        StrSql = "update Leaf_Truck_Master_SW set LeafTruck_Status='F' where LeafTruck_No='" & GlobalVariables.Code & "' "
        cmd.CommandText = StrSql
        cmd.ExecuteNonQuery()
    End Function
    Public Function FalseUpdateLeaftruck()

        cmd.Connection = OpenConnectionFIFO()
        StrSql = "update Leaf_Truck_Master set LeafTruck_Status='F' where LeafTruck_No='" & GlobalVariables.Code & "' "

        cmd.CommandText = StrSql
        cmd.ExecuteNonQuery()
    End Function
    Public Function insertTreadScrapStatus()

        cmd.Connection = OpenConnectionFIFO()
        StrSql = "update DEXT_HOLDENTRY set H_Status='Scrap' where Hold_id='" & GlobalVariables.id & "' "
        cmd.CommandText = StrSql
        cmd.ExecuteNonQuery()
    End Function
    Public Function insertTreadHoldStatus()

        cmd.Connection = OpenConnectionFIFO()
        StrSql = "update DEXT_TREADENTRY set HOLD='TRUE', TREADS='" & GlobalVariables.Qty & "', Hold_id='" & GlobalVariables.HId & "'  where PKDEXTID='" & GlobalVariables.id & "' "
        cmd.CommandText = StrSql
        cmd.ExecuteNonQuery()
    End Function
    Public Function FalseUpdateLocation()

        cmd.Connection = OpenConnectionFIFO()
        StrSql = "update Location_Master set Location_Status='F' where Location='" & GlobalVariables.Location & "' "

        cmd.CommandText = StrSql
        cmd.ExecuteNonQuery()
    End Function
    Public Function Check_Login() As Single
        Dim strsql As String = "select count(*) from DEXT_OPRLOGIN where Login_Name='" & GlobalVariables.Username & "' and Password='" & GlobalVariables.Password & "'"

        cmd.Connection = OpenConnectionFIFO()
        Dim maxcount As New SqlCommand(strsql, CONN)

        Dim count As Single = Convert.ToInt64(maxcount.ExecuteScalar())
        Return count
    End Function

    Public Function SpecRetrieval() As DataSet
        Ds.Clear()
        cmd.Connection = OpenConnectionDataserver()
        StrSql = "Select * from " & GlobalVariables.spectablename & " where Ext_Code='" & GlobalVariables.SelectionTreadCode & "'"
        cmd.CommandText = StrSql
        ADP.SelectCommand = cmd
        ADP.Fill(Ds)
        SpecRetrieval = Ds
    End Function
    Public Function GetLocationData() As DataSet
        Dsp.Clear()
        cmd.Connection = OpenConnectionFIFO()
        StrSql = " Select a.*,LM.Location from DEXT_TREADENTRY a left outer join Location_Master LM on a.Location=LM.Location where a.STATUS='FALSE' and a.SCRAP='FALSE'  order by LM.Location_ID "
        cmd.CommandText = StrSql
        ADP.SelectCommand = cmd
        ADP.Fill(Dsp)
        GetLocationData = Dsp
    End Function
    Public Function GETTBDATA() As DataSet
        Dsp.Clear()
        cmd.Connection = OpenConnectionFIFO()
        StrSql = " Select * from  DEXT_OKENTRY where ext_date='" & GlobalVariables.FromDate & "' and ext_shift='" & GlobalVariables.Shift & "' "
        cmd.CommandText = StrSql
        ADP.SelectCommand = cmd
        ADP.Fill(Dsp)
        GETTBDATA = Dsp
    End Function
    Public Function DeleteHistory() As DataSet
        Dsp.Clear()
        cmd.Connection = OpenConnectionFIFO()
        StrSql = " Select * from  DEXT_DELETEENTRY where del_date='" & GlobalVariables.FromDate & "' and del_shift='" & GlobalVariables.Shift & "' "
        cmd.CommandText = StrSql
        ADP.SelectCommand = cmd
        ADP.Fill(Dsp)
        DeleteHistory = Dsp
    End Function
    Public Function GETFinalData() As DataSet
        Dsp.Clear()
        cmd.Connection = OpenConnectionFIFO()
        StrSql = "   select top 1 *  FROM DEXT_OKENTRY order by id desc "
        cmd.CommandText = StrSql
        ADP.SelectCommand = cmd
        ADP.Fill(Dsp)
        GETFinalData = Dsp
    End Function
    Public Function GETFinalDataColourcode() As DataSet
        Dsp.Clear()
        cmd.Connection = OpenConnectionFIFO()
        StrSql = "    select top 1 *  FROM DEXT_OKENTRY where colorcode='" & GlobalVariables.ccode & "' order by id desc "
        cmd.CommandText = StrSql
        ADP.SelectCommand = cmd
        ADP.Fill(Dsp)
        GETFinalDataColourcode = Dsp
    End Function
    Public Function AllTreadRetrevial() As DataSet
        Dsp.Clear()
        cmd.Connection = OpenConnectionFIFO()
        StrSql = "Select * from DEXT_TREADENTRY where  STATUS='FALSE' and HOLD<>'TRUE' and SCRAP<>'TRUE'  order by Location  "
        cmd.CommandText = StrSql
        ADP.SelectCommand = cmd
        ADP.Fill(Dsp)
        AllTreadRetrevial = Dsp
    End Function
    Public Function GETProductionData() As DataSet
        Dsp.Clear()
        cmd.Connection = OpenConnectionFIFO()
        StrSql = "Select * from DEXT_TREADENTRY where EXT_DATE between '" & GlobalVariables.FromDate & "' and '" & GlobalVariables.ToDate & "' order by PKDEXTID"
        cmd.CommandText = StrSql
        ADP.SelectCommand = cmd
        ADP.Fill(Dsp)
        GETProductionData = Dsp
    End Function
    Public Function GETHoldHistory() As DataSet
        Dsp.Clear()
        cmd.Connection = OpenConnectionFIFO()
        StrSql = "Select * from DEXT_HOLDENTRY where Hold_Date between '" & GlobalVariables.FromDate & "' and '" & GlobalVariables.ToDate & "' order by Hold_id"
        cmd.CommandText = StrSql
        ADP.SelectCommand = cmd
        ADP.Fill(Dsp)
        GETHoldHistory = Dsp
    End Function
    Public Function GETScrapRep() As DataSet
        Dsp.Clear()
        cmd.Connection = OpenConnectionFIFO()
        StrSql = "Select * from DEXT_SCRAPENTRY where scrp_date between '" & GlobalVariables.FromDate & "' and '" & GlobalVariables.ToDate & "' order by id"
        cmd.CommandText = StrSql
        ADP.SelectCommand = cmd
        ADP.Fill(Dsp)
        GETScrapRep = Dsp
    End Function

    Public Function GetAgeData() As DataSet
        Dsp.Clear()
        cmd.Connection = OpenConnectionFIFO()
        StrSql = "Select a.*,LM.Location from DEXT_TREADENTRY a left outer join Location_Master LM on a.Location=LM.Location where a.STATUS='FALSE' and a.SCRAP='FALSE'  order by EXT_DATETIME asc"
        cmd.CommandText = StrSql
        ADP.SelectCommand = cmd
        ADP.Fill(Dsp)
        GetAgeData = Dsp
    End Function
    Public Function GetColourData() As DataSet
        Dsp.Clear()
        cmd.Connection = OpenConnectionFIFO()
        StrSql = "Select * from DEXT_TREADENTRY where STATUS='FALSE' and SCRAP='FALSE'  and Tread_Code='" & GlobalVariables.clrcode & "' order by PKDEXTID"
        cmd.CommandText = StrSql
        ADP.SelectCommand = cmd
        ADP.Fill(Dsp)
        GetColourData = Dsp
    End Function

    Public Function Check_Tread_Value() As Single
        Dim strsql As String = "select count(*) from Tbl_Treads_new where Tread_Code='" & GlobalVariables.selectTread & "'"

        cmd.Connection = OpenConnectionFIFO()
        Dim maxcount As New SqlCommand(strsql, CONN)

        Dim count As Single = Convert.ToInt64(maxcount.ExecuteScalar())
        Return count
    End Function

    Public Function Fetch_Recent_TruckNo(shiftname As String, proddate As String) As Single
        Dim strsql As String = "select isnull(Max(TRUCK_NO),0) as TruckNo FROM [FIFO].[dbo].[DEXT_TREADENTRY] where TREADCODE='" & GlobalVariables.SelectionTreadCode & "' and SHIFT='" & shiftname & "' and [EXT_DATE]='" & proddate & "' and [Dual_Ext]='" & GlobalVariables.DualSelection & "'"

        cmd.Connection = OpenConnectionFIFO()
        Dim maxcount As New SqlCommand(strsql, CONN)

        Dim count As Single = Convert.ToInt64(maxcount.ExecuteScalar())
        Return count
    End Function






    Public Function Get_Tagged_TreadList(shiftname As String, proddate As String) As DataTable
        Dim dt_TreadList As New DataTable
        Dim strsql As String = "select [TREADCODE] FROM [FIFO].[dbo].[DEXT_TREADENTRY] where EXT_DATE='" & proddate & "' and SHIFT='" & shiftname & "' and Dual_Ext='" & GlobalVariables.DualSelection & "'"
        cmd.Connection = OpenConnectionFIFO()
        cmd.CommandText = strsql
        ADP.SelectCommand = cmd
        ADP.Fill(dt_TreadList)
        Get_Tagged_TreadList = dt_TreadList
    End Function

    Public Function Get_TreadList_AboveScheduled(shiftname As String, proddate As String) As DataTable
        Dim strsql As String
        Dim dt_TreadList As New DataTable
        If GlobalVariables.DualSelection = "DUAL1" Then
            strsql = "select Ext_Code,Sch_Value  FROM [Apollo_Cochin].[dbo].[DE_DailyProduction] t1 inner join(select TREADCODE,SUM(TREADS) as TotalTreads from [FIFO].[dbo].[DEXT_TREADENTRY] where EXT_DATE='" & proddate & "' and SHIFT='" & shiftname & "' and Dual_Ext='" & GlobalVariables.DualSelection & "'  group by TREADCODE)A on Ext_Code=A.TREADCODE where Sch_Value<A.TotalTreads and [Prod_Date]='" & proddate & "' and t1.Shift='" & shiftname & "'"
        Else
            strsql = "select Ext_Code,Sch_Value  FROM [Apollo_Cochin].[dbo].[DE_DailyProduction_M] t1 inner join(select TREADCODE,SUM(TREADS) as TotalTreads from [FIFO].[dbo].[DEXT_TREADENTRY] where EXT_DATE='" & proddate & "' and SHIFT='" & shiftname & "' and Dual_Ext='" & GlobalVariables.DualSelection & "'  group by TREADCODE)A on Ext_Code=A.TREADCODE where Sch_Value<A.TotalTreads and [Prod_Date]='" & proddate & "' and t1.Shift='" & shiftname & "'"
        End If


        cmd.Connection = OpenConnectionFIFO()
        cmd.CommandText = strsql
        ADP.SelectCommand = cmd
        ADP.Fill(dt_TreadList)
        Get_TreadList_AboveScheduled = dt_TreadList
    End Function
End Class




