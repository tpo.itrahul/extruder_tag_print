﻿Public Class FrmDisplay
    Public Dsbtnvalue As New DataSet()
    Dim objcon As New Connections
    Public SelectedDate As DateTime
    Public SelectedprodDate As DateTime
    Public dt As New DataTable()
    Dim rwcnt As Integer = 0
    Public ds As New DataSet()
    Private Sub FrmDisplay_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If DateTime.Now > Convert.ToDateTime("12:00:00 AM") AndAlso DateTime.Now < Convert.ToDateTime("06:00:00 AM") Then
            Dim dt As DateTime = DateTime.Now

            lblDate0.Text = dt.AddDays(-1).ToString("dd/MM/yyyy")
        Else
            lblDate0.Text = DateTime.Now.ToString("dd/MM/yyyy")
        End If

        lblTime0.Text = DateTime.Now.ToString("hh:mm:ss")
        SelectedDate = Convert.ToDateTime(lblDate0.Text)

        dt.Rows.Clear()
        dt.Columns.Clear()
        rwcnt = 0
        DataGridView1.Visible = True


        dt.Columns.Add("id", GetType(String))
        dt.Columns.Add("ext_date", GetType(String))
        dt.Columns.Add("ext_time", GetType(String))
        dt.Columns.Add("ext_shift", GetType(String))
        dt.Columns.Add("treadcode", GetType(String))
        dt.Columns.Add("colorcode", GetType(String))
        dt.Columns.Add("description", GetType(String))
        dt.Columns.Add("truckno", GetType(String))
        dt.Columns.Add("location", GetType(String))
        dt.Columns.Add("treads", GetType(String))
        dt.Columns.Add("prod_date", GetType(String))
        dt.Columns.Add("prod_shift", GetType(String))
        dt.Columns.Add("pkdextid", GetType(String))
        dt.Columns.Add("Dual_Ext", GetType(String))
        dt.Columns.Add("Operator", GetType(String))
        dt.Columns.Add("Truck_No", GetType(String))

        ds = objcon.GETFinalDataColourcode()

        If ds.Tables(0).Rows.Count > 0 Then

            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1

                Dim dr As DataRow = dt.NewRow()

                dt.Rows.Add(dr)
                dt.Rows(rwcnt)(0) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(0))
                dt.Rows(rwcnt)(1) = Convert.ToDateTime(ds.Tables(0).Rows(i).ItemArray(1)).ToString("dd/MM/yyyy")
                dt.Rows(rwcnt)(2) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(2))
                dt.Rows(rwcnt)(3) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(3))
                dt.Rows(rwcnt)(4) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(4))
                dt.Rows(rwcnt)(5) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(5))
                dt.Rows(rwcnt)(6) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(6))
                dt.Rows(rwcnt)(7) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(7))
                dt.Rows(rwcnt)(8) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(8))
                dt.Rows(rwcnt)(9) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(9))
                dt.Rows(rwcnt)(10) = Convert.ToDateTime(ds.Tables(0).Rows(i).ItemArray(10)).ToString("dd/MM/yyyy")
                dt.Rows(rwcnt)(11) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(11))
                dt.Rows(rwcnt)(12) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(13))
                dt.Rows(rwcnt)(13) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(14))
                dt.Rows(rwcnt)(14) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(15))
                dt.Rows(rwcnt)(14) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(16))

                '' rwcnt += 1
            Next

            DataGridView1.DataSource = dt


        Else
            DataGridView1.Visible = False
            '  MessageBox.Show("No data Found !!!")
        End If

        BindCurrentDateandShift()
        displybuttonvalue()
    End Sub

    Private Sub BindCurrentDateandShift()
        Dim ShiftA As DateTime, ShiftB As DateTime, ShiftC As DateTime, EndDayTime As DateTime, NextDayStartTime As DateTime
        Dim now As DateTime = DateTime.Now
        Dim curTime As DateTime = DateTime.Now
        ShiftA = Convert.ToDateTime("06:01:00 AM")
        ShiftB = Convert.ToDateTime("02:01:00 PM")
        ShiftC = Convert.ToDateTime("10:01:00 PM")
        EndDayTime = Convert.ToDateTime("11:59:55 PM")
        NextDayStartTime = Convert.ToDateTime("12:00:00 AM")
        If curTime >= ShiftA AndAlso curTime < ShiftB Then

            lblShift0.Text = "A"
        ElseIf curTime >= ShiftB AndAlso curTime < ShiftC Then
            lblShift0.Text = "B"
        ElseIf curTime >= ShiftC AndAlso curTime <= EndDayTime Then
            lblShift0.Text = "C"
        ElseIf curTime >= NextDayStartTime AndAlso curTime < ShiftA Then
            lblShift0.Text = "C"
        Else
            lblShift0.Text = "G"
        End If

    End Sub
    Public Sub displybuttonvalue()
        GlobalVariables.pkdextid = GlobalVariables.idvalue
        If GlobalVariables.agevalue = "underaged" Then
            btnID.BackColor = Color.Red
            btnID.ForeColor = Color.White
            btnID.Text = "Under Aged"
            Label12.Text = "Remaining Time :"
            btnOK.Visible = False
        ElseIf GlobalVariables.agevalue = "aged" Then
            btnID.Text = "Aged"
            btnID.ForeColor = Color.White
            btnID.BackColor = Color.Green
            Label12.Text = "Time after Ageing :"
            btnOK.Visible = True
        ElseIf GlobalVariables.agevalue = "usetoday" Then
            btnID.Text = "Use Today"
            btnID.ForeColor = Color.White
            btnID.BackColor = Color.Orange
            Label12.Text = "Time :"
            btnOK.Visible = True
        ElseIf GlobalVariables.agevalue = "overaged" Then
            btnID.Text = "Over Aged"
            btnID.BackColor = Color.Blue
            btnID.ForeColor = Color.White

            btnOK.Visible = False
        End If
        Dim tagid, mhe As String

        Dsbtnvalue = objcon.TreadDetailsRetrieval()
        If Dsbtnvalue.Tables(0).Rows.Count > 0 Then
            GlobalVariables.dualext = Convert.ToString(Dsbtnvalue.Tables(0).Rows(0).ItemArray(20))


            lblTreadCode.Text = Convert.ToString(Dsbtnvalue.Tables(0).Rows(0).ItemArray(4))
            lblColorCode.Text = Convert.ToString(Dsbtnvalue.Tables(0).Rows(0).ItemArray(19))
            lblDescription.Text = Convert.ToString(Dsbtnvalue.Tables(0).Rows(0).ItemArray(5))
            lblTruckNo.Text = Convert.ToString(Dsbtnvalue.Tables(0).Rows(0).ItemArray(6))
            lblLocation.Text = Convert.ToString(Dsbtnvalue.Tables(0).Rows(0).ItemArray(8))

            lblTreads.Text = Convert.ToString(Dsbtnvalue.Tables(0).Rows(0).ItemArray(7))
            lblDate.Text = Convert.ToDateTime(Dsbtnvalue.Tables(0).Rows(0).ItemArray(1)).ToString("dd/MM/yyyy")
            lblTime.Text = Convert.ToString(Dsbtnvalue.Tables(0).Rows(0).ItemArray(2))
            lblShift.Text = Convert.ToString(Dsbtnvalue.Tables(0).Rows(0).ItemArray(3))
            lblAge.Text = GlobalVariables.timevaluehr

            GlobalVariables.No = Convert.ToString(Dsbtnvalue.Tables(0).Rows(0).ItemArray(22))
            GlobalVariables.Qty = Convert.ToString(Dsbtnvalue.Tables(0).Rows(0).ItemArray(21))

        End If
        Dim location As String
        mhe = lblTruckNo.Text
        tagid = objcon.GetTagNameForMHEName(mhe)
        location = objcon.GetMHELocation(tagid)
        lblActualLoc.Text = location
    End Sub
    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.Close()
        FrmTakentoTB.Show()
    End Sub

    Private Sub btnHOLD_Click(sender As Object, e As EventArgs)
        Me.Close()
        FrmHold.Show()
    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click

        If GlobalVariables.agevalue = "underaged" Then
            MessageBox.Show("under aged....!!!!")
        Else
            Dim okextid As Single = objcon.get_DextOKID()
            GlobalVariables.id = okextid
            GlobalVariables.pkdextid = GlobalVariables.idvalue


            SelectedDate = Convert.ToDateTime(lblDate0.Text)
            GlobalVariables.extdate = Convert.ToDateTime(SelectedDate).ToString("MM/dd/yyyy")


            GlobalVariables.exttime = lblTime0.Text
            GlobalVariables.extshift = lblShift0.Text
            GlobalVariables.TreadCode = lblTreadCode.Text
            GlobalVariables.colorcode = lblColorCode.Text
            GlobalVariables.description = lblDescription.Text
            GlobalVariables.truckno = lblTruckNo.Text
            GlobalVariables.Location = lblLocation.Text
            GlobalVariables.treads = lblTreads.Text
            SelectedprodDate = Convert.ToDateTime(lblDate.Text)
            GlobalVariables.proddate = Convert.ToDateTime(SelectedprodDate).ToString("MM/dd/yyyy")


            GlobalVariables.prodshift = lblShift.Text
            GlobalVariables.prodtime = lblTime.Text

            GlobalVariables.SerialNo = lblTruckNo.Text

            

            objcon.insertOKEntery() 'insert to dextoktread table
            objcon.insertOK() 'Staus updation to true in dexttread entry

            objcon.TrueUpdateLocation()

            If lblColorCode.Text.StartsWith("SW") Then
                GlobalVariables.Defect_Type = "SW"
                objcon.TrueUpdateLeaftruckSW()
            Else
                GlobalVariables.Defect_Type = "other"
                objcon.insertTBRequest()
                objcon.TrueUpdateLeaftruck()
            End If

            Me.Close()
            FrmTakentoTB.Show()
        End If
    End Sub

    Private Sub lblTime_Click(sender As Object, e As EventArgs) Handles lblTime.Click

    End Sub

    Private Sub lblTreadCode_Click(sender As Object, e As EventArgs) Handles lblTreadCode.Click

    End Sub

    Private Sub lblTreads_Click(sender As Object, e As EventArgs) Handles lblTreads.Click

    End Sub

    Private Sub Label10_Click(sender As Object, e As EventArgs) Handles Label10.Click

    End Sub

    Private Sub lblColorCode_Click(sender As Object, e As EventArgs) Handles lblColorCode.Click

    End Sub

    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub

    Private Sub Label4_Click(sender As Object, e As EventArgs) Handles Label4.Click

    End Sub

    Private Sub btnID_Click(sender As Object, e As EventArgs) Handles btnID.Click

    End Sub

    Private Sub Label14_Click(sender As Object, e As EventArgs) Handles Label14.Click

    End Sub

    Private Sub Label6_Click(sender As Object, e As EventArgs) Handles Label6.Click

    End Sub

    Private Sub Label16_Click(sender As Object, e As EventArgs) Handles Label16.Click

    End Sub

    Private Sub Label20_Click(sender As Object, e As EventArgs) Handles Label20.Click

    End Sub

    Private Sub lblDate_Click(sender As Object, e As EventArgs) Handles lblDate.Click

    End Sub
End Class