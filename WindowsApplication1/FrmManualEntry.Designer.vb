﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmManualEntry
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmManualEntry))
        Me.lblDate1 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.lblTime24 = New System.Windows.Forms.Label()
        Me.lblDate0 = New System.Windows.Forms.Label()
        Me.lblTime0 = New System.Windows.Forms.Label()
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument()
        Me.txtOperator = New System.Windows.Forms.TextBox()
        Me.Lblcomp = New System.Windows.Forms.Label()
        Me.PrintDocument2 = New System.Drawing.Printing.PrintDocument()
        Me.LblDesc = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtColourCode = New System.Windows.Forms.TextBox()
        Me.txtItemCode = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.btnDel = New System.Windows.Forms.Button()
        Me.lblfinal = New System.Windows.Forms.Label()
        Me.PrintPreviewDialog1 = New System.Windows.Forms.PrintPreviewDialog()
        Me.totdattime = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.lblhr = New System.Windows.Forms.Label()
        Me.CboLocation = New System.Windows.Forms.ComboBox()
        Me.txtNo = New System.Windows.Forms.TextBox()
        Me.Lblno = New System.Windows.Forms.Label()
        Me.txtQty = New System.Windows.Forms.TextBox()
        Me.lblCode = New System.Windows.Forms.Label()
        Me.cboCode = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.btnBack = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Shift = New System.Windows.Forms.ComboBox()
        Me.Fdate = New System.Windows.Forms.DateTimePicker()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.sec = New System.Windows.Forms.ComboBox()
        Me.min = New System.Windows.Forms.ComboBox()
        Me.hr = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblDate1
        '
        Me.lblDate1.AutoSize = True
        Me.lblDate1.Location = New System.Drawing.Point(15, 462)
        Me.lblDate1.Name = "lblDate1"
        Me.lblDate1.Size = New System.Drawing.Size(39, 13)
        Me.lblDate1.TabIndex = 107
        Me.lblDate1.Text = "Label3"
        Me.lblDate1.Visible = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(15, 429)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(39, 13)
        Me.Label6.TabIndex = 106
        Me.Label6.Text = "Label6"
        Me.Label6.Visible = False
        '
        'lblTime24
        '
        Me.lblTime24.AutoSize = True
        Me.lblTime24.Location = New System.Drawing.Point(15, 397)
        Me.lblTime24.Name = "lblTime24"
        Me.lblTime24.Size = New System.Drawing.Size(39, 13)
        Me.lblTime24.TabIndex = 105
        Me.lblTime24.Text = "Label5"
        Me.lblTime24.Visible = False
        '
        'lblDate0
        '
        Me.lblDate0.AutoSize = True
        Me.lblDate0.Location = New System.Drawing.Point(15, 353)
        Me.lblDate0.Name = "lblDate0"
        Me.lblDate0.Size = New System.Drawing.Size(39, 13)
        Me.lblDate0.TabIndex = 104
        Me.lblDate0.Text = "Label3"
        Me.lblDate0.Visible = False
        '
        'lblTime0
        '
        Me.lblTime0.AutoSize = True
        Me.lblTime0.Location = New System.Drawing.Point(15, 376)
        Me.lblTime0.Name = "lblTime0"
        Me.lblTime0.Size = New System.Drawing.Size(39, 13)
        Me.lblTime0.TabIndex = 103
        Me.lblTime0.Text = "Label4"
        Me.lblTime0.Visible = False
        '
        'btnPrint
        '
        Me.btnPrint.BackColor = System.Drawing.Color.Yellow
        Me.btnPrint.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrint.Location = New System.Drawing.Point(583, 338)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(296, 83)
        Me.btnPrint.TabIndex = 101
        Me.btnPrint.Text = "Print"
        Me.btnPrint.UseVisualStyleBackColor = False
        '
        'PrintDocument1
        '
        '
        'txtOperator
        '
        Me.txtOperator.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOperator.Location = New System.Drawing.Point(153, 25)
        Me.txtOperator.Name = "txtOperator"
        Me.txtOperator.Size = New System.Drawing.Size(226, 29)
        Me.txtOperator.TabIndex = 89
        '
        'Lblcomp
        '
        Me.Lblcomp.AutoSize = True
        Me.Lblcomp.Location = New System.Drawing.Point(15, 501)
        Me.Lblcomp.Name = "Lblcomp"
        Me.Lblcomp.Size = New System.Drawing.Size(39, 13)
        Me.Lblcomp.TabIndex = 108
        Me.Lblcomp.Text = "Label3"
        Me.Lblcomp.Visible = False
        '
        'PrintDocument2
        '
        '
        'LblDesc
        '
        Me.LblDesc.AutoSize = True
        Me.LblDesc.Location = New System.Drawing.Point(90, 462)
        Me.LblDesc.Name = "LblDesc"
        Me.LblDesc.Size = New System.Drawing.Size(32, 13)
        Me.LblDesc.TabIndex = 109
        Me.LblDesc.Text = "Desc"
        Me.LblDesc.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(3, 21)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(144, 32)
        Me.Label2.TabIndex = 88
        Me.Label2.Text = "OPERATOR"
        '
        'txtColourCode
        '
        Me.txtColourCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtColourCode.Location = New System.Drawing.Point(211, 79)
        Me.txtColourCode.Name = "txtColourCode"
        Me.txtColourCode.Size = New System.Drawing.Size(226, 29)
        Me.txtColourCode.TabIndex = 87
        '
        'txtItemCode
        '
        Me.txtItemCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtItemCode.Location = New System.Drawing.Point(211, 26)
        Me.txtItemCode.Name = "txtItemCode"
        Me.txtItemCode.Size = New System.Drawing.Size(226, 29)
        Me.txtItemCode.TabIndex = 86
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(17, 79)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(169, 32)
        Me.Label1.TabIndex = 85
        Me.Label1.Text = "COLOR CODE"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.White
        Me.Label19.Location = New System.Drawing.Point(17, 22)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(145, 32)
        Me.Label19.TabIndex = 82
        Me.Label19.Text = "ITEM CODE"
        '
        'btnDel
        '
        Me.btnDel.BackColor = System.Drawing.Color.CornflowerBlue
        Me.btnDel.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDel.Location = New System.Drawing.Point(762, 433)
        Me.btnDel.Name = "btnDel"
        Me.btnDel.Size = New System.Drawing.Size(296, 83)
        Me.btnDel.TabIndex = 111
        Me.btnDel.Text = "Delete "
        Me.btnDel.UseVisualStyleBackColor = False
        Me.btnDel.Visible = False
        '
        'lblfinal
        '
        Me.lblfinal.AutoSize = True
        Me.lblfinal.Location = New System.Drawing.Point(65, 523)
        Me.lblfinal.Name = "lblfinal"
        Me.lblfinal.Size = New System.Drawing.Size(21, 13)
        Me.lblfinal.TabIndex = 110
        Me.lblfinal.Text = "Lbl"
        Me.lblfinal.Visible = False
        '
        'PrintPreviewDialog1
        '
        Me.PrintPreviewDialog1.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.PrintPreviewDialog1.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.PrintPreviewDialog1.ClientSize = New System.Drawing.Size(400, 300)
        Me.PrintPreviewDialog1.Enabled = True
        Me.PrintPreviewDialog1.Icon = CType(resources.GetObject("PrintPreviewDialog1.Icon"), System.Drawing.Icon)
        Me.PrintPreviewDialog1.Name = "PrintPreviewDialog1"
        Me.PrintPreviewDialog1.Visible = False
        '
        'totdattime
        '
        Me.totdattime.AutoSize = True
        Me.totdattime.Location = New System.Drawing.Point(262, 450)
        Me.totdattime.Name = "totdattime"
        Me.totdattime.Size = New System.Drawing.Size(39, 13)
        Me.totdattime.TabIndex = 113
        Me.totdattime.Text = "Label3"
        Me.totdattime.Visible = False
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.White
        Me.Label14.Location = New System.Drawing.Point(791, 48)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(79, 26)
        Me.Label14.TabIndex = 62
        Me.Label14.Text = "SHIFT"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Location = New System.Drawing.Point(476, 48)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(74, 26)
        Me.Label7.TabIndex = 60
        Me.Label7.Text = "DATE"
        '
        'lblhr
        '
        Me.lblhr.AutoSize = True
        Me.lblhr.Location = New System.Drawing.Point(60, 433)
        Me.lblhr.Name = "lblhr"
        Me.lblhr.Size = New System.Drawing.Size(39, 13)
        Me.lblhr.TabIndex = 112
        Me.lblhr.Text = "Label3"
        Me.lblhr.Visible = False
        '
        'CboLocation
        '
        Me.CboLocation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CboLocation.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CboLocation.FormattingEnabled = True
        Me.CboLocation.Location = New System.Drawing.Point(462, 86)
        Me.CboLocation.Name = "CboLocation"
        Me.CboLocation.Size = New System.Drawing.Size(88, 33)
        Me.CboLocation.TabIndex = 93
        '
        'txtNo
        '
        Me.txtNo.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNo.Location = New System.Drawing.Point(959, 89)
        Me.txtNo.Name = "txtNo"
        Me.txtNo.Size = New System.Drawing.Size(87, 29)
        Me.txtNo.TabIndex = 92
        '
        'Lblno
        '
        Me.Lblno.AutoSize = True
        Me.Lblno.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lblno.Location = New System.Drawing.Point(814, 87)
        Me.Lblno.Name = "Lblno"
        Me.Lblno.Size = New System.Drawing.Size(139, 32)
        Me.Lblno.TabIndex = 91
        Me.Lblno.Text = "TRUCK NO"
        '
        'txtQty
        '
        Me.txtQty.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtQty.Location = New System.Drawing.Point(719, 88)
        Me.txtQty.Name = "txtQty"
        Me.txtQty.Size = New System.Drawing.Size(89, 29)
        Me.txtQty.TabIndex = 90
        '
        'lblCode
        '
        Me.lblCode.AutoSize = True
        Me.lblCode.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCode.Location = New System.Drawing.Point(3, 86)
        Me.lblCode.Name = "lblCode"
        Me.lblCode.Size = New System.Drawing.Size(202, 32)
        Me.lblCode.TabIndex = 78
        Me.lblCode.Text = "LEAF TRUCK NO"
        '
        'cboCode
        '
        Me.cboCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCode.FormattingEnabled = True
        Me.cboCode.Location = New System.Drawing.Point(211, 84)
        Me.cboCode.Name = "cboCode"
        Me.cboCode.Size = New System.Drawing.Size(112, 33)
        Me.cboCode.TabIndex = 76
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(565, 87)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(138, 32)
        Me.Label8.TabIndex = 67
        Me.Label8.Text = "QUANTITY"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(329, 86)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(137, 32)
        Me.Label9.TabIndex = 66
        Me.Label9.Text = "LOCATION"
        '
        'btnBack
        '
        Me.btnBack.BackColor = System.Drawing.Color.SpringGreen
        Me.btnBack.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBack.Location = New System.Drawing.Point(210, 338)
        Me.btnBack.Name = "btnBack"
        Me.btnBack.Size = New System.Drawing.Size(310, 83)
        Me.btnBack.TabIndex = 102
        Me.btnBack.Text = "Back"
        Me.btnBack.UseVisualStyleBackColor = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.DarkMagenta
        Me.Panel1.Controls.Add(Me.Shift)
        Me.Panel1.Controls.Add(Me.Fdate)
        Me.Panel1.Controls.Add(Me.txtColourCode)
        Me.Panel1.Controls.Add(Me.txtItemCode)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.Label19)
        Me.Panel1.Controls.Add(Me.Label14)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1049, 134)
        Me.Panel1.TabIndex = 100
        '
        'Shift
        '
        Me.Shift.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Shift.FormattingEnabled = True
        Me.Shift.Items.AddRange(New Object() {"A", "B", "C"})
        Me.Shift.Location = New System.Drawing.Point(897, 48)
        Me.Shift.Name = "Shift"
        Me.Shift.Size = New System.Drawing.Size(56, 28)
        Me.Shift.TabIndex = 110
        '
        'Fdate
        '
        Me.Fdate.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Fdate.Location = New System.Drawing.Point(571, 48)
        Me.Fdate.Name = "Fdate"
        Me.Fdate.Size = New System.Drawing.Size(193, 26)
        Me.Fdate.TabIndex = 109
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.LightSalmon
        Me.Panel3.Controls.Add(Me.sec)
        Me.Panel3.Controls.Add(Me.min)
        Me.Panel3.Controls.Add(Me.hr)
        Me.Panel3.Controls.Add(Me.Label3)
        Me.Panel3.Controls.Add(Me.txtOperator)
        Me.Panel3.Controls.Add(Me.Label2)
        Me.Panel3.Controls.Add(Me.CboLocation)
        Me.Panel3.Controls.Add(Me.txtNo)
        Me.Panel3.Controls.Add(Me.Lblno)
        Me.Panel3.Controls.Add(Me.txtQty)
        Me.Panel3.Controls.Add(Me.lblCode)
        Me.Panel3.Controls.Add(Me.cboCode)
        Me.Panel3.Controls.Add(Me.Label8)
        Me.Panel3.Controls.Add(Me.Label9)
        Me.Panel3.Location = New System.Drawing.Point(12, 152)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(1049, 168)
        Me.Panel3.TabIndex = 99
        '
        'sec
        '
        Me.sec.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.sec.FormattingEnabled = True
        Me.sec.Items.AddRange(New Object() {"00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59"})
        Me.sec.Location = New System.Drawing.Point(695, 25)
        Me.sec.Name = "sec"
        Me.sec.Size = New System.Drawing.Size(43, 28)
        Me.sec.TabIndex = 113
        '
        'min
        '
        Me.min.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.min.FormattingEnabled = True
        Me.min.Items.AddRange(New Object() {"00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59"})
        Me.min.Location = New System.Drawing.Point(633, 25)
        Me.min.Name = "min"
        Me.min.Size = New System.Drawing.Size(43, 28)
        Me.min.TabIndex = 112
        '
        'hr
        '
        Me.hr.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.hr.FormattingEnabled = True
        Me.hr.Items.AddRange(New Object() {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "00"})
        Me.hr.Location = New System.Drawing.Point(571, 25)
        Me.hr.Name = "hr"
        Me.hr.Size = New System.Drawing.Size(42, 28)
        Me.hr.TabIndex = 111
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Black
        Me.Label3.Location = New System.Drawing.Point(478, 21)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(72, 32)
        Me.Label3.TabIndex = 94
        Me.Label3.Text = "TIME"
        '
        'FrmManualEntry
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1114, 526)
        Me.Controls.Add(Me.lblDate1)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.lblTime24)
        Me.Controls.Add(Me.lblDate0)
        Me.Controls.Add(Me.lblTime0)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.Lblcomp)
        Me.Controls.Add(Me.LblDesc)
        Me.Controls.Add(Me.btnDel)
        Me.Controls.Add(Me.lblfinal)
        Me.Controls.Add(Me.totdattime)
        Me.Controls.Add(Me.lblhr)
        Me.Controls.Add(Me.btnBack)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Panel3)
        Me.Name = "FrmManualEntry"
        Me.Text = "FrmManualEntry"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblDate1 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents lblTime24 As System.Windows.Forms.Label
    Friend WithEvents lblDate0 As System.Windows.Forms.Label
    Friend WithEvents lblTime0 As System.Windows.Forms.Label
    Friend WithEvents btnPrint As System.Windows.Forms.Button
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
    Friend WithEvents txtOperator As System.Windows.Forms.TextBox
    Friend WithEvents Lblcomp As System.Windows.Forms.Label
    Friend WithEvents PrintDocument2 As System.Drawing.Printing.PrintDocument
    Friend WithEvents LblDesc As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtColourCode As System.Windows.Forms.TextBox
    Friend WithEvents txtItemCode As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents btnDel As System.Windows.Forms.Button
    Friend WithEvents lblfinal As System.Windows.Forms.Label
    Friend WithEvents PrintPreviewDialog1 As System.Windows.Forms.PrintPreviewDialog
    Friend WithEvents totdattime As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents lblhr As System.Windows.Forms.Label
    Friend WithEvents CboLocation As System.Windows.Forms.ComboBox
    Friend WithEvents txtNo As System.Windows.Forms.TextBox
    Friend WithEvents Lblno As System.Windows.Forms.Label
    Friend WithEvents txtQty As System.Windows.Forms.TextBox
    Friend WithEvents lblCode As System.Windows.Forms.Label
    Friend WithEvents cboCode As System.Windows.Forms.ComboBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents btnBack As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Public WithEvents Shift As System.Windows.Forms.ComboBox
    Friend WithEvents Fdate As System.Windows.Forms.DateTimePicker
    Public WithEvents sec As System.Windows.Forms.ComboBox
    Public WithEvents min As System.Windows.Forms.ComboBox
    Public WithEvents hr As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
End Class
