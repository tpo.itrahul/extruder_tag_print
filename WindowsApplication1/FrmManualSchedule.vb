﻿Public Class FrmManualSchedule
    Dim DT As DataTable
    Dim DTSpec As DataTable
    Dim objcon As New Connections
    Dim DualExactDate As Date
    Dim DualExactShift As String
    Private Sub BtnSubmit_Click(sender As Object, e As EventArgs) Handles BtnSubmit.Click

        If cboMachine.Text = "dual1" Or cboMachine.Text = "dual2" Then



            GlobalVariables.FromDate = Fdate.Text
            GlobalVariables.ToDate = TDate.Text
            GlobalVariables.DualSelection = cboMachine.Text

            If GlobalVariables.FromDate = "" And GlobalVariables.ToDate = "" And (cboMachine.Text = "dual1" Or cboMachine.Text = "dual2") Then
                MessageBox.Show("Select All Details")
            Else
                DT = objcon.GetDualAllSchedule().Tables(0)

                If DT.Rows.Count > 0 Then



                    DGSchSelection.AutoGenerateColumns = False
                    DGSchSelection.Columns(1).DataPropertyName = "Ext_code"
                    DGSchSelection.Columns(2).DataPropertyName = "Tread_Name"
                    DGSchSelection.Columns(3).DataPropertyName = "priority_No"
                    DGSchSelection.Columns(4).DataPropertyName = "Tread_Code"
                    DGSchSelection.Columns(5).DataPropertyName = "sch_value"
                    DGSchSelection.Columns(6).DataPropertyName = "Comp_Cap"
                    DGSchSelection.Columns(7).DataPropertyName = "Comp_Base"
                    DGSchSelection.Columns(8).DataPropertyName = "Comp_Cushion"

                    DGSchSelection.DataSource = DT

                End If
            End If
        Else
            MessageBox.Show("Select Dual1 or Dual2 Details")
        End If

    End Sub

    Private Sub DGSchSelection_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DGSchSelection.CellContentClick
        If e.ColumnIndex = 9 Then
            GlobalVariables.SelectionTreadCode = DGSchSelection.Rows(e.RowIndex).Cells(1).Value()
            GlobalVariables.ItemCode = DGSchSelection.Rows(e.RowIndex).Cells(4).Value()
            GlobalVariables.description = DGSchSelection.Rows(e.RowIndex).Cells(2).Value()
            GlobalVariables.Splicer1 = DGSchSelection.Rows(e.RowIndex).Cells(6).Value()
            GlobalVariables.Splicer2 = DGSchSelection.Rows(e.RowIndex).Cells(7).Value()
            GlobalVariables.Splicer3 = DGSchSelection.Rows(e.RowIndex).Cells(8).Value()
            FrmManualEntry.Show()

        End If
    End Sub

    Private Sub BtnCancel_Click(sender As Object, e As EventArgs) Handles BtnCancel.Click
        Me.Close()
        FrmSelection.Show()
    End Sub
End Class