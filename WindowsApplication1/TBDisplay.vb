﻿Public Class TBDisplay
    Public Dsbtnvalue As New DataSet()
    Dim objcon As New Connections
    Public SelectedDate As DateTime
    Public SelectedprodDate As DateTime
    Public DsTreads As New DataSet()
    Private Sub TBDisplay_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If DateTime.Now > Convert.ToDateTime("12:00:00 AM") AndAlso DateTime.Now < Convert.ToDateTime("06:00:00 AM") Then
            Dim dt As DateTime = DateTime.Now

            lblDate0.Text = dt.AddDays(-1).ToString("dd/MM/yyyy")
        Else
            lblDate0.Text = DateTime.Now.ToString("dd/MM/yyyy")
        End If

        lblTime0.Text = DateTime.Now.ToString("hh:mm:ss")
        SelectedDate = Convert.ToDateTime(lblDate0.Text)

        Dim str As String = GlobalVariables.colourcode
        str = str.Replace(" ", "")
        GlobalVariables.ccode = str
        DsTreads = objcon.SpecificTreadRetrieval1()
        If DsTreads.Tables(0).Rows.Count > 0 Then
            GlobalVariables.time = Convert.ToString(DsTreads.Tables(0).Rows(0).ItemArray(9))
            Dim timespan As TimeSpan
            timespan = DateTime.Now - Convert.ToDateTime(GlobalVariables.time)
            Dim intMinutes As Double = timespan.TotalMinutes
            Dim days As Integer = timespan.Days
            Dim span As TimeSpan = timespan.FromMinutes(intMinutes)


            If days = 0 Then
                Dim answer As String = String.Format("{0:D2}H:{1:D2}M:{2:D2}S", span.Hours, span.Minutes, span.Seconds)
                GlobalVariables.timevaluehr = answer
            Else
                Dim answer As String = String.Format("{0:D2}D:{1:D2}H:{2:D2}M:{3:D2}S", span.Days, span.Hours, span.Minutes, span.Seconds)
                GlobalVariables.timevaluehr = answer
            End If

            GlobalVariables.selectTread = GlobalVariables.ccode
            Dim count As Single = objcon.Check_Tread()
            If count > 0 Then
                If intMinutes < 181 Then

                    GlobalVariables.agevalue = "underaged"
                    GlobalVariables.idvalue = Convert.ToString(DsTreads.Tables(0).Rows(0).ItemArray(0))

                ElseIf intMinutes > 180 AndAlso intMinutes < 8641 Then

                    GlobalVariables.agevalue = "aged"
                    GlobalVariables.idvalue = Convert.ToString(DsTreads.Tables(0).Rows(0).ItemArray(0))

                ElseIf intMinutes > 8640 AndAlso intMinutes < 10081 Then

                    GlobalVariables.agevalue = "usetoday"
                    GlobalVariables.idvalue = Convert.ToString(DsTreads.Tables(0).Rows(0).ItemArray(0))

                Else

                    GlobalVariables.agevalue = "overaged"
                    GlobalVariables.idvalue = Convert.ToString(DsTreads.Tables(0).Rows(0).ItemArray(0))

                End If

                GlobalVariables.timevalue = intMinutes

                'Me.Close()
                'FrmDisplay.Show()

            Else
                If intMinutes < 241 Then
                    GlobalVariables.agevalue = "underaged"
                    GlobalVariables.idvalue = Convert.ToString(DsTreads.Tables(0).Rows(0).ItemArray(0))
                ElseIf intMinutes > 240 AndAlso intMinutes < 8641 Then
                    GlobalVariables.agevalue = "aged"
                    GlobalVariables.idvalue = Convert.ToString(DsTreads.Tables(0).Rows(0).ItemArray(0))
                ElseIf intMinutes > 8640 AndAlso intMinutes < 10081 Then
                    GlobalVariables.agevalue = "usetoday"
                    GlobalVariables.idvalue = Convert.ToString(DsTreads.Tables(0).Rows(0).ItemArray(0))
                Else
                    GlobalVariables.agevalue = "overaged"
                    GlobalVariables.idvalue = Convert.ToString(DsTreads.Tables(0).Rows(0).ItemArray(0))
                End If
                GlobalVariables.timevalue = intMinutes

                'Me.Close()
                'FrmDisplay.Show()


            End If
        Else
            MessageBox.Show("Not available....!!!!")
        End If
       

     
            BindCurrentDateandShift()
            displybuttonvalue()

       
    End Sub
    Private Sub BindCurrentDateandShift()
        Dim ShiftA As DateTime, ShiftB As DateTime, ShiftC As DateTime, EndDayTime As DateTime, NextDayStartTime As DateTime
        Dim now As DateTime = DateTime.Now
        Dim curTime As DateTime = DateTime.Now
        ShiftA = Convert.ToDateTime("06:01:00 AM")
        ShiftB = Convert.ToDateTime("02:01:00 PM")
        ShiftC = Convert.ToDateTime("10:01:00 PM")
        EndDayTime = Convert.ToDateTime("11:59:55 PM")
        NextDayStartTime = Convert.ToDateTime("12:00:00 AM")
        If curTime >= ShiftA AndAlso curTime < ShiftB Then

            lblShift0.Text = "A"
        ElseIf curTime >= ShiftB AndAlso curTime < ShiftC Then
            lblShift0.Text = "B"
        ElseIf curTime >= ShiftC AndAlso curTime <= EndDayTime Then
            lblShift0.Text = "C"
        ElseIf curTime >= NextDayStartTime AndAlso curTime < ShiftA Then
            lblShift0.Text = "C"
        Else
            lblShift0.Text = "G"
        End If

    End Sub
    Public Sub displybuttonvalue()
        GlobalVariables.pkdextid = GlobalVariables.id
        If GlobalVariables.agevalue = "underaged" Then
            btnID.BackColor = Color.Red
            btnID.ForeColor = Color.White
            btnID.Text = "Under Aged"

            btnOK.Visible = False
        ElseIf GlobalVariables.agevalue = "aged" Then
            btnID.Text = "Aged"
            btnID.ForeColor = Color.White
            btnID.BackColor = Color.Green

            btnOK.Visible = True
        ElseIf GlobalVariables.agevalue = "usetoday" Then
            btnID.Text = "Use Today"
            btnID.ForeColor = Color.White
            btnID.BackColor = Color.Orange

            btnOK.Visible = True
        ElseIf GlobalVariables.agevalue = "overaged" Then
            btnID.Text = "Over Aged"
            btnID.BackColor = Color.Blue
            btnID.ForeColor = Color.White

            btnOK.Visible = False
        End If

        Dsbtnvalue = objcon.TreadDetailsRetrieval()
        If Dsbtnvalue.Tables(0).Rows.Count > 0 Then
            GlobalVariables.dualext = Convert.ToString(Dsbtnvalue.Tables(0).Rows(0).ItemArray(20))


            lblTreadCode.Text = Convert.ToString(Dsbtnvalue.Tables(0).Rows(0).ItemArray(4))
            lblColorCode.Text = Convert.ToString(Dsbtnvalue.Tables(0).Rows(0).ItemArray(19))
            lblDescription.Text = Convert.ToString(Dsbtnvalue.Tables(0).Rows(0).ItemArray(5))
            lblTruckNo.Text = Convert.ToString(Dsbtnvalue.Tables(0).Rows(0).ItemArray(6))
            lblLocation.Text = Convert.ToString(Dsbtnvalue.Tables(0).Rows(0).ItemArray(8))
            lblTreads.Text = Convert.ToString(Dsbtnvalue.Tables(0).Rows(0).ItemArray(7))
            lblDate.Text = Convert.ToDateTime(Dsbtnvalue.Tables(0).Rows(0).ItemArray(1)).ToString("dd/MM/yyyy")
            lblTime.Text = Convert.ToString(Dsbtnvalue.Tables(0).Rows(0).ItemArray(2))
            lblShift.Text = Convert.ToString(Dsbtnvalue.Tables(0).Rows(0).ItemArray(3))
            lblAge.Text = GlobalVariables.timevaluehr

            GlobalVariables.No = Convert.ToString(Dsbtnvalue.Tables(0).Rows(0).ItemArray(22))
            GlobalVariables.Qty = Convert.ToString(Dsbtnvalue.Tables(0).Rows(0).ItemArray(21))

        End If
    End Sub
    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.Close()
        DisplayAllTread.Show()
    End Sub

    Private Sub btnHOLD_Click(sender As Object, e As EventArgs) Handles btnHOLD.Click
        Me.Close()
        FrmHold.Show()
    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click

        If GlobalVariables.agevalue = "underaged" Then
            MessageBox.Show("under aged....!!!!")
        Else
            Dim okextid As Single = objcon.get_DextOKID()
            GlobalVariables.id = okextid
            GlobalVariables.pkdextid = GlobalVariables.idvalue


            SelectedDate = Convert.ToDateTime(lblDate0.Text)
            GlobalVariables.extdate = Convert.ToDateTime(SelectedDate).ToString("MM/dd/yyyy")


            GlobalVariables.exttime = lblTime0.Text
            GlobalVariables.extshift = lblShift0.Text
            GlobalVariables.TreadCode = lblTreadCode.Text
            GlobalVariables.colorcode = lblColorCode.Text
            GlobalVariables.description = lblDescription.Text
            GlobalVariables.truckno = lblTruckNo.Text
            GlobalVariables.Location = lblLocation.Text
            GlobalVariables.treads = lblTreads.Text
            SelectedprodDate = Convert.ToDateTime(lblDate.Text)
            GlobalVariables.proddate = Convert.ToDateTime(SelectedprodDate).ToString("MM/dd/yyyy")


            GlobalVariables.prodshift = lblShift.Text
            GlobalVariables.prodtime = lblTime.Text

            GlobalVariables.SerialNo = lblTruckNo.Text

            objcon.insertOKEntery() 'insert to dextoktread table
            objcon.insertOK() 'Staus updation to true in dexttread entry table
            objcon.TrueUpdateLocation()
            objcon.TrueUpdateLeaftruck()

            Me.Close()
            TBSelection.Show()
        End If
    End Sub
End Class