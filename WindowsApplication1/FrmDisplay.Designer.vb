﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmDisplay
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.lblCode = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblTreadCode = New System.Windows.Forms.Label()
        Me.lblDescription = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lblTruckNo = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.lblLocation = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.lblTreads = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.lblAge = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.lblTime = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.lblShift = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.lblDate = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.lblColorCode = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnOK = New System.Windows.Forms.Button()
        Me.btnID = New System.Windows.Forms.Button()
        Me.lblDate0 = New System.Windows.Forms.Label()
        Me.lblTime0 = New System.Windows.Forms.Label()
        Me.lblShift0 = New System.Windows.Forms.Label()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblActualLoc = New System.Windows.Forms.Label()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblCode
        '
        Me.lblCode.AutoSize = True
        Me.lblCode.BackColor = System.Drawing.Color.Transparent
        Me.lblCode.Font = New System.Drawing.Font("Malgun Gothic", 35.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCode.ForeColor = System.Drawing.Color.Indigo
        Me.lblCode.Location = New System.Drawing.Point(412, 9)
        Me.lblCode.Name = "lblCode"
        Me.lblCode.Size = New System.Drawing.Size(337, 62)
        Me.lblCode.TabIndex = 80
        Me.lblCode.Text = "FIFO DISPLAY"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Indigo
        Me.Label1.Location = New System.Drawing.Point(62, 127)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(167, 32)
        Me.Label1.TabIndex = 81
        Me.Label1.Text = "Tread Code : "
        '
        'lblTreadCode
        '
        Me.lblTreadCode.AutoSize = True
        Me.lblTreadCode.Font = New System.Drawing.Font("Malgun Gothic", 16.0!, System.Drawing.FontStyle.Bold)
        Me.lblTreadCode.ForeColor = System.Drawing.Color.Black
        Me.lblTreadCode.Location = New System.Drawing.Point(219, 127)
        Me.lblTreadCode.Name = "lblTreadCode"
        Me.lblTreadCode.Size = New System.Drawing.Size(185, 30)
        Me.lblTreadCode.TabIndex = 82
        Me.lblTreadCode.Text = "LEAF TRUCK NO"
        '
        'lblDescription
        '
        Me.lblDescription.AutoSize = True
        Me.lblDescription.Font = New System.Drawing.Font("Malgun Gothic", 16.0!, System.Drawing.FontStyle.Bold)
        Me.lblDescription.ForeColor = System.Drawing.Color.Black
        Me.lblDescription.Location = New System.Drawing.Point(221, 178)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(185, 30)
        Me.lblDescription.TabIndex = 84
        Me.lblDescription.Text = "LEAF TRUCK NO"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Indigo
        Me.Label4.Location = New System.Drawing.Point(62, 178)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(160, 32)
        Me.Label4.TabIndex = 83
        Me.Label4.Text = "Description :"
        '
        'lblTruckNo
        '
        Me.lblTruckNo.AutoSize = True
        Me.lblTruckNo.Font = New System.Drawing.Font("Malgun Gothic", 16.0!, System.Drawing.FontStyle.Bold)
        Me.lblTruckNo.ForeColor = System.Drawing.Color.Black
        Me.lblTruckNo.Location = New System.Drawing.Point(219, 231)
        Me.lblTruckNo.Name = "lblTruckNo"
        Me.lblTruckNo.Size = New System.Drawing.Size(185, 30)
        Me.lblTruckNo.TabIndex = 86
        Me.lblTruckNo.Text = "LEAF TRUCK NO"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Indigo
        Me.Label6.Location = New System.Drawing.Point(62, 231)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(134, 32)
        Me.Label6.TabIndex = 85
        Me.Label6.Text = "Truck No :"
        '
        'lblLocation
        '
        Me.lblLocation.AutoSize = True
        Me.lblLocation.Font = New System.Drawing.Font("Malgun Gothic", 16.0!, System.Drawing.FontStyle.Bold)
        Me.lblLocation.ForeColor = System.Drawing.Color.Black
        Me.lblLocation.Location = New System.Drawing.Point(219, 286)
        Me.lblLocation.Name = "lblLocation"
        Me.lblLocation.Size = New System.Drawing.Size(185, 30)
        Me.lblLocation.TabIndex = 88
        Me.lblLocation.Text = "LEAF TRUCK NO"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.Indigo
        Me.Label8.Location = New System.Drawing.Point(62, 285)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(127, 32)
        Me.Label8.TabIndex = 87
        Me.Label8.Text = "Location :"
        '
        'lblTreads
        '
        Me.lblTreads.AutoSize = True
        Me.lblTreads.Font = New System.Drawing.Font("Malgun Gothic", 16.0!, System.Drawing.FontStyle.Bold)
        Me.lblTreads.ForeColor = System.Drawing.Color.Black
        Me.lblTreads.Location = New System.Drawing.Point(820, 334)
        Me.lblTreads.Name = "lblTreads"
        Me.lblTreads.Size = New System.Drawing.Size(185, 30)
        Me.lblTreads.TabIndex = 90
        Me.lblTreads.Text = "LEAF TRUCK NO"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.Indigo
        Me.Label10.Location = New System.Drawing.Point(665, 332)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(104, 32)
        Me.Label10.TabIndex = 89
        Me.Label10.Text = "Treads :"
        '
        'lblAge
        '
        Me.lblAge.AutoSize = True
        Me.lblAge.Font = New System.Drawing.Font("Malgun Gothic", 16.0!, System.Drawing.FontStyle.Bold)
        Me.lblAge.ForeColor = System.Drawing.Color.Black
        Me.lblAge.Location = New System.Drawing.Point(302, 379)
        Me.lblAge.Name = "lblAge"
        Me.lblAge.Size = New System.Drawing.Size(185, 30)
        Me.lblAge.TabIndex = 100
        Me.lblAge.Text = "LEAF TRUCK NO"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.BackColor = System.Drawing.Color.Transparent
        Me.Label12.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.Indigo
        Me.Label12.Location = New System.Drawing.Point(62, 379)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(74, 32)
        Me.Label12.TabIndex = 99
        Me.Label12.Text = "Age :"
        '
        'lblTime
        '
        Me.lblTime.AutoSize = True
        Me.lblTime.Font = New System.Drawing.Font("Malgun Gothic", 16.0!, System.Drawing.FontStyle.Bold)
        Me.lblTime.ForeColor = System.Drawing.Color.Black
        Me.lblTime.Location = New System.Drawing.Point(823, 285)
        Me.lblTime.Name = "lblTime"
        Me.lblTime.Size = New System.Drawing.Size(185, 30)
        Me.lblTime.TabIndex = 98
        Me.lblTime.Text = "LEAF TRUCK NO"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.BackColor = System.Drawing.Color.Transparent
        Me.Label14.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.Indigo
        Me.Label14.Location = New System.Drawing.Point(667, 283)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(85, 32)
        Me.Label14.TabIndex = 97
        Me.Label14.Text = "Time :"
        '
        'lblShift
        '
        Me.lblShift.AutoSize = True
        Me.lblShift.Font = New System.Drawing.Font("Malgun Gothic", 16.0!, System.Drawing.FontStyle.Bold)
        Me.lblShift.ForeColor = System.Drawing.Color.Black
        Me.lblShift.Location = New System.Drawing.Point(823, 235)
        Me.lblShift.Name = "lblShift"
        Me.lblShift.Size = New System.Drawing.Size(185, 30)
        Me.lblShift.TabIndex = 96
        Me.lblShift.Text = "LEAF TRUCK NO"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.BackColor = System.Drawing.Color.Transparent
        Me.Label16.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.Indigo
        Me.Label16.Location = New System.Drawing.Point(667, 229)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(83, 32)
        Me.Label16.TabIndex = 95
        Me.Label16.Text = "Shift :"
        '
        'lblDate
        '
        Me.lblDate.AutoSize = True
        Me.lblDate.Font = New System.Drawing.Font("Malgun Gothic", 16.0!, System.Drawing.FontStyle.Bold)
        Me.lblDate.ForeColor = System.Drawing.Color.Black
        Me.lblDate.Location = New System.Drawing.Point(823, 181)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(185, 30)
        Me.lblDate.TabIndex = 94
        Me.lblDate.Text = "LEAF TRUCK NO"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.BackColor = System.Drawing.Color.Transparent
        Me.Label18.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.Indigo
        Me.Label18.Location = New System.Drawing.Point(667, 177)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(82, 32)
        Me.Label18.TabIndex = 93
        Me.Label18.Text = "Date :"
        '
        'lblColorCode
        '
        Me.lblColorCode.AutoSize = True
        Me.lblColorCode.Font = New System.Drawing.Font("Malgun Gothic", 16.0!, System.Drawing.FontStyle.Bold)
        Me.lblColorCode.ForeColor = System.Drawing.Color.Black
        Me.lblColorCode.Location = New System.Drawing.Point(823, 127)
        Me.lblColorCode.Name = "lblColorCode"
        Me.lblColorCode.Size = New System.Drawing.Size(185, 30)
        Me.lblColorCode.TabIndex = 92
        Me.lblColorCode.Text = "LEAF TRUCK NO"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.BackColor = System.Drawing.Color.Transparent
        Me.Label20.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.Indigo
        Me.Label20.Location = New System.Drawing.Point(665, 127)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(164, 32)
        Me.Label20.TabIndex = 91
        Me.Label20.Text = "Color Code  :"
        '
        'btnCancel
        '
        Me.btnCancel.BackColor = System.Drawing.Color.Purple
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btnCancel.Location = New System.Drawing.Point(1054, 233)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(155, 32)
        Me.btnCancel.TabIndex = 104
        Me.btnCancel.Text = "CANCEL"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'btnOK
        '
        Me.btnOK.BackColor = System.Drawing.Color.Purple
        Me.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOK.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btnOK.Location = New System.Drawing.Point(1052, 181)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(157, 32)
        Me.btnOK.TabIndex = 106
        Me.btnOK.Text = "TAKE TO TB"
        Me.btnOK.UseVisualStyleBackColor = False
        '
        'btnID
        '
        Me.btnID.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnID.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnID.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnID.Location = New System.Drawing.Point(1052, 127)
        Me.btnID.Name = "btnID"
        Me.btnID.Size = New System.Drawing.Size(157, 32)
        Me.btnID.TabIndex = 107
        Me.btnID.UseVisualStyleBackColor = False
        '
        'lblDate0
        '
        Me.lblDate0.AutoSize = True
        Me.lblDate0.Location = New System.Drawing.Point(62, 13)
        Me.lblDate0.Name = "lblDate0"
        Me.lblDate0.Size = New System.Drawing.Size(39, 13)
        Me.lblDate0.TabIndex = 108
        Me.lblDate0.Text = "Label2"
        Me.lblDate0.Visible = False
        '
        'lblTime0
        '
        Me.lblTime0.AutoSize = True
        Me.lblTime0.Location = New System.Drawing.Point(118, 13)
        Me.lblTime0.Name = "lblTime0"
        Me.lblTime0.Size = New System.Drawing.Size(39, 13)
        Me.lblTime0.TabIndex = 109
        Me.lblTime0.Text = "Label3"
        Me.lblTime0.Visible = False
        '
        'lblShift0
        '
        Me.lblShift0.AutoSize = True
        Me.lblShift0.Location = New System.Drawing.Point(180, 13)
        Me.lblShift0.Name = "lblShift0"
        Me.lblShift0.Size = New System.Drawing.Size(39, 13)
        Me.lblShift0.TabIndex = 110
        Me.lblShift0.Text = "Label5"
        Me.lblShift0.Visible = False
        '
        'DataGridView1
        '
        Me.DataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.DataGridView1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(68, 433)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(1139, 121)
        Me.DataGridView1.TabIndex = 131
        Me.DataGridView1.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Indigo
        Me.Label2.Location = New System.Drawing.Point(62, 334)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(149, 32)
        Me.Label2.TabIndex = 132
        Me.Label2.Text = "Actual Loc :"
        '
        'lblActualLoc
        '
        Me.lblActualLoc.AutoSize = True
        Me.lblActualLoc.Font = New System.Drawing.Font("Malgun Gothic", 16.0!, System.Drawing.FontStyle.Bold)
        Me.lblActualLoc.ForeColor = System.Drawing.Color.Black
        Me.lblActualLoc.Location = New System.Drawing.Point(221, 335)
        Me.lblActualLoc.Name = "lblActualLoc"
        Me.lblActualLoc.Size = New System.Drawing.Size(185, 30)
        Me.lblActualLoc.TabIndex = 133
        Me.lblActualLoc.Text = "LEAF TRUCK NO"
        '
        'FrmDisplay
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(1257, 572)
        Me.Controls.Add(Me.lblActualLoc)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.lblShift0)
        Me.Controls.Add(Me.lblTime0)
        Me.Controls.Add(Me.lblDate0)
        Me.Controls.Add(Me.btnID)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.lblAge)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.lblTime)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.lblShift)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.lblDate)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.lblColorCode)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.lblTreads)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.lblLocation)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.lblTruckNo)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.lblDescription)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.lblTreadCode)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblCode)
        Me.ForeColor = System.Drawing.Color.Indigo
        Me.Name = "FrmDisplay"
        Me.Text = "FrmDisplay"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblCode As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblTreadCode As System.Windows.Forms.Label
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lblTruckNo As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents lblLocation As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents lblTreads As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents lblAge As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents lblTime As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents lblShift As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents lblColorCode As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnOK As System.Windows.Forms.Button
    Friend WithEvents btnID As System.Windows.Forms.Button
    Friend WithEvents lblDate0 As System.Windows.Forms.Label
    Friend WithEvents lblTime0 As System.Windows.Forms.Label
    Friend WithEvents lblShift0 As System.Windows.Forms.Label
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Label2 As Label
    Friend WithEvents lblActualLoc As Label
End Class
