﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class TBDisplay
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblShift0 = New System.Windows.Forms.Label()
        Me.lblTime0 = New System.Windows.Forms.Label()
        Me.lblDate0 = New System.Windows.Forms.Label()
        Me.btnID = New System.Windows.Forms.Button()
        Me.btnOK = New System.Windows.Forms.Button()
        Me.btnHOLD = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.lblAge = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.lblTime = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.lblShift = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.lblDate = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.lblColorCode = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.lblTreads = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.lblLocation = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.lblTruckNo = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.lblDescription = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lblTreadCode = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblCode = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'lblShift0
        '
        Me.lblShift0.AutoSize = True
        Me.lblShift0.Location = New System.Drawing.Point(180, 13)
        Me.lblShift0.Name = "lblShift0"
        Me.lblShift0.Size = New System.Drawing.Size(39, 13)
        Me.lblShift0.TabIndex = 138
        Me.lblShift0.Text = "Label5"
        Me.lblShift0.Visible = False
        '
        'lblTime0
        '
        Me.lblTime0.AutoSize = True
        Me.lblTime0.Location = New System.Drawing.Point(118, 13)
        Me.lblTime0.Name = "lblTime0"
        Me.lblTime0.Size = New System.Drawing.Size(39, 13)
        Me.lblTime0.TabIndex = 137
        Me.lblTime0.Text = "Label3"
        Me.lblTime0.Visible = False
        '
        'lblDate0
        '
        Me.lblDate0.AutoSize = True
        Me.lblDate0.Location = New System.Drawing.Point(62, 13)
        Me.lblDate0.Name = "lblDate0"
        Me.lblDate0.Size = New System.Drawing.Size(39, 13)
        Me.lblDate0.TabIndex = 136
        Me.lblDate0.Text = "Label2"
        Me.lblDate0.Visible = False
        '
        'btnID
        '
        Me.btnID.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnID.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnID.Location = New System.Drawing.Point(1067, 129)
        Me.btnID.Name = "btnID"
        Me.btnID.Size = New System.Drawing.Size(157, 32)
        Me.btnID.TabIndex = 135
        Me.btnID.UseVisualStyleBackColor = False
        '
        'btnOK
        '
        Me.btnOK.BackColor = System.Drawing.Color.Purple
        Me.btnOK.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btnOK.Location = New System.Drawing.Point(1067, 237)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(157, 32)
        Me.btnOK.TabIndex = 134
        Me.btnOK.Text = "TAKE TO TB"
        Me.btnOK.UseVisualStyleBackColor = False
        '
        'btnHOLD
        '
        Me.btnHOLD.BackColor = System.Drawing.Color.Purple
        Me.btnHOLD.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHOLD.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btnHOLD.Location = New System.Drawing.Point(1067, 382)
        Me.btnHOLD.Name = "btnHOLD"
        Me.btnHOLD.Size = New System.Drawing.Size(157, 32)
        Me.btnHOLD.TabIndex = 133
        Me.btnHOLD.Text = "HOLD"
        Me.btnHOLD.UseVisualStyleBackColor = False
        Me.btnHOLD.Visible = False
        '
        'btnCancel
        '
        Me.btnCancel.BackColor = System.Drawing.Color.Purple
        Me.btnCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btnCancel.Location = New System.Drawing.Point(1067, 293)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(157, 32)
        Me.btnCancel.TabIndex = 132
        Me.btnCancel.Text = "CANCEL"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'lblAge
        '
        Me.lblAge.AutoSize = True
        Me.lblAge.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAge.Location = New System.Drawing.Point(757, 338)
        Me.lblAge.Name = "lblAge"
        Me.lblAge.Size = New System.Drawing.Size(202, 32)
        Me.lblAge.TabIndex = 131
        Me.lblAge.Text = "LEAF TRUCK NO"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.BackColor = System.Drawing.Color.Pink
        Me.Label12.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(586, 340)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(60, 32)
        Me.Label12.TabIndex = 130
        Me.Label12.Text = "Age"
        '
        'lblTime
        '
        Me.lblTime.AutoSize = True
        Me.lblTime.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTime.Location = New System.Drawing.Point(757, 289)
        Me.lblTime.Name = "lblTime"
        Me.lblTime.Size = New System.Drawing.Size(202, 32)
        Me.lblTime.TabIndex = 129
        Me.lblTime.Text = "LEAF TRUCK NO"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.BackColor = System.Drawing.Color.Pink
        Me.Label14.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(586, 291)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(71, 32)
        Me.Label14.TabIndex = 128
        Me.Label14.Text = "Time"
        '
        'lblShift
        '
        Me.lblShift.AutoSize = True
        Me.lblShift.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblShift.Location = New System.Drawing.Point(757, 235)
        Me.lblShift.Name = "lblShift"
        Me.lblShift.Size = New System.Drawing.Size(202, 32)
        Me.lblShift.TabIndex = 127
        Me.lblShift.Text = "LEAF TRUCK NO"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.BackColor = System.Drawing.Color.Pink
        Me.Label16.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(586, 237)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(69, 32)
        Me.Label16.TabIndex = 126
        Me.Label16.Text = "Shift"
        '
        'lblDate
        '
        Me.lblDate.AutoSize = True
        Me.lblDate.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDate.Location = New System.Drawing.Point(757, 180)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(202, 32)
        Me.lblDate.TabIndex = 125
        Me.lblDate.Text = "LEAF TRUCK NO"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.BackColor = System.Drawing.Color.Pink
        Me.Label18.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(586, 180)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(68, 32)
        Me.Label18.TabIndex = 124
        Me.Label18.Text = "Date"
        '
        'lblColorCode
        '
        Me.lblColorCode.AutoSize = True
        Me.lblColorCode.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblColorCode.Location = New System.Drawing.Point(757, 129)
        Me.lblColorCode.Name = "lblColorCode"
        Me.lblColorCode.Size = New System.Drawing.Size(202, 32)
        Me.lblColorCode.TabIndex = 123
        Me.lblColorCode.Text = "LEAF TRUCK NO"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.BackColor = System.Drawing.Color.Pink
        Me.Label20.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(586, 129)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(142, 32)
        Me.Label20.TabIndex = 122
        Me.Label20.Text = "Color Code"
        '
        'lblTreads
        '
        Me.lblTreads.AutoSize = True
        Me.lblTreads.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTreads.Location = New System.Drawing.Point(177, 336)
        Me.lblTreads.Name = "lblTreads"
        Me.lblTreads.Size = New System.Drawing.Size(202, 32)
        Me.lblTreads.TabIndex = 121
        Me.lblTreads.Text = "LEAF TRUCK NO"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.Pink
        Me.Label10.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(12, 338)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(90, 32)
        Me.Label10.TabIndex = 120
        Me.Label10.Text = "Treads"
        '
        'lblLocation
        '
        Me.lblLocation.AutoSize = True
        Me.lblLocation.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLocation.Location = New System.Drawing.Point(177, 287)
        Me.lblLocation.Name = "lblLocation"
        Me.lblLocation.Size = New System.Drawing.Size(202, 32)
        Me.lblLocation.TabIndex = 119
        Me.lblLocation.Text = "LEAF TRUCK NO"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.Pink
        Me.Label8.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(12, 289)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(113, 32)
        Me.Label8.TabIndex = 118
        Me.Label8.Text = "Location"
        '
        'lblTruckNo
        '
        Me.lblTruckNo.AutoSize = True
        Me.lblTruckNo.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTruckNo.Location = New System.Drawing.Point(177, 233)
        Me.lblTruckNo.Name = "lblTruckNo"
        Me.lblTruckNo.Size = New System.Drawing.Size(202, 32)
        Me.lblTruckNo.TabIndex = 117
        Me.lblTruckNo.Text = "LEAF TRUCK NO"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Pink
        Me.Label6.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(12, 235)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(120, 32)
        Me.Label6.TabIndex = 116
        Me.Label6.Text = "Truck No"
        '
        'lblDescription
        '
        Me.lblDescription.AutoSize = True
        Me.lblDescription.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDescription.Location = New System.Drawing.Point(177, 178)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(202, 32)
        Me.lblDescription.TabIndex = 115
        Me.lblDescription.Text = "LEAF TRUCK NO"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Pink
        Me.Label4.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(12, 178)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(146, 32)
        Me.Label4.TabIndex = 114
        Me.Label4.Text = "Description"
        '
        'lblTreadCode
        '
        Me.lblTreadCode.AutoSize = True
        Me.lblTreadCode.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTreadCode.Location = New System.Drawing.Point(177, 127)
        Me.lblTreadCode.Name = "lblTreadCode"
        Me.lblTreadCode.Size = New System.Drawing.Size(202, 32)
        Me.lblTreadCode.TabIndex = 113
        Me.lblTreadCode.Text = "LEAF TRUCK NO"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Pink
        Me.Label1.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 127)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(145, 32)
        Me.Label1.TabIndex = 112
        Me.Label1.Text = "Tread Code"
        '
        'lblCode
        '
        Me.lblCode.AutoSize = True
        Me.lblCode.BackColor = System.Drawing.Color.LightPink
        Me.lblCode.Font = New System.Drawing.Font("Malgun Gothic", 35.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCode.ForeColor = System.Drawing.Color.Maroon
        Me.lblCode.Location = New System.Drawing.Point(379, 9)
        Me.lblCode.Name = "lblCode"
        Me.lblCode.Size = New System.Drawing.Size(337, 62)
        Me.lblCode.TabIndex = 111
        Me.lblCode.Text = "FIFO DISPLAY"
        '
        'TBDisplay
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.DarkSeaGreen
        Me.ClientSize = New System.Drawing.Size(1241, 432)
        Me.Controls.Add(Me.lblShift0)
        Me.Controls.Add(Me.lblTime0)
        Me.Controls.Add(Me.lblDate0)
        Me.Controls.Add(Me.btnID)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.btnHOLD)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.lblAge)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.lblTime)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.lblShift)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.lblDate)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.lblColorCode)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.lblTreads)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.lblLocation)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.lblTruckNo)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.lblDescription)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.lblTreadCode)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblCode)
        Me.Name = "TBDisplay"
        Me.Text = "TBDisplay"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblShift0 As System.Windows.Forms.Label
    Friend WithEvents lblTime0 As System.Windows.Forms.Label
    Friend WithEvents lblDate0 As System.Windows.Forms.Label
    Friend WithEvents btnID As System.Windows.Forms.Button
    Friend WithEvents btnOK As System.Windows.Forms.Button
    Friend WithEvents btnHOLD As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents lblAge As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents lblTime As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents lblShift As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents lblColorCode As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents lblTreads As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents lblLocation As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents lblTruckNo As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lblTreadCode As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblCode As System.Windows.Forms.Label
End Class
