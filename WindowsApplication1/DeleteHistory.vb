﻿Imports Excel = Microsoft.Office.Interop.Excel
Imports Microsoft.Office
Imports System.Text
Imports System.IO
Imports System.Windows.Forms
Imports System.Data.SqlClient
Imports System.Data
Imports System.Drawing
Public Class DeleteHistory
    Dim DTSpec As DataTable
    Dim objcon As New Connections
    Dim DualExactDate As Date
    Dim DualExactShift As String
    Public ds As New DataSet()
    Dim DtStock As DataTable
    Dim rwcnt As Integer = 0
    Public dt As New DataTable()

    Dim excelLocation As String
    Dim APP As New Excel.Application
    Dim worksheet As Excel.Worksheet
    Dim workbook As Excel.Workbook
   
    Private Sub BtnSubmit_Click(sender As Object, e As EventArgs) Handles BtnSubmit.Click
        If TDate.Text <> "" And cboMachine.Text <> "" Then



            GlobalVariables.FromDate = TDate.Text
            GlobalVariables.Shift = cboMachine.Text

            dt.Rows.Clear()
            dt.Columns.Clear()
            rwcnt = 0
            DataGridView1.Visible = True


            dt.Columns.Add("pkdextid", GetType(String))
            dt.Columns.Add("prod_date", GetType(String))
            dt.Columns.Add("prod_time", GetType(String))
            dt.Columns.Add("prod_shift", GetType(String))
            dt.Columns.Add("treadcode", GetType(String))
            dt.Columns.Add("colorcode", GetType(String))
            dt.Columns.Add("description", GetType(String))
            dt.Columns.Add("truckno", GetType(String))
            dt.Columns.Add("location", GetType(String))
            dt.Columns.Add("treads", GetType(String))
            dt.Columns.Add("del_date", GetType(String))
            dt.Columns.Add("del_time", GetType(String))
            dt.Columns.Add("del_shift", GetType(String))
            dt.Columns.Add("Dual_Ext", GetType(String))
            dt.Columns.Add("Deleted_By", GetType(String))


            ds = objcon.DeleteHistory()

            If ds.Tables(0).Rows.Count > 0 Then

                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1

                    Dim dr As DataRow = dt.NewRow()

                    dt.Rows.Add(dr)
                    dt.Rows(rwcnt)(0) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(1))
                    dt.Rows(rwcnt)(1) = Convert.ToDateTime(ds.Tables(0).Rows(i).ItemArray(2)).ToString("dd/MM/yyyy")
                    dt.Rows(rwcnt)(2) = Convert.ToDateTime(ds.Tables(0).Rows(i).ItemArray(3)).ToString("HH:mm:ss")
                    dt.Rows(rwcnt)(3) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(4))
                    dt.Rows(rwcnt)(4) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(5))
                    dt.Rows(rwcnt)(5) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(6))
                    dt.Rows(rwcnt)(6) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(7))
                    dt.Rows(rwcnt)(7) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(8))
                    dt.Rows(rwcnt)(8) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(9))
                    dt.Rows(rwcnt)(9) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(10))
                    dt.Rows(rwcnt)(10) = Convert.ToDateTime(ds.Tables(0).Rows(i).ItemArray(11)).ToString("dd/MM/yyyy")
                    dt.Rows(rwcnt)(11) = Convert.ToDateTime(ds.Tables(0).Rows(i).ItemArray(12)).ToString("HH:mm:ss")
                    dt.Rows(rwcnt)(12) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(13))
                    dt.Rows(rwcnt)(13) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(14))
                    dt.Rows(rwcnt)(14) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(15))


                    rwcnt += 1
                Next

                DataGridView1.DataSource = dt


            Else
                MessageBox.Show("No data Found !!!")
            End If

        Else
            MessageBox.Show("Enter All Details !!!")
        End If

    End Sub

    Private Sub btnExport_Click(sender As Object, e As EventArgs) Handles btnExport.Click
        If DataGridView1.Rows.Count <= 2 Then
            MsgBox("Nothing to Export")
            Exit Sub

        End If
        Dim saveDlg As New System.Windows.Forms.SaveFileDialog()

        saveDlg.InitialDirectory = "C:\"
        saveDlg.Filter = "Excel files (*.xlsx)|*.xlsx"
        saveDlg.FilterIndex = 0
        saveDlg.RestoreDirectory = True
        saveDlg.Title = "Export Excel File To"

        If saveDlg.ShowDialog() = System.Windows.Forms.DialogResult.OK Then

            workbook = APP.Workbooks.Open("c:\Temp\File.xlsx")
            worksheet = workbook.Worksheets("Sheet1")

            worksheet.Range(worksheet.Cells(1, 1), worksheet.Cells(1, 10)).Merge()
            worksheet.Range(worksheet.Cells(1, 1), worksheet.Cells(1, 10)).VerticalAlignment = Excel.Constants.xlCenter

            worksheet.Cells(1, 1).Value = "DELETE HISTORY Report on " & TDate.Text & "  Shift  " & cboMachine.Text
            Dim columnsCount As Integer = DataGridView1.Columns.Count
            For Each column In DataGridView1.Columns
                worksheet.Cells(2, column.Index + 1).Value = column.Name
                worksheet.Columns.AutoFit()
            Next
            'Export Header Name End


            'Export Each Row Start
            For i As Integer = 0 To DataGridView1.Rows.Count - 2
                Dim columnIndex As Integer = 0
                Do Until columnIndex = columnsCount
                    worksheet.Cells(i + 3, columnIndex + 1).Value = DataGridView1.Item(columnIndex, i).Value.ToString
                    columnIndex += 1
                Loop
            Next
            'Export Each Row End




            excelLocation = saveDlg.FileName
            workbook.SaveCopyAs(excelLocation)
            workbook.Saved = True
            workbook.Close(True, 0, 0)
            APP.Quit()

            MessageBox.Show("Exporting is completed..")
        End If
    End Sub
End Class