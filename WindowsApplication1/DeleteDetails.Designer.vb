﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DeleteDetails
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.lblCode = New System.Windows.Forms.Label()
        Me.cboCalShift = New System.Windows.Forms.ComboBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.dtpCalDate = New System.Windows.Forms.DateTimePicker()
        Me.DGSchSelection = New System.Windows.Forms.DataGridView()
        Me.ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Date1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Shift = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Code = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColorCode = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LeafTruckNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Location = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Quantity = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TruckNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Delete = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.btnBack = New System.Windows.Forms.Button()
        Me.lblSect = New System.Windows.Forms.Label()
        Me.btnSearch = New System.Windows.Forms.Button()
        Me.DateLabel = New System.Windows.Forms.Label()
        Me.ShiftLabel = New System.Windows.Forms.Label()
        CType(Me.DGSchSelection, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblCode
        '
        Me.lblCode.AutoSize = True
        Me.lblCode.Font = New System.Drawing.Font("Malgun Gothic", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCode.Location = New System.Drawing.Point(300, 31)
        Me.lblCode.Name = "lblCode"
        Me.lblCode.Size = New System.Drawing.Size(257, 37)
        Me.lblCode.TabIndex = 79
        Me.lblCode.Text = "DELETE DETAILS - "
        '
        'cboCalShift
        '
        Me.cboCalShift.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCalShift.FormattingEnabled = True
        Me.cboCalShift.Items.AddRange(New Object() {"A", "B", "C"})
        Me.cboCalShift.Location = New System.Drawing.Point(12, 82)
        Me.cboCalShift.Name = "cboCalShift"
        Me.cboCalShift.Size = New System.Drawing.Size(78, 33)
        Me.cboCalShift.TabIndex = 81
        Me.cboCalShift.Visible = False
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(483, 114)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(81, 32)
        Me.Label18.TabIndex = 83
        Me.Label18.Text = "SHIFT"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(195, 114)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(77, 32)
        Me.Label17.TabIndex = 82
        Me.Label17.Text = "DATE"
        '
        'dtpCalDate
        '
        Me.dtpCalDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpCalDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpCalDate.Location = New System.Drawing.Point(12, 45)
        Me.dtpCalDate.Name = "dtpCalDate"
        Me.dtpCalDate.Size = New System.Drawing.Size(146, 31)
        Me.dtpCalDate.TabIndex = 84
        Me.dtpCalDate.Visible = False
        '
        'DGSchSelection
        '
        Me.DGSchSelection.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Peru
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGSchSelection.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DGSchSelection.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGSchSelection.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ID, Me.Date1, Me.Shift, Me.Code, Me.ColorCode, Me.LeafTruckNo, Me.Location, Me.Quantity, Me.TruckNo, Me.Delete})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DGSchSelection.DefaultCellStyle = DataGridViewCellStyle2
        Me.DGSchSelection.Location = New System.Drawing.Point(4, 165)
        Me.DGSchSelection.Name = "DGSchSelection"
        Me.DGSchSelection.RowTemplate.Height = 30
        Me.DGSchSelection.Size = New System.Drawing.Size(1069, 459)
        Me.DGSchSelection.TabIndex = 85
        '
        'ID
        '
        Me.ID.HeaderText = "ID"
        Me.ID.Name = "ID"
        Me.ID.Visible = False
        '
        'Date1
        '
        Me.Date1.HeaderText = "Date"
        Me.Date1.Name = "Date1"
        '
        'Shift
        '
        Me.Shift.HeaderText = "Shift"
        Me.Shift.Name = "Shift"
        '
        'Code
        '
        Me.Code.HeaderText = "Code"
        Me.Code.Name = "Code"
        '
        'ColorCode
        '
        Me.ColorCode.HeaderText = "Color Code"
        Me.ColorCode.Name = "ColorCode"
        Me.ColorCode.Width = 150
        '
        'LeafTruckNo
        '
        Me.LeafTruckNo.HeaderText = "LeafTruckNo"
        Me.LeafTruckNo.Name = "LeafTruckNo"
        Me.LeafTruckNo.Width = 120
        '
        'Location
        '
        Me.Location.HeaderText = "Location"
        Me.Location.Name = "Location"
        '
        'Quantity
        '
        Me.Quantity.HeaderText = "Quantity"
        Me.Quantity.Name = "Quantity"
        '
        'TruckNo
        '
        Me.TruckNo.HeaderText = "Truck No"
        Me.TruckNo.Name = "TruckNo"
        '
        'Delete
        '
        Me.Delete.HeaderText = "Delete"
        Me.Delete.Name = "Delete"
        Me.Delete.Text = "DELETE"
        Me.Delete.UseColumnTextForButtonValue = True
        '
        'btnBack
        '
        Me.btnBack.BackColor = System.Drawing.Color.SpringGreen
        Me.btnBack.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBack.Location = New System.Drawing.Point(870, 32)
        Me.btnBack.Name = "btnBack"
        Me.btnBack.Size = New System.Drawing.Size(145, 44)
        Me.btnBack.TabIndex = 87
        Me.btnBack.Text = "Back"
        Me.btnBack.UseVisualStyleBackColor = False
        '
        'lblSect
        '
        Me.lblSect.AutoSize = True
        Me.lblSect.Font = New System.Drawing.Font("Malgun Gothic", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSect.Location = New System.Drawing.Point(552, 31)
        Me.lblSect.Name = "lblSect"
        Me.lblSect.Size = New System.Drawing.Size(39, 37)
        Me.lblSect.TabIndex = 88
        Me.lblSect.Text = "--"
        '
        'btnSearch
        '
        Me.btnSearch.BackColor = System.Drawing.Color.Orchid
        Me.btnSearch.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSearch.Location = New System.Drawing.Point(709, 106)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(116, 42)
        Me.btnSearch.TabIndex = 89
        Me.btnSearch.Text = "Search"
        Me.btnSearch.UseVisualStyleBackColor = False
        Me.btnSearch.Visible = False
        '
        'DateLabel
        '
        Me.DateLabel.AutoSize = True
        Me.DateLabel.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateLabel.Location = New System.Drawing.Point(313, 114)
        Me.DateLabel.Name = "DateLabel"
        Me.DateLabel.Size = New System.Drawing.Size(77, 32)
        Me.DateLabel.TabIndex = 90
        Me.DateLabel.Text = "DATE"
        '
        'ShiftLabel
        '
        Me.ShiftLabel.AutoSize = True
        Me.ShiftLabel.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ShiftLabel.Location = New System.Drawing.Point(570, 114)
        Me.ShiftLabel.Name = "ShiftLabel"
        Me.ShiftLabel.Size = New System.Drawing.Size(81, 32)
        Me.ShiftLabel.TabIndex = 91
        Me.ShiftLabel.Text = "SHIFT"
        '
        'DeleteDetails
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightPink
        Me.ClientSize = New System.Drawing.Size(1085, 684)
        Me.Controls.Add(Me.ShiftLabel)
        Me.Controls.Add(Me.DateLabel)
        Me.Controls.Add(Me.btnSearch)
        Me.Controls.Add(Me.lblSect)
        Me.Controls.Add(Me.btnBack)
        Me.Controls.Add(Me.DGSchSelection)
        Me.Controls.Add(Me.dtpCalDate)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.cboCalShift)
        Me.Controls.Add(Me.lblCode)
        Me.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Name = "DeleteDetails"
        Me.Text = "Delete Details"
        CType(Me.DGSchSelection, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblCode As System.Windows.Forms.Label
    Friend WithEvents cboCalShift As System.Windows.Forms.ComboBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents dtpCalDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents DGSchSelection As System.Windows.Forms.DataGridView
    Friend WithEvents btnBack As System.Windows.Forms.Button
    Friend WithEvents lblSect As System.Windows.Forms.Label
    Friend WithEvents btnSearch As System.Windows.Forms.Button
    Friend WithEvents ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Date1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Shift As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Code As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColorCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LeafTruckNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Location As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Quantity As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TruckNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Delete As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents DateLabel As System.Windows.Forms.Label
    Friend WithEvents ShiftLabel As System.Windows.Forms.Label
End Class
