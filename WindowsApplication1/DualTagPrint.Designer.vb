﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class lblTime
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(lblTime))
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.cbTruckNo = New System.Windows.Forms.ComboBox()
        Me.txtBooker = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.BtnHold = New System.Windows.Forms.Button()
        Me.CboReason = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.CboLocation = New System.Windows.Forms.ComboBox()
        Me.txtNo = New System.Windows.Forms.TextBox()
        Me.Lblno = New System.Windows.Forms.Label()
        Me.txtQty = New System.Windows.Forms.TextBox()
        Me.lblCode = New System.Windows.Forms.Label()
        Me.cboCode = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.txtOperator = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtColourCode = New System.Windows.Forms.TextBox()
        Me.txtItemCode = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.lblShift = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.lblDate123 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.btnBack = New System.Windows.Forms.Button()
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.lblTime0 = New System.Windows.Forms.Label()
        Me.lblDate0 = New System.Windows.Forms.Label()
        Me.lblTime24 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.lblDate1 = New System.Windows.Forms.Label()
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument()
        Me.PrintPreviewDialog1 = New System.Windows.Forms.PrintPreviewDialog()
        Me.Lblcomp = New System.Windows.Forms.Label()
        Me.PrintDocument2 = New System.Drawing.Printing.PrintDocument()
        Me.LblDesc = New System.Windows.Forms.Label()
        Me.lblfinal = New System.Windows.Forms.Label()
        Me.btnDel = New System.Windows.Forms.Button()
        Me.lblhr = New System.Windows.Forms.Label()
        Me.totdattime = New System.Windows.Forms.Label()
        Me.PrintDocument3 = New System.Drawing.Printing.PrintDocument()
        Me.Panel3.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.LightSalmon
        Me.Panel3.Controls.Add(Me.cbTruckNo)
        Me.Panel3.Controls.Add(Me.txtBooker)
        Me.Panel3.Controls.Add(Me.Label4)
        Me.Panel3.Controls.Add(Me.BtnHold)
        Me.Panel3.Controls.Add(Me.CboReason)
        Me.Panel3.Controls.Add(Me.Label3)
        Me.Panel3.Controls.Add(Me.CboLocation)
        Me.Panel3.Controls.Add(Me.txtNo)
        Me.Panel3.Controls.Add(Me.Lblno)
        Me.Panel3.Controls.Add(Me.txtQty)
        Me.Panel3.Controls.Add(Me.lblCode)
        Me.Panel3.Controls.Add(Me.cboCode)
        Me.Panel3.Controls.Add(Me.Label8)
        Me.Panel3.Controls.Add(Me.Label9)
        Me.Panel3.Location = New System.Drawing.Point(22, 159)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(1049, 231)
        Me.Panel3.TabIndex = 70
        '
        'cbTruckNo
        '
        Me.cbTruckNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbTruckNo.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbTruckNo.FormattingEnabled = True
        Me.cbTruckNo.Location = New System.Drawing.Point(212, 79)
        Me.cbTruckNo.Name = "cbTruckNo"
        Me.cbTruckNo.Size = New System.Drawing.Size(121, 26)
        Me.cbTruckNo.TabIndex = 135
        '
        'txtBooker
        '
        Me.txtBooker.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBooker.Location = New System.Drawing.Point(571, 83)
        Me.txtBooker.Name = "txtBooker"
        Me.txtBooker.Size = New System.Drawing.Size(138, 29)
        Me.txtBooker.TabIndex = 134
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(389, 79)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(158, 32)
        Me.Label4.TabIndex = 133
        Me.Label4.Text = "BOOKER NO"
        '
        'BtnHold
        '
        Me.BtnHold.BackColor = System.Drawing.Color.Tomato
        Me.BtnHold.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnHold.Location = New System.Drawing.Point(728, 144)
        Me.BtnHold.Name = "BtnHold"
        Me.BtnHold.Size = New System.Drawing.Size(97, 42)
        Me.BtnHold.TabIndex = 132
        Me.BtnHold.Text = "HOLD"
        Me.BtnHold.UseVisualStyleBackColor = False
        '
        'CboReason
        '
        Me.CboReason.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CboReason.FormattingEnabled = True
        Me.CboReason.Location = New System.Drawing.Point(407, 153)
        Me.CboReason.Name = "CboReason"
        Me.CboReason.Size = New System.Drawing.Size(287, 33)
        Me.CboReason.TabIndex = 114
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(275, 157)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(113, 32)
        Me.Label3.TabIndex = 113
        Me.Label3.Text = "REASON"
        '
        'CboLocation
        '
        Me.CboLocation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CboLocation.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CboLocation.FormattingEnabled = True
        Me.CboLocation.Location = New System.Drawing.Point(571, 12)
        Me.CboLocation.Name = "CboLocation"
        Me.CboLocation.Size = New System.Drawing.Size(112, 33)
        Me.CboLocation.TabIndex = 93
        '
        'txtNo
        '
        Me.txtNo.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNo.Location = New System.Drawing.Point(53, 161)
        Me.txtNo.Name = "txtNo"
        Me.txtNo.Size = New System.Drawing.Size(87, 29)
        Me.txtNo.TabIndex = 92
        Me.txtNo.Visible = False
        '
        'Lblno
        '
        Me.Lblno.AutoSize = True
        Me.Lblno.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lblno.Location = New System.Drawing.Point(66, 79)
        Me.Lblno.Name = "Lblno"
        Me.Lblno.Size = New System.Drawing.Size(139, 32)
        Me.Lblno.TabIndex = 91
        Me.Lblno.Text = "TRUCK NO"
        '
        'txtQty
        '
        Me.txtQty.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtQty.Location = New System.Drawing.Point(926, 13)
        Me.txtQty.Name = "txtQty"
        Me.txtQty.Size = New System.Drawing.Size(89, 29)
        Me.txtQty.TabIndex = 90
        '
        'lblCode
        '
        Me.lblCode.AutoSize = True
        Me.lblCode.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCode.Location = New System.Drawing.Point(3, 13)
        Me.lblCode.Name = "lblCode"
        Me.lblCode.Size = New System.Drawing.Size(202, 32)
        Me.lblCode.TabIndex = 78
        Me.lblCode.Text = "LEAF TRUCK NO"
        '
        'cboCode
        '
        Me.cboCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCode.FormattingEnabled = True
        Me.cboCode.Location = New System.Drawing.Point(211, 11)
        Me.cboCode.Name = "cboCode"
        Me.cboCode.Size = New System.Drawing.Size(112, 33)
        Me.cboCode.TabIndex = 76
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(746, 13)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(138, 32)
        Me.Label8.TabIndex = 67
        Me.Label8.Text = "QUANTITY"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(418, 13)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(137, 32)
        Me.Label9.TabIndex = 66
        Me.Label9.Text = "LOCATION"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.DarkMagenta
        Me.Panel1.Controls.Add(Me.txtOperator)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.txtColourCode)
        Me.Panel1.Controls.Add(Me.txtItemCode)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.Label19)
        Me.Panel1.Controls.Add(Me.lblShift)
        Me.Panel1.Controls.Add(Me.Label14)
        Me.Panel1.Controls.Add(Me.lblDate123)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Location = New System.Drawing.Point(22, 19)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1049, 134)
        Me.Panel1.TabIndex = 83
        '
        'txtOperator
        '
        Me.txtOperator.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOperator.Location = New System.Drawing.Point(736, 82)
        Me.txtOperator.Name = "txtOperator"
        Me.txtOperator.Size = New System.Drawing.Size(226, 29)
        Me.txtOperator.TabIndex = 89
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(586, 78)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(144, 32)
        Me.Label2.TabIndex = 88
        Me.Label2.Text = "OPERATOR"
        '
        'txtColourCode
        '
        Me.txtColourCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtColourCode.Location = New System.Drawing.Point(211, 79)
        Me.txtColourCode.Name = "txtColourCode"
        Me.txtColourCode.Size = New System.Drawing.Size(226, 29)
        Me.txtColourCode.TabIndex = 87
        '
        'txtItemCode
        '
        Me.txtItemCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtItemCode.Location = New System.Drawing.Point(211, 26)
        Me.txtItemCode.Name = "txtItemCode"
        Me.txtItemCode.Size = New System.Drawing.Size(226, 29)
        Me.txtItemCode.TabIndex = 86
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(17, 79)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(169, 32)
        Me.Label1.TabIndex = 85
        Me.Label1.Text = "COLOR CODE"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Malgun Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.White
        Me.Label19.Location = New System.Drawing.Point(17, 22)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(145, 32)
        Me.Label19.TabIndex = 82
        Me.Label19.Text = "ITEM CODE"
        '
        'lblShift
        '
        Me.lblShift.AutoSize = True
        Me.lblShift.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblShift.ForeColor = System.Drawing.Color.White
        Me.lblShift.Location = New System.Drawing.Point(888, 22)
        Me.lblShift.Name = "lblShift"
        Me.lblShift.Size = New System.Drawing.Size(74, 26)
        Me.lblShift.TabIndex = 63
        Me.lblShift.Text = "DATE"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.White
        Me.Label14.Location = New System.Drawing.Point(789, 22)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(79, 26)
        Me.Label14.TabIndex = 62
        Me.Label14.Text = "SHIFT"
        '
        'lblDate123
        '
        Me.lblDate123.AutoSize = True
        Me.lblDate123.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDate123.ForeColor = System.Drawing.Color.White
        Me.lblDate123.Location = New System.Drawing.Point(593, 28)
        Me.lblDate123.Name = "lblDate123"
        Me.lblDate123.Size = New System.Drawing.Size(74, 26)
        Me.lblDate123.TabIndex = 61
        Me.lblDate123.Text = "DATE"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Location = New System.Drawing.Point(496, 28)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(74, 26)
        Me.Label7.TabIndex = 60
        Me.Label7.Text = "DATE"
        '
        'btnBack
        '
        Me.btnBack.BackColor = System.Drawing.Color.SpringGreen
        Me.btnBack.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBack.Location = New System.Drawing.Point(75, 437)
        Me.btnBack.Name = "btnBack"
        Me.btnBack.Size = New System.Drawing.Size(310, 83)
        Me.btnBack.TabIndex = 85
        Me.btnBack.Text = "Back"
        Me.btnBack.UseVisualStyleBackColor = False
        '
        'btnPrint
        '
        Me.btnPrint.BackColor = System.Drawing.Color.Yellow
        Me.btnPrint.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrint.Location = New System.Drawing.Point(417, 437)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(296, 83)
        Me.btnPrint.TabIndex = 84
        Me.btnPrint.Text = "Print"
        Me.btnPrint.UseVisualStyleBackColor = False
        '
        'lblTime0
        '
        Me.lblTime0.AutoSize = True
        Me.lblTime0.Location = New System.Drawing.Point(62, 407)
        Me.lblTime0.Name = "lblTime0"
        Me.lblTime0.Size = New System.Drawing.Size(39, 13)
        Me.lblTime0.TabIndex = 88
        Me.lblTime0.Text = "Label4"
        Me.lblTime0.Visible = False
        '
        'lblDate0
        '
        Me.lblDate0.AutoSize = True
        Me.lblDate0.Location = New System.Drawing.Point(12, 407)
        Me.lblDate0.Name = "lblDate0"
        Me.lblDate0.Size = New System.Drawing.Size(39, 13)
        Me.lblDate0.TabIndex = 89
        Me.lblDate0.Text = "Label3"
        Me.lblDate0.Visible = False
        '
        'lblTime24
        '
        Me.lblTime24.AutoSize = True
        Me.lblTime24.Location = New System.Drawing.Point(116, 407)
        Me.lblTime24.Name = "lblTime24"
        Me.lblTime24.Size = New System.Drawing.Size(39, 13)
        Me.lblTime24.TabIndex = 90
        Me.lblTime24.Text = "Label5"
        Me.lblTime24.Visible = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(170, 407)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(39, 13)
        Me.Label6.TabIndex = 91
        Me.Label6.Text = "Label6"
        Me.Label6.Visible = False
        '
        'lblDate1
        '
        Me.lblDate1.AutoSize = True
        Me.lblDate1.Location = New System.Drawing.Point(291, 407)
        Me.lblDate1.Name = "lblDate1"
        Me.lblDate1.Size = New System.Drawing.Size(39, 13)
        Me.lblDate1.TabIndex = 92
        Me.lblDate1.Text = "Label3"
        Me.lblDate1.Visible = False
        '
        'PrintDocument1
        '
        '
        'PrintPreviewDialog1
        '
        Me.PrintPreviewDialog1.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.PrintPreviewDialog1.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.PrintPreviewDialog1.ClientSize = New System.Drawing.Size(400, 300)
        Me.PrintPreviewDialog1.Enabled = True
        Me.PrintPreviewDialog1.Icon = CType(resources.GetObject("PrintPreviewDialog1.Icon"), System.Drawing.Icon)
        Me.PrintPreviewDialog1.Name = "PrintPreviewDialog1"
        Me.PrintPreviewDialog1.Visible = False
        '
        'Lblcomp
        '
        Me.Lblcomp.AutoSize = True
        Me.Lblcomp.Location = New System.Drawing.Point(459, 407)
        Me.Lblcomp.Name = "Lblcomp"
        Me.Lblcomp.Size = New System.Drawing.Size(39, 13)
        Me.Lblcomp.TabIndex = 93
        Me.Lblcomp.Text = "Label3"
        Me.Lblcomp.Visible = False
        '
        'PrintDocument2
        '
        '
        'LblDesc
        '
        Me.LblDesc.AutoSize = True
        Me.LblDesc.Location = New System.Drawing.Point(346, 407)
        Me.LblDesc.Name = "LblDesc"
        Me.LblDesc.Size = New System.Drawing.Size(32, 13)
        Me.LblDesc.TabIndex = 94
        Me.LblDesc.Text = "Desc"
        Me.LblDesc.Visible = False
        '
        'lblfinal
        '
        Me.lblfinal.AutoSize = True
        Me.lblfinal.Location = New System.Drawing.Point(521, 407)
        Me.lblfinal.Name = "lblfinal"
        Me.lblfinal.Size = New System.Drawing.Size(21, 13)
        Me.lblfinal.TabIndex = 95
        Me.lblfinal.Text = "Lbl"
        Me.lblfinal.Visible = False
        '
        'btnDel
        '
        Me.btnDel.BackColor = System.Drawing.Color.CornflowerBlue
        Me.btnDel.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDel.Location = New System.Drawing.Point(750, 437)
        Me.btnDel.Name = "btnDel"
        Me.btnDel.Size = New System.Drawing.Size(296, 83)
        Me.btnDel.TabIndex = 96
        Me.btnDel.Text = "Delete "
        Me.btnDel.UseVisualStyleBackColor = False
        '
        'lblhr
        '
        Me.lblhr.AutoSize = True
        Me.lblhr.Location = New System.Drawing.Point(231, 407)
        Me.lblhr.Name = "lblhr"
        Me.lblhr.Size = New System.Drawing.Size(39, 13)
        Me.lblhr.TabIndex = 97
        Me.lblhr.Text = "Label3"
        Me.lblhr.Visible = False
        '
        'totdattime
        '
        Me.totdattime.AutoSize = True
        Me.totdattime.Location = New System.Drawing.Point(399, 407)
        Me.totdattime.Name = "totdattime"
        Me.totdattime.Size = New System.Drawing.Size(39, 13)
        Me.totdattime.TabIndex = 98
        Me.totdattime.Text = "Label3"
        Me.totdattime.Visible = False
        '
        'PrintDocument3
        '
        '
        'lblTime
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1169, 560)
        Me.Controls.Add(Me.totdattime)
        Me.Controls.Add(Me.lblhr)
        Me.Controls.Add(Me.btnDel)
        Me.Controls.Add(Me.lblfinal)
        Me.Controls.Add(Me.LblDesc)
        Me.Controls.Add(Me.Lblcomp)
        Me.Controls.Add(Me.lblDate1)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.lblTime24)
        Me.Controls.Add(Me.lblDate0)
        Me.Controls.Add(Me.lblTime0)
        Me.Controls.Add(Me.btnBack)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Panel3)
        Me.Name = "lblTime"
        Me.Text = "DualTagPrint"
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents lblCode As System.Windows.Forms.Label
    Friend WithEvents cboCode As System.Windows.Forms.ComboBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtQty As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents lblShift As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents lblDate123 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtColourCode As System.Windows.Forms.TextBox
    Friend WithEvents txtItemCode As System.Windows.Forms.TextBox
    Friend WithEvents txtOperator As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnBack As System.Windows.Forms.Button
    Friend WithEvents btnPrint As System.Windows.Forms.Button
    Friend WithEvents txtNo As System.Windows.Forms.TextBox
    Friend WithEvents Lblno As System.Windows.Forms.Label
    Friend WithEvents CboLocation As System.Windows.Forms.ComboBox
    Friend WithEvents lblTime0 As System.Windows.Forms.Label
    Friend WithEvents lblDate0 As System.Windows.Forms.Label
    Friend WithEvents lblTime24 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents lblDate1 As System.Windows.Forms.Label
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
    Friend WithEvents PrintPreviewDialog1 As System.Windows.Forms.PrintPreviewDialog
    Friend WithEvents Lblcomp As System.Windows.Forms.Label
    Friend WithEvents PrintDocument2 As System.Drawing.Printing.PrintDocument
    Friend WithEvents LblDesc As System.Windows.Forms.Label
    Friend WithEvents lblfinal As System.Windows.Forms.Label
    Friend WithEvents btnDel As System.Windows.Forms.Button
    Friend WithEvents lblhr As System.Windows.Forms.Label
    Friend WithEvents totdattime As System.Windows.Forms.Label
    Friend WithEvents CboReason As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents BtnHold As System.Windows.Forms.Button
    Friend WithEvents PrintDocument3 As System.Drawing.Printing.PrintDocument
    Friend WithEvents txtBooker As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cbTruckNo As System.Windows.Forms.ComboBox
End Class
