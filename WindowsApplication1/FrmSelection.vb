﻿Public Class FrmSelection

    Private Sub btnCurrent_Click(sender As Object, e As EventArgs) Handles btnCurrent.Click
        Stock_Report.Show()
    End Sub

    Private Sub btnLocation_Click(sender As Object, e As EventArgs) Handles btnLocation.Click
        FrmNewPrint.Show()
    End Sub

    Private Sub BtnHold_Click(sender As Object, e As EventArgs) Handles BtnHold.Click
        FrmUpdateHold.Show()
    End Sub

    Private Sub BtnManual_Click(sender As Object, e As EventArgs) Handles BtnManual.Click
        FrmManualSchedule.Show()
    End Sub

    Private Sub btnScrap_Click(sender As Object, e As EventArgs) Handles btnScrap.Click
        FrmScrap.Show()
    End Sub

    Private Sub btnAged_Click(sender As Object, e As EventArgs) Handles btnAged.Click
        FrmOverAged.Show()
    End Sub

    Private Sub btnProd_Click(sender As Object, e As EventArgs) Handles btnProd.Click
        FrmProd_Report.Show()
    End Sub

    Private Sub btnscraprep_Click(sender As Object, e As EventArgs) Handles btnscraprep.Click
        FrmScrapReport.Show()
    End Sub

    Private Sub btnTakentoTB_Click(sender As Object, e As EventArgs) Handles btnTakentoTB.Click
        ReportTB.Show()
    End Sub

    Private Sub btnTBRej_Click(sender As Object, e As EventArgs) Handles btnTBRej.Click

    End Sub

    Private Sub btnDelHistory_Click(sender As Object, e As EventArgs) Handles btnDelHistory.Click
        DeleteHistory.Show()
    End Sub

    Private Sub BtnHoldRep_Click(sender As Object, e As EventArgs) Handles BtnHoldRep.Click
        FrmHoldReport.Show()
    End Sub
End Class